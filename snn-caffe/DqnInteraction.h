

#ifndef DQNINTERACTION_H_
#define DQNINTERACTION_H_


#include <deque>
#include <cfloat>
#include <cstdlib>
#include <opencv/cv.h>


using namespace cv;


class DqnInteration
{
	public:

		Mat* input;
		double immediate_reward;
		int action;

		int* buttons_states;
		std::deque<int> *last_commands;
		std::vector<uint8_t> *rom_info;

		DqnInteration();
		DqnInteration(DqnInteration *iter);
		DqnInteration(Mat *input_p, double immediate_reward_p,
					int action_p, int *buttons_states_p,
					std::deque<int> *last_commands_p,
					std::vector<uint8_t> *rom_info_p);
};


#endif
