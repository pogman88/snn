/*
 * Roms.cpp
 *
 *  Created on: 20 de jul de 2016
 *      Author: fernando
 */

#include "Roms.h"
#include "RomSettings.h"

#include "supported/StreetFighter2.h"
#include <stdio.h>
#include <string.h>

// @@ FIXME : Need to use a generic vector to point for diverse RomSettings
static const RomSettings *roms[] = {
	new StreetFighter2Settings()
};

// @@ FIXME : Need to use a generic vector to point for diverse RomSettings
RomSettings *buildRomRLWrapper(const char* rom) {
	for (size_t i=0; i < sizeof(roms)/sizeof(roms[0]); i++) {
		int ret = strcmp(rom, roms[i]->rom());
		if (strcmp(rom, roms[i]->rom()) == 0) return roms[i]->clone();
	}

	return NULL;
}

