/*
 * StreetFigther2Settings.h
 *
 *  Created on: 20 de jul de 2016
 *      Author: fernando
 */

#ifndef GAMES_SUPPORTED_STREETFIGHTER2_H_
#define GAMES_SUPPORTED_STREETFIGHTER2_H_

#include "../RomSettings.h"
#include "../Roms.h"

// ROM ADDRESS
#define PLAYER1_LIFE  0x7E0D12 // 0:176
#define PLAYER2_LIFE  0x7E0F12 // 0:176
#define MATCH_TIME    0x7E1AC8 // 0:153
#define MATCH_STARTED 0x7E0027 // 0-1
#define WINNER        0x7E1ACF // 0-1-2-255
#define PLAYER1_ID    0x7E0CD1 // 0:7 = NORMAL PLAYERS
#define PLAYER2_ID    0x7E0ED1 // 0:7 = NORMAL PLAYERS 9:12 = BOSSES
#define STAGE_ID	  0x7E1A5A // 0:7 = NORMAL PLAYERS 9:12 = BOSSES

#define MAX_LIFE		176
#define MAX_MATCHTIME	153
#define MAX_STATES		8
/*
 * MASK STATES
 * BIT 0 to 7  - CHARACTER PLAYER 1
 * BIT 8 to 15 - CHARACTER BOT
 * Sequence of Character = Ryu, Edhonda, Blanka, Guile, Ken, Chunli, Zangief, Dhalsin
 */
const uint16 MASK_STATE_P1  = 0xFF00;
const uint16 MASK_STATE_BOT = 0x00FF;

const uint16 MASK_DHALSIN   = 0x0080;
const uint16 MASK_ZANGIEF   = 0x0040;
const uint16 MASK_CHUNLI   	= 0x0020;
const uint16 MASK_KEN   	= 0x0010;
const uint16 MASK_GUILE     = 0x0008;
const uint16 MASK_BLANKA   	= 0x0004;
const uint16 MASK_EDHONDA   = 0x0002;
const uint16 MASK_RYU       = 0x0001;

#define P1_ANIMATION_STATUS 0x7E0CB3
#define P1_ANIMATION2_STATUS 0x7E0C03
#define P2_ANIMATION_STATUS 0x7E0EB3
#define P2_ANIMATION2_STATUS 0x7E0E03

#define P1_CURRENTSIDE 0x7E0CF4 // 64 - Player's located in starting side; 0 - Player's located in opposite side of starting side;
#define P2_CURRENTSIDE 0x7E0C14 // 64 - Player's located in starting side; 0 - Player's located in opposite side of starting side;

const int WAITING_FRAMES = 30;

enum class STATES {

};

// @@ FIXME : use "uint8" instead of use "int"
class StreetFighter2Settings: public RomSettings {
public:
	StreetFighter2Settings();

	/*
	 * ROM SETTINGS
	 */
	// reset the rom to player selection screen
	void reset();

	// process the latest information from SNS
	void update();

	// is end of the game
	bool isTerminal() const;

	// get the most recently observed reward
	reward_t getReward() const;

	// create a new instance of the rom
	RomSettings *clone() const;

	// is an action part of the minimal set?
	bool isMinimal(const Action &a) const;

	// check if the match started
	bool isInitiated() const;

	// Validation of the Action
	bool isLegal(const Action &a);

	std::vector<uint8> getInformation() const;

	std::vector<Action> getMinimalActionSet();

	std::vector<Action> getStartingActions();

	/*
	 * Description of states:
	 * 		State 0 - Versus P2 - Left side
	 * 		State 1 - Versus P2 - Right Side
	 * 		State 2 - Versus AI in diffficult 1 - Left side
	 * 		State 3 - Versus AI in diffficult 1 - Right side
	 * 		State 4 - Versus AI in diffficult 3 - Left side
	 * 		State 5 - Versus AI in diffficult 3 - Right side
	 * 		State 6 - Versus AI in diffficult 5 - Left side
	 * 		State 7 - Versus AI in diffficult 5 - Right side
	 * 		State 8 - Versus AI in diffficult 7 - Left side
	 * 		State 9 - Versus AI in diffficult 7 - Right side
	 */
	std::string loadState(int state);

	// get the name of the rom
	inline const char *rom() const {
		return ROMNAME_STREETFIGHTER2;
	}

	/*
	 * ROM STATE
	 */
	std::vector<uint16> getAllStates() const;
	void setOneState(const uint16 &state);

	bool canAct(const int player);
private:
	uint8 loadCharacter(uint16 c);

	bool m_match_started;
	bool m_end_match_by_victory;
	bool m_end_match_by_draw;

	uint8 m_p1_life;
	uint8 m_p2_life;
	uint8 m_match_time;
	uint8 m_p1_id;
	uint8 m_p2_id;

	uint8 m_bot_ids[8]; // = { 0, 1, 2, 3, 4, 5, 6, 7 };

	bool m_in_animation;
	int m_what_player; // 0 - player 1; 1 - player 2;
};

#endif /* GAMES_SUPPORTED_STREETFIGHTER2_H_ */
