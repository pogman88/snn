/*
 * StreetFigther2Settings.cpp
 *
 *  Created on: 20 de jul de 2016
 *      Author: fernando
 */

#include "StreetFighter2.h"
#include "../RomsUtils.h"
#include <math.h>

uint16 powU16(int i) {
	if (i == 0) {
		return 1;
	}

	uint r = 1;
	for (int j = 0; j < i; j++) {
		r *= 2;
	}
	return r;
}

StreetFighter2Settings::StreetFighter2Settings() {
	m_match_started = false;
	m_terminal = false;
	m_end_match_by_victory = false;
	m_end_match_by_draw = false;
	m_p1_life = m_p2_life = 0;
	m_match_time = 0;
	m_p1_id = 255;
	m_p2_id = 255;
	m_in_animation = false;
	m_what_player = 0;
	for (int i = 0; i < 8; i++)
		m_bot_ids[i] = i; //{ 0, 1, 2, 3, 4, 5, 6, 7 };
}

void StreetFighter2Settings::reset() {
	m_match_started = false;
	m_terminal = false;
	m_end_match_by_victory = false;
	m_end_match_by_draw = false;
	m_p1_life = m_p2_life = 0;
	m_match_time = 0;
	m_p1_id = 255;
	m_p2_id = 255;
	m_in_animation = false;
	m_what_player = 0;
}

bool StreetFighter2Settings::isTerminal() const {
	static int wait_frames = 0;
	if (m_terminal) {
		wait_frames++;
	}

	bool ret = false;

	if (wait_frames == WAITING_FRAMES){
		ret = true;
		wait_frames = 0;
	}

	return ret;
}

reward_t StreetFighter2Settings::getReward() const {
	return (!m_what_player) ? m_p1_life /*+ m_matchTime*/ - m_p2_life : m_p2_life - m_p1_life;
}

RomSettings* StreetFighter2Settings::clone() const {
	RomSettings* rval = new StreetFighter2Settings();
	*rval = *this;
	return rval;
}

bool StreetFighter2Settings::isMinimal(const Action& a) const {
	return (a < PLAYER1_END_ACTIONS) ? true : false;
}

bool StreetFighter2Settings::isLegal(const Action& a) {
	if (a == PLAYER1_NONE || a == PLAYER2_NONE) {
		return false;
	}

	if (!m_what_player) {
		if (m_p1_id == 255) {
			if (a >= PLAYER1_BEGIN_PRESS_ACTIONS && a <= PLAYER1_END_PRESS_ACTIONS) {
				m_p1_id = RUGetByte(PLAYER1_ID);
			}
		}

		if (m_p2_id == 255) {
			if (a >= PLAYER1_BEGIN_PRESS_ACTIONS && a <= PLAYER1_END_PRESS_ACTIONS) {
				m_p1_id = RUGetByte(PLAYER2_ID);
			}
		}
	} else {
		if (m_p1_id == 255) {
			if (a >= PLAYER2_BEGIN_PRESS_ACTIONS && a <= PLAYER2_END_PRESS_ACTIONS) {
				m_p1_id = RUGetByte(PLAYER2_ID);
			}
		}

		if (m_p2_id == 255) {
			if (a >= PLAYER2_BEGIN_PRESS_ACTIONS && a <= PLAYER2_END_PRESS_ACTIONS) {
				m_p1_id = RUGetByte(PLAYER1_ID);
			}
		}
	}

	return true;
}

std::vector<uint8> StreetFighter2Settings::getInformation() const {
	std::vector<uint8> info;

	if (m_match_started) {
		info.push_back(m_match_time);
		info.push_back(m_p1_life);
		info.push_back(m_p2_life);
		if (!m_what_player)
			info.push_back((uint8) RUGetByte(P1_CURRENTSIDE) == 64 ? LEFT : RIGHT); // 0 = Left; 1 - Right
		else
			info.push_back((uint8) RUGetByte(P2_CURRENTSIDE) == 64 ? RIGHT : LEFT); // 0 = Left; 1 - Right
	}

	return info;
}

std::vector<Action> StreetFighter2Settings::getMinimalActionSet() {
	std::vector<Action> actions;
	for (int a = 0; a <= PLAYER1_END_ACTIONS; a++) {
		actions.push_back((Action) a);
	}
	return actions;
}

std::vector<Action> StreetFighter2Settings::getStartingActions() {
	std::vector<Action> actions;
	for (int a = 0; a < PLAYER1_B_PRESS; a++) {
		actions.push_back((Action) a);
	}
	return actions;
}

bool StreetFighter2Settings::isInitiated() const {
	return m_match_started;
}

void StreetFighter2Settings::update() {
	// Check if the match was started
	if (!m_match_started) {
		if (!m_what_player) {
			if (m_p2_id != 255) {
				RUSetByte(m_p2_id, PLAYER2_ID);
				RUSetByte(m_p2_id, STAGE_ID);
			}
			if (m_p1_id != 255) {
				RUSetByte(m_p1_id, PLAYER1_ID);
			}
		} else {
			if (m_p2_id != 255) {
				RUSetByte(m_p2_id, PLAYER1_ID);
				RUSetByte(m_p2_id, STAGE_ID);
			}
			if (m_p1_id != 255) {
				RUSetByte(m_p1_id, PLAYER2_ID);
			}
		}
		m_match_started = (RUGetByte(MATCH_STARTED) == 1) ? true : false;
	}

	if (!m_what_player) {
		m_p1_life = RUGetByte(PLAYER1_LIFE);
		m_p2_life = RUGetByte(PLAYER2_LIFE);
	} else {
		m_p1_life = RUGetByte(PLAYER2_LIFE);
		m_p2_life = RUGetByte(PLAYER1_LIFE);
	}

	m_p1_life = (m_p1_life > MAX_LIFE) ? 0 : m_p1_life;
	m_p2_life = (m_p2_life > MAX_LIFE) ? 0 : m_p2_life;
	m_match_time = RUGetByte(MATCH_TIME);

	if (m_match_started) {

		if (m_p2_id == 255) {
			if (!m_what_player)
				m_p2_id = RUGetByte(PLAYER2_ID);
			else
				m_p2_id = RUGetByte(PLAYER1_ID);
		}

		uint8 winner = RUGetByte(WINNER);
		if (winner == 1 || winner == 2) {
			m_end_match_by_victory = true;
		} else if (winner == 255) {
			m_end_match_by_draw = true;
		}
	}

	if ((m_end_match_by_victory && (m_p1_life == 0 || m_p2_life == 0 || m_match_time == 0)) || m_end_match_by_draw) {
		m_terminal = true;
	}
}

std::vector<uint16> StreetFighter2Settings::getAllStates() const {
	std::vector<uint16> states;
//
//	for (int i = 0; i < MAX_STATES; i++) {
//		for (int j = 0; j < MAX_STATES; j++) {
//			states.push_back(powU16(i) << 8 | powU16(j));
//		}
//	}
//
	return states;
}

void StreetFighter2Settings::setOneState(const uint16 &state) {
	uint16 p1 = (state & MASK_STATE_P1) >> 8;
	uint16 bot = state & MASK_STATE_BOT;

	m_p1_id = loadCharacter(p1);
	m_p2_id = loadCharacter(bot);
}

bool StreetFighter2Settings::canAct(const int player) {
	if (player == 1) {
		uint8 byte1 = RUGetByte(P1_ANIMATION_STATUS);
		uint8 byte2 = RUGetByte(P1_ANIMATION2_STATUS);
		if (byte1 == 1 || byte2 == 14 || byte2 == 1 || byte2 == 3) {
			return false;
		}
	} else {
		uint8 byte1 = RUGetByte(P2_ANIMATION_STATUS);
		uint8 byte2 = RUGetByte(P2_ANIMATION2_STATUS);
		if (byte1 == 1 || byte2 == 14 || byte2 == 1 || byte2 == 3) {
			return false;
		}
	}

	return true;
}

uint8 StreetFighter2Settings::loadCharacter(uint16 c) {
	switch (c) {
	case MASK_RYU:     return 0;
	case MASK_EDHONDA: return 1;
	case MASK_BLANKA:  return 2;
	case MASK_GUILE:   return 3;
	case MASK_KEN:     return 4;
	case MASK_CHUNLI:  return 5;
	case MASK_ZANGIEF: return 6;
	default:           return 7; // MASK_DHALSIN
	}
}

std::string StreetFighter2Settings::loadState(int state) {
	std::string s;
	switch(state) {
	case 0:
		s = "QuickLoad000"; // Random
		m_what_player = 0;
		break;
	case 1:
		s = "QuickLoad000";
		m_what_player = 1;
		break;
	case 2:
		s = "QuickLoad001";
		m_what_player = 0;
		break;
	case 3:
		s = "QuickLoad005";
		m_what_player = 1;
		break;
	case 4:
		s = "QuickLoad002";
		m_what_player = 0;
		break;
	case 5:
		s = "QuickLoad006";
		m_what_player = 1;
		break;
	case 6:
		s = "QuickLoad003";
		m_what_player = 0;
		break;
	case 7:
		s = "QuickLoad007";
		m_what_player = 1;
		break;
	case 8:
		s = "QuickLoad004";
		m_what_player = 0;
		break;
	case 9:
		s = "QuickLoad008";
		m_what_player = 1;
		break;
	}

	return s;
}
