/*
 * RomUtils.h
 *
 *  Created on: 21 de jul de 2016
 *      Author: fernando
 */

#ifndef GAMES_ROMSUTILS_H_
#define GAMES_ROMSUTILS_H_

#include "snes9x.h"
#include "memmap.h"
#include "cheats.h"

uint8 RUGetByte (uint32);
void RUSetByte (uint8 byte, uint32 address);

#endif /* GAMES_ROMSUTILS_H_ */
