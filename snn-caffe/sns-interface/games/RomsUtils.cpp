/*
 * RomUtils.cpp
 *
 *  Created on: 21 de jul de 2016
 *      Author: fernando
 */

#include "RomsUtils.h"

uint8 RUGetByte (uint32 address)
{
	uint32	Cycles = CPU.Cycles;
	uint8	byte;

	byte = S9xGetByte(address);
	CPU.Cycles = Cycles;

	return (byte);
}

void RUSetByte (uint8 byte, uint32 address)
{
	uint32	Cycles = CPU.Cycles;

	S9xSetByte(byte, address);
	CPU.Cycles = Cycles;
}
