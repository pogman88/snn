/*
 * rom.h
 *
 *  Created on: 20 de jul de 2016
 *      Author: fernando
 */

#ifndef GAMES_ROMS_H_
#define GAMES_ROMS_H_

#include "RomSettings.h"
#include <string>

#define ROMNAME_STREETFIGHTER2 "street_fighter_2"

class RomSettings;

extern RomSettings * buildRomRLWrapper(const char* rom);


#endif /* GAMES_ROMS_H_ */
