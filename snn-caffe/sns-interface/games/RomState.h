/*
 * RomState.h
 *
 *  Created on: Aug 19, 2016
 *      Author: fernando
 */

#ifndef SNN_CAFFE_SNS_INTERFACE_GAMES_ROMSTATE_H_
#define SNN_CAFFE_SNS_INTERFACE_GAMES_ROMSTATE_H_

#include "../Constants.h"
//#include "../FlagSet.hpp"
#include <vector>
#include <iostream>
#include <bitset>
#include <cassert>

class RomState {
public:
	virtual ~RomState();
	virtual std::vector<uint16> getAllStates() const = 0;
	virtual void setOneState(const uint16 &state) = 0;
protected:
	std::vector<uint16> m_all_states;
	uint16 m_state;
};

#endif /* SNN_CAFFE_SNS_INTERFACE_GAMES_ROMSTATE_H_ */
