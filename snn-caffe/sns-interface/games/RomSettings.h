/*
 * RomSettings.h
 *
 *  Created on: 20 de jul de 2016
 *      Author: fernando
 */

#ifndef GAMES_ROMSETTINGS_H_
#define GAMES_ROMSETTINGS_H_

#include "../Constants.h"
#include "../snes9x/port.h"
#include "RomState.h"

#include <vector>

class RomSettings : public RomState {
public:
	virtual ~RomSettings() {}

	virtual void reset() = 0;

	virtual bool isTerminal() const = 0;

	virtual reward_t getReward() const = 0;

	virtual void update() = 0;

	virtual const char *rom() const = 0;

	virtual RomSettings *clone() const = 0;

	virtual std::vector<uint8> getInformation() const = 0;

	virtual bool isMinimal(const Action &a) const = 0;

	virtual bool isInitiated() const = 0;

	virtual bool isLegal(const Action &a) = 0;

	virtual std::vector<Action> getMinimalActionSet() = 0;

	virtual std::vector<Action> getStartingActions() = 0;

	virtual std::string loadState(int state) = 0;

	virtual bool canAct(const int player) = 0;
protected:
	bool m_terminal;
};


#endif /* GAMES_ROMSETTINGS_H_ */
