/*
 * snn_main.cpp
 *
 *  Created on: 18 de jul de 2016
 *      Author: fernando
 */

#include <cstdlib>
#include <ctime>
#include "Constants.h"
#include "SNSInterface.h"
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

int main(int argc, char** argv) {
	SNSInterface *sns = new SNSInterface(argv[1]);

	sns->init();

	if (sns->isSnnInitiated()) {
		sns->loadROM();
	}

	cv::Mat raw_frame = sns->getScreen();

	printf("NL: %d NC: %d\n", raw_frame.rows, raw_frame.cols);
	//	return 0;

	std::vector<Action> actsS = sns->getStartingActions();
	std::vector<Action> acts = sns->getMinimalActionSet();

	srand((int) time(0));

	while (1) {
		sns->changeBotBehavior(0);
		sns->resetGame();
		sns->setOneState(MASK_RYU << 8 | MASK_BLANKA);

		int step = 0;
		char c;

		while (!sns->gameOver()) {
			sns->act(PLAYER1_DOWN_PRESS);
			sns->act(PLAYER1_DOWN_RELEASE);
//			std::vector<Action> v;
//			int i;
//			i = rand() % acts.size();
//			v.push_back(acts[i]);
			reward_t t = sns->act();
			cv::Mat raw_frame = sns->getScreen();
			cv::imshow("frame", raw_frame);
			
			if (step)
				c = cv::waitKey(-1);
			else
				c = cv::waitKey(5);
			if (c == 's') step = !step;


		}
	}

	return 0;
}
