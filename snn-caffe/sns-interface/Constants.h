/*
 * Constants.h
 *
 *  Created on: Jul 21, 2016
 *      Author: fernando
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#include <string>
#include <vector>

// Define Actions
enum Action {
	PLAYER1_NONE 			= 0,
	PLAYER1_UP_PRESS 		= 1,
	PLAYER1_RIGHT_PRESS 	= 2,
	PLAYER1_DOWN_PRESS 		= 3,
	PLAYER1_LEFT_PRESS 		= 4,
	PLAYER1_A_PRESS 		= 5,
	PLAYER1_B_PRESS 		= 6,
	PLAYER1_X_PRESS 		= 7,
	PLAYER1_Y_PRESS 		= 8,
	PLAYER1_L_PRESS 		= 9,
	PLAYER1_R_PRESS 		= 10,

	PLAYER1_UP_RELEASE 		= 11,
	PLAYER1_RIGHT_RELEASE 	= 12,
	PLAYER1_DOWN_RELEASE 	= 13,
	PLAYER1_LEFT_RELEASE 	= 14,
	PLAYER1_A_RELEASE 		= 15,
	PLAYER1_B_RELEASE 		= 16,
	PLAYER1_X_RELEASE 		= 17,
	PLAYER1_Y_RELEASE 		= 18,
	PLAYER1_L_RELEASE 		= 19,
	PLAYER1_R_RELEASE 		= 20,

	PLAYER2_NONE 			= 21,
	PLAYER2_UP_PRESS 		= 22,
	PLAYER2_RIGHT_PRESS 	= 23,
	PLAYER2_DOWN_PRESS 		= 24,
	PLAYER2_LEFT_PRESS 		= 25,
	PLAYER2_A_PRESS 		= 26,
	PLAYER2_B_PRESS 		= 27,
	PLAYER2_X_PRESS 		= 28,
	PLAYER2_Y_PRESS 		= 29,
	PLAYER2_L_PRESS 		= 30,
	PLAYER2_R_PRESS 		= 31,

	PLAYER2_UP_RELEASE 		= 32,
	PLAYER2_RIGHT_RELEASE 	= 33,
	PLAYER2_DOWN_RELEASE 	= 34,
	PLAYER2_LEFT_RELEASE 	= 35,
	PLAYER2_A_RELEASE 		= 36,
	PLAYER2_B_RELEASE 		= 37,
	PLAYER2_X_RELEASE 		= 38,
	PLAYER2_Y_RELEASE 		= 39,
	PLAYER2_L_RELEASE 		= 40,
	PLAYER2_R_RELEASE 		= 41,

	NONE 					= 100
};

enum ConfigSystem {
	FRAME_RATE_0   = 2,
	FRAME_RATE_10  = 3,
	FRAME_RATE_20  = 4,
	FRAME_RATE_30  = 5,
	FRAME_RATE_40  = 6,
	FRAME_RATE_50  = 7,
	FRAME_RATE_60  = 8,
	FRAME_RATE_70  = 9,
	FRAME_RATE_80  = 10,
	FRAME_RATE_90  = 11,
	FRAME_RATE_100 = 12,
	NO_SOUND       = 13,
	WITH_SOUND     = 14,
	FRAME_RATE_1   = 15,
};

#define PLAYER1_BEGIN_PRESS_ACTIONS PLAYER1_UP_PRESS
#define PLAYER1_BEGIN_RELEASE_ACTIONS PLAYER1_UP_RELEASE
#define PLAYER1_END_PRESS_ACTIONS PLAYER1_R_PRESS
#define PLAYER1_END_RELEASE_ACTIONS PLAYER1_R_RELEASE

#define PLAYER2_BEGIN_PRESS_ACTIONS PLAYER2_UP_PRESS
#define PLAYER2_BEGIN_RELEASE_ACTIONS PLAYER2_UP_RELEASE
#define PLAYER2_END_PRESS_ACTIONS PLAYER2_R_PRESS
#define PLAYER2_END_RELEASE_ACTIONS PLAYER2_R_RELEASE

#define PLAYER1_BEGIN_ACTIONS PLAYER1_UP_PRESS
#define PLAYER1_END_ACTIONS PLAYER1_R_RELEASE
#define PLAYER2_END_ACTIONS PLAYER2_R_RELEASE
#define TOTAL_ACTIONS (PLAYER1_END_ACTIONS + 1)
#define TOTAL_BUTTONS 10

#define LEFT 0
#define RIGHT 1

int isPress(const Action a);
std::string action_to_string(const Action a) ;
Action int_to_action(const int a);

template< typename T, size_t N >
inline std::vector<T> makeVector(const T (&data)[N]) {
    return std::vector<T>(data, data+N);
}

//  Define datatypes
typedef std::vector<ConfigSystem> ConfigVect;

// reward type for SNS interface
typedef int reward_t;
typedef unsigned short uint16;

#endif /* CONSTANTS_H_ */
