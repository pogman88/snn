# README #

### SNN ###

Custom Snes9x Emulator for a step-by-step emulation of the rom Street Fighter 2

### Pre-Requisites ###
* OpenCv 3.1 

### Compilation ###
* git clone https://pogman88@bitbucket.org/pogman88/snn.git
* cd snn 
* make

### Run ###
* ./snn ~/snn/snes9x/rom/sf2.sfc