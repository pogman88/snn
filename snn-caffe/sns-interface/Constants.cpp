/*
 * Constants.cpp
 *
 *  Created on: 26 de jul de 2016
 *      Author: fernando
 */

#include "Constants.h"

int isPress(const Action a) {
	return ((a >= PLAYER1_BEGIN_PRESS_ACTIONS &&	a <= PLAYER1_END_PRESS_ACTIONS) ||
			(a >= PLAYER2_BEGIN_PRESS_ACTIONS &&	a <= PLAYER2_END_PRESS_ACTIONS)) ? 1 : 0;
}

std::string action_to_string(const Action a) {
	if (a == PLAYER1_UP_PRESS || a == PLAYER1_UP_RELEASE)
		return std::string("Joypad1 Up");
	else if (a == PLAYER1_RIGHT_PRESS || a == PLAYER1_RIGHT_RELEASE)
		return std::string("Joypad1 Right");
	else if (a == PLAYER1_DOWN_PRESS || a == PLAYER1_DOWN_RELEASE)
		return std::string("Joypad1 Down");
	else if (a == PLAYER1_LEFT_PRESS || a == PLAYER1_LEFT_RELEASE)
		return std::string("Joypad1 Left");
	else if (a == PLAYER1_A_PRESS || a == PLAYER1_A_RELEASE)
		return std::string("Joypad1 A");
	else if (a == PLAYER1_B_PRESS || a == PLAYER1_B_RELEASE)
		return std::string("Joypad1 B");
	else if (a == PLAYER1_X_PRESS || a == PLAYER1_X_RELEASE)
		return std::string("Joypad1 X");
	else if (a == PLAYER1_Y_PRESS || a == PLAYER1_Y_RELEASE)
		return std::string("Joypad1 Y");
	else if (a == PLAYER1_L_PRESS || a == PLAYER1_L_RELEASE)
		return std::string("Joypad1 L");
	else if (a == PLAYER1_R_PRESS || a == PLAYER1_R_RELEASE)
		return std::string("Joypad1 R");

	if (a == PLAYER2_UP_PRESS || a == PLAYER2_UP_RELEASE)
		return std::string("Joypad2 Up");
	else if (a == PLAYER2_RIGHT_PRESS || a == PLAYER2_RIGHT_RELEASE)
		return std::string("Joypad2 Right");
	else if (a == PLAYER2_DOWN_PRESS || a == PLAYER2_DOWN_RELEASE)
		return std::string("Joypad2 Down");
	else if (a == PLAYER2_LEFT_PRESS || a == PLAYER2_LEFT_RELEASE)
		return std::string("Joypad2 Left");
	else if (a == PLAYER2_A_PRESS || a == PLAYER2_A_RELEASE)
		return std::string("Joypad2 A");
	else if (a == PLAYER2_B_PRESS || a == PLAYER2_B_RELEASE)
		return std::string("Joypad2 B");
	else if (a == PLAYER2_X_PRESS || a == PLAYER2_X_RELEASE)
		return std::string("Joypad2 X");
	else if (a == PLAYER2_Y_PRESS || a == PLAYER2_Y_RELEASE)
		return std::string("Joypad2 Y");
	else if (a == PLAYER2_L_PRESS || a == PLAYER2_L_RELEASE)
		return std::string("Joypad2 L");
	else if (a == PLAYER2_R_PRESS || a == PLAYER2_R_RELEASE)
		return std::string("Joypad2 R");
	else
		return "None";
}

Action int_to_action(const int a) {
	switch (a) {
	case 0:  return PLAYER1_NONE;
	case 1:	 return PLAYER1_UP_PRESS;
	case 2:  return PLAYER1_RIGHT_PRESS;
	case 3:	 return PLAYER1_DOWN_PRESS;
	case 4:  return PLAYER1_LEFT_PRESS;
	case 5:  return PLAYER1_A_PRESS;
	case 6:  return PLAYER1_B_PRESS;
	case 7:  return PLAYER1_X_PRESS;
	case 8:  return PLAYER1_Y_PRESS;
	case 9: return PLAYER1_L_PRESS;
	case 10: return PLAYER1_R_PRESS;
	case 11: return PLAYER1_UP_RELEASE;
	case 12: return PLAYER1_RIGHT_RELEASE;
	case 13: return PLAYER1_DOWN_RELEASE;
	case 14: return PLAYER1_LEFT_RELEASE;
	case 15: return PLAYER1_A_RELEASE;
	case 16: return PLAYER1_B_RELEASE;
	case 17: return PLAYER1_X_RELEASE;
	case 18: return PLAYER1_Y_RELEASE;
	case 19: return PLAYER1_L_RELEASE;
	case 20: return PLAYER1_R_RELEASE;

	case 21: return PLAYER2_NONE;
	case 22: return PLAYER2_UP_PRESS;
	case 23: return PLAYER2_RIGHT_PRESS;
	case 24: return PLAYER2_DOWN_PRESS;
	case 25: return PLAYER2_LEFT_PRESS;
	case 26: return PLAYER2_A_PRESS;
	case 27: return PLAYER2_B_PRESS;
	case 28: return PLAYER2_X_PRESS;
	case 29: return PLAYER2_Y_PRESS;
	case 30: return PLAYER2_L_PRESS;
	case 31: return PLAYER2_R_PRESS;
	case 32: return PLAYER2_UP_RELEASE;
	case 33: return PLAYER2_RIGHT_RELEASE;
	case 34: return PLAYER2_DOWN_RELEASE;
	case 35: return PLAYER2_LEFT_RELEASE;
	case 36: return PLAYER2_A_RELEASE;
	case 37: return PLAYER2_B_RELEASE;
	case 38: return PLAYER2_X_RELEASE;
	case 39: return PLAYER2_Y_RELEASE;
	case 40: return PLAYER2_L_RELEASE;
	case 41: return PLAYER2_R_RELEASE;

	default: return PLAYER1_NONE;
	}
}
