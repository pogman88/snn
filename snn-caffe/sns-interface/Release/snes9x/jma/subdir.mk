################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../snes9x/jma/7zlzma.cpp \
../snes9x/jma/crc32.cpp \
../snes9x/jma/iiostrm.cpp \
../snes9x/jma/inbyte.cpp \
../snes9x/jma/jma.cpp \
../snes9x/jma/lzma.cpp \
../snes9x/jma/lzmadec.cpp \
../snes9x/jma/s9x-jma.cpp \
../snes9x/jma/winout.cpp 

O_SRCS += \
../snes9x/jma/7zlzma.o \
../snes9x/jma/crc32.o \
../snes9x/jma/iiostrm.o \
../snes9x/jma/inbyte.o \
../snes9x/jma/jma.o \
../snes9x/jma/lzma.o \
../snes9x/jma/lzmadec.o \
../snes9x/jma/s9x-jma.o \
../snes9x/jma/winout.o 

OBJS += \
./snes9x/jma/7zlzma.o \
./snes9x/jma/crc32.o \
./snes9x/jma/iiostrm.o \
./snes9x/jma/inbyte.o \
./snes9x/jma/jma.o \
./snes9x/jma/lzma.o \
./snes9x/jma/lzmadec.o \
./snes9x/jma/s9x-jma.o \
./snes9x/jma/winout.o 

CPP_DEPS += \
./snes9x/jma/7zlzma.d \
./snes9x/jma/crc32.d \
./snes9x/jma/iiostrm.d \
./snes9x/jma/inbyte.d \
./snes9x/jma/jma.d \
./snes9x/jma/lzma.d \
./snes9x/jma/lzmadec.d \
./snes9x/jma/s9x-jma.d \
./snes9x/jma/winout.d 


# Each subdirectory must supply rules for building sources it contributes
snes9x/jma/%.o: ../snes9x/jma/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


