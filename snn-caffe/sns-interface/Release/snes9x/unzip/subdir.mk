################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../snes9x/unzip/ioapi.c \
../snes9x/unzip/iowin32.c \
../snes9x/unzip/miniunz.c \
../snes9x/unzip/minizip.c \
../snes9x/unzip/mztools.c \
../snes9x/unzip/unzip.c \
../snes9x/unzip/zip.c 

O_SRCS += \
../snes9x/unzip/ioapi.o \
../snes9x/unzip/unzip.o 

OBJS += \
./snes9x/unzip/ioapi.o \
./snes9x/unzip/iowin32.o \
./snes9x/unzip/miniunz.o \
./snes9x/unzip/minizip.o \
./snes9x/unzip/mztools.o \
./snes9x/unzip/unzip.o \
./snes9x/unzip/zip.o 

C_DEPS += \
./snes9x/unzip/ioapi.d \
./snes9x/unzip/iowin32.d \
./snes9x/unzip/miniunz.d \
./snes9x/unzip/minizip.d \
./snes9x/unzip/mztools.d \
./snes9x/unzip/unzip.d \
./snes9x/unzip/zip.d 


# Each subdirectory must supply rules for building sources it contributes
snes9x/unzip/%.o: ../snes9x/unzip/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


