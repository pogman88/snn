################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../snes9x/bsx.cpp \
../snes9x/c4.cpp \
../snes9x/c4emu.cpp \
../snes9x/cheats.cpp \
../snes9x/cheats2.cpp \
../snes9x/clip.cpp \
../snes9x/conffile.cpp \
../snes9x/controls.cpp \
../snes9x/cpu.cpp \
../snes9x/cpuexec.cpp \
../snes9x/cpuops.cpp \
../snes9x/crosshairs.cpp \
../snes9x/customFunctions.cpp \
../snes9x/debug.cpp \
../snes9x/dma.cpp \
../snes9x/dsp.cpp \
../snes9x/dsp1.cpp \
../snes9x/dsp2.cpp \
../snes9x/dsp3.cpp \
../snes9x/dsp4.cpp \
../snes9x/fxdbg.cpp \
../snes9x/fxemu.cpp \
../snes9x/fxinst.cpp \
../snes9x/gfx.cpp \
../snes9x/globals.cpp \
../snes9x/loadzip.cpp \
../snes9x/logger.cpp \
../snes9x/memmap.cpp \
../snes9x/movie.cpp \
../snes9x/netplay.cpp \
../snes9x/obc1.cpp \
../snes9x/ppu.cpp \
../snes9x/reader.cpp \
../snes9x/sa1.cpp \
../snes9x/sa1cpu.cpp \
../snes9x/screenshot.cpp \
../snes9x/sdd1.cpp \
../snes9x/sdd1emu.cpp \
../snes9x/server.cpp \
../snes9x/seta.cpp \
../snes9x/seta010.cpp \
../snes9x/seta011.cpp \
../snes9x/seta018.cpp \
../snes9x/snapshot.cpp \
../snes9x/snes9x.cpp \
../snes9x/spc7110.cpp \
../snes9x/spc7110dec.cpp \
../snes9x/spc7110emu.cpp \
../snes9x/srtc.cpp \
../snes9x/srtcemu.cpp \
../snes9x/tile.cpp 

O_SRCS += \
../snes9x/bsx.o \
../snes9x/c4.o \
../snes9x/c4emu.o \
../snes9x/cheats.o \
../snes9x/cheats2.o \
../snes9x/clip.o \
../snes9x/conffile.o \
../snes9x/controls.o \
../snes9x/cpu.o \
../snes9x/cpuexec.o \
../snes9x/cpuops.o \
../snes9x/crosshairs.o \
../snes9x/debug.o \
../snes9x/dma.o \
../snes9x/dsp.o \
../snes9x/dsp1.o \
../snes9x/dsp2.o \
../snes9x/dsp3.o \
../snes9x/dsp4.o \
../snes9x/fxdbg.o \
../snes9x/fxemu.o \
../snes9x/fxinst.o \
../snes9x/gfx.o \
../snes9x/globals.o \
../snes9x/loadzip.o \
../snes9x/logger.o \
../snes9x/memmap.o \
../snes9x/movie.o \
../snes9x/netplay.o \
../snes9x/obc1.o \
../snes9x/ppu.o \
../snes9x/reader.o \
../snes9x/sa1.o \
../snes9x/sa1cpu.o \
../snes9x/screenshot.o \
../snes9x/sdd1.o \
../snes9x/sdd1emu.o \
../snes9x/server.o \
../snes9x/seta.o \
../snes9x/seta010.o \
../snes9x/seta011.o \
../snes9x/seta018.o \
../snes9x/snapshot.o \
../snes9x/snes9x.o \
../snes9x/spc7110.o \
../snes9x/srtc.o \
../snes9x/tile.o 

OBJS += \
./snes9x/bsx.o \
./snes9x/c4.o \
./snes9x/c4emu.o \
./snes9x/cheats.o \
./snes9x/cheats2.o \
./snes9x/clip.o \
./snes9x/conffile.o \
./snes9x/controls.o \
./snes9x/cpu.o \
./snes9x/cpuexec.o \
./snes9x/cpuops.o \
./snes9x/crosshairs.o \
./snes9x/customFunctions.o \
./snes9x/debug.o \
./snes9x/dma.o \
./snes9x/dsp.o \
./snes9x/dsp1.o \
./snes9x/dsp2.o \
./snes9x/dsp3.o \
./snes9x/dsp4.o \
./snes9x/fxdbg.o \
./snes9x/fxemu.o \
./snes9x/fxinst.o \
./snes9x/gfx.o \
./snes9x/globals.o \
./snes9x/loadzip.o \
./snes9x/logger.o \
./snes9x/memmap.o \
./snes9x/movie.o \
./snes9x/netplay.o \
./snes9x/obc1.o \
./snes9x/ppu.o \
./snes9x/reader.o \
./snes9x/sa1.o \
./snes9x/sa1cpu.o \
./snes9x/screenshot.o \
./snes9x/sdd1.o \
./snes9x/sdd1emu.o \
./snes9x/server.o \
./snes9x/seta.o \
./snes9x/seta010.o \
./snes9x/seta011.o \
./snes9x/seta018.o \
./snes9x/snapshot.o \
./snes9x/snes9x.o \
./snes9x/spc7110.o \
./snes9x/spc7110dec.o \
./snes9x/spc7110emu.o \
./snes9x/srtc.o \
./snes9x/srtcemu.o \
./snes9x/tile.o 

CPP_DEPS += \
./snes9x/bsx.d \
./snes9x/c4.d \
./snes9x/c4emu.d \
./snes9x/cheats.d \
./snes9x/cheats2.d \
./snes9x/clip.d \
./snes9x/conffile.d \
./snes9x/controls.d \
./snes9x/cpu.d \
./snes9x/cpuexec.d \
./snes9x/cpuops.d \
./snes9x/crosshairs.d \
./snes9x/customFunctions.d \
./snes9x/debug.d \
./snes9x/dma.d \
./snes9x/dsp.d \
./snes9x/dsp1.d \
./snes9x/dsp2.d \
./snes9x/dsp3.d \
./snes9x/dsp4.d \
./snes9x/fxdbg.d \
./snes9x/fxemu.d \
./snes9x/fxinst.d \
./snes9x/gfx.d \
./snes9x/globals.d \
./snes9x/loadzip.d \
./snes9x/logger.d \
./snes9x/memmap.d \
./snes9x/movie.d \
./snes9x/netplay.d \
./snes9x/obc1.d \
./snes9x/ppu.d \
./snes9x/reader.d \
./snes9x/sa1.d \
./snes9x/sa1cpu.d \
./snes9x/screenshot.d \
./snes9x/sdd1.d \
./snes9x/sdd1emu.d \
./snes9x/server.d \
./snes9x/seta.d \
./snes9x/seta010.d \
./snes9x/seta011.d \
./snes9x/seta018.d \
./snes9x/snapshot.d \
./snes9x/snes9x.d \
./snes9x/spc7110.d \
./snes9x/spc7110dec.d \
./snes9x/spc7110emu.d \
./snes9x/srtc.d \
./snes9x/srtcemu.d \
./snes9x/tile.d 


# Each subdirectory must supply rules for building sources it contributes
snes9x/%.o: ../snes9x/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


