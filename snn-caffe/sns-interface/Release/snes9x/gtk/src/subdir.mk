################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../snes9x/gtk/src/filter_epx_unsafe.cpp \
../snes9x/gtk/src/gtk_binding.cpp \
../snes9x/gtk/src/gtk_builder_window.cpp \
../snes9x/gtk/src/gtk_cheat.cpp \
../snes9x/gtk/src/gtk_config.cpp \
../snes9x/gtk/src/gtk_control.cpp \
../snes9x/gtk/src/gtk_display.cpp \
../snes9x/gtk/src/gtk_display_driver_gtk.cpp \
../snes9x/gtk/src/gtk_display_driver_opengl.cpp \
../snes9x/gtk/src/gtk_display_driver_xv.cpp \
../snes9x/gtk/src/gtk_file.cpp \
../snes9x/gtk/src/gtk_netplay.cpp \
../snes9x/gtk/src/gtk_netplay_dialog.cpp \
../snes9x/gtk/src/gtk_preferences.cpp \
../snes9x/gtk/src/gtk_s9x.cpp \
../snes9x/gtk/src/gtk_s9xwindow.cpp \
../snes9x/gtk/src/gtk_sound.cpp \
../snes9x/gtk/src/gtk_sound_driver_alsa.cpp \
../snes9x/gtk/src/gtk_sound_driver_oss.cpp \
../snes9x/gtk/src/gtk_sound_driver_portaudio.cpp \
../snes9x/gtk/src/gtk_sound_driver_pulse.cpp \
../snes9x/gtk/src/gtk_sound_driver_sdl.cpp 

C_SRCS += \
../snes9x/gtk/src/snes_ntsc.c \
../snes9x/gtk/src/sourcify.c 

OBJS += \
./snes9x/gtk/src/filter_epx_unsafe.o \
./snes9x/gtk/src/gtk_binding.o \
./snes9x/gtk/src/gtk_builder_window.o \
./snes9x/gtk/src/gtk_cheat.o \
./snes9x/gtk/src/gtk_config.o \
./snes9x/gtk/src/gtk_control.o \
./snes9x/gtk/src/gtk_display.o \
./snes9x/gtk/src/gtk_display_driver_gtk.o \
./snes9x/gtk/src/gtk_display_driver_opengl.o \
./snes9x/gtk/src/gtk_display_driver_xv.o \
./snes9x/gtk/src/gtk_file.o \
./snes9x/gtk/src/gtk_netplay.o \
./snes9x/gtk/src/gtk_netplay_dialog.o \
./snes9x/gtk/src/gtk_preferences.o \
./snes9x/gtk/src/gtk_s9x.o \
./snes9x/gtk/src/gtk_s9xwindow.o \
./snes9x/gtk/src/gtk_sound.o \
./snes9x/gtk/src/gtk_sound_driver_alsa.o \
./snes9x/gtk/src/gtk_sound_driver_oss.o \
./snes9x/gtk/src/gtk_sound_driver_portaudio.o \
./snes9x/gtk/src/gtk_sound_driver_pulse.o \
./snes9x/gtk/src/gtk_sound_driver_sdl.o \
./snes9x/gtk/src/snes_ntsc.o \
./snes9x/gtk/src/sourcify.o 

CPP_DEPS += \
./snes9x/gtk/src/filter_epx_unsafe.d \
./snes9x/gtk/src/gtk_binding.d \
./snes9x/gtk/src/gtk_builder_window.d \
./snes9x/gtk/src/gtk_cheat.d \
./snes9x/gtk/src/gtk_config.d \
./snes9x/gtk/src/gtk_control.d \
./snes9x/gtk/src/gtk_display.d \
./snes9x/gtk/src/gtk_display_driver_gtk.d \
./snes9x/gtk/src/gtk_display_driver_opengl.d \
./snes9x/gtk/src/gtk_display_driver_xv.d \
./snes9x/gtk/src/gtk_file.d \
./snes9x/gtk/src/gtk_netplay.d \
./snes9x/gtk/src/gtk_netplay_dialog.d \
./snes9x/gtk/src/gtk_preferences.d \
./snes9x/gtk/src/gtk_s9x.d \
./snes9x/gtk/src/gtk_s9xwindow.d \
./snes9x/gtk/src/gtk_sound.d \
./snes9x/gtk/src/gtk_sound_driver_alsa.d \
./snes9x/gtk/src/gtk_sound_driver_oss.d \
./snes9x/gtk/src/gtk_sound_driver_portaudio.d \
./snes9x/gtk/src/gtk_sound_driver_pulse.d \
./snes9x/gtk/src/gtk_sound_driver_sdl.d 

C_DEPS += \
./snes9x/gtk/src/snes_ntsc.d \
./snes9x/gtk/src/sourcify.d 


# Each subdirectory must supply rules for building sources it contributes
snes9x/gtk/src/%.o: ../snes9x/gtk/src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

snes9x/gtk/src/%.o: ../snes9x/gtk/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


