################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../snes9x/apu/SNES_SPC.cpp \
../snes9x/apu/SNES_SPC_misc.cpp \
../snes9x/apu/SNES_SPC_state.cpp \
../snes9x/apu/SPC_DSP.cpp \
../snes9x/apu/SPC_Filter.cpp \
../snes9x/apu/apu.cpp 

O_SRCS += \
../snes9x/apu/SNES_SPC.o \
../snes9x/apu/SNES_SPC_misc.o \
../snes9x/apu/SNES_SPC_state.o \
../snes9x/apu/SPC_DSP.o \
../snes9x/apu/SPC_Filter.o \
../snes9x/apu/apu.o 

OBJS += \
./snes9x/apu/SNES_SPC.o \
./snes9x/apu/SNES_SPC_misc.o \
./snes9x/apu/SNES_SPC_state.o \
./snes9x/apu/SPC_DSP.o \
./snes9x/apu/SPC_Filter.o \
./snes9x/apu/apu.o 

CPP_DEPS += \
./snes9x/apu/SNES_SPC.d \
./snes9x/apu/SNES_SPC_misc.d \
./snes9x/apu/SNES_SPC_state.d \
./snes9x/apu/SPC_DSP.d \
./snes9x/apu/SPC_Filter.d \
./snes9x/apu/apu.d 


# Each subdirectory must supply rules for building sources it contributes
snes9x/apu/%.o: ../snes9x/apu/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


