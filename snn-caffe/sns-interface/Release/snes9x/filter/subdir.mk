################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../snes9x/filter/2xsai.cpp \
../snes9x/filter/blit.cpp \
../snes9x/filter/epx.cpp \
../snes9x/filter/hq2x.cpp 

C_SRCS += \
../snes9x/filter/snes_ntsc.c 

O_SRCS += \
../snes9x/filter/2xsai.o \
../snes9x/filter/blit.o \
../snes9x/filter/epx.o \
../snes9x/filter/hq2x.o \
../snes9x/filter/snes_ntsc.o 

OBJS += \
./snes9x/filter/2xsai.o \
./snes9x/filter/blit.o \
./snes9x/filter/epx.o \
./snes9x/filter/hq2x.o \
./snes9x/filter/snes_ntsc.o 

CPP_DEPS += \
./snes9x/filter/2xsai.d \
./snes9x/filter/blit.d \
./snes9x/filter/epx.d \
./snes9x/filter/hq2x.d 

C_DEPS += \
./snes9x/filter/snes_ntsc.d 


# Each subdirectory must supply rules for building sources it contributes
snes9x/filter/%.o: ../snes9x/filter/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

snes9x/filter/%.o: ../snes9x/filter/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


