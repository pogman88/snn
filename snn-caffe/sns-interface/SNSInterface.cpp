/*
 * SNSInterface.cpp
 *
 *  Created on: 18 de jul de 2016
 *      Author: fernando
 */

#include "SNSInterface.h"
#include <ctime>

typedef std::pair<std::string, std::string> strpair_t;

ConfigFile::secvec_t keymaps;

#define FIXED_POINT				0x10000
#define FIXED_POINT_SHIFT		16
#define FIXED_POINT_REMAINDER	0xffff
#define SOUND_BUFFER_SIZE		(1024 * 16)
#define SOUND_BUFFER_SIZE_MASK	(SOUND_BUFFER_SIZE - 1)

static volatile bool8 block_signal = FALSE;
static volatile bool8 block_generate_sound = FALSE;

static const char *sound_device = NULL;
static const char *s9x_base_dir = NULL, *rom_filename = NULL,
		*snapshot_filename = NULL, *play_smv_filename = NULL,
		*record_smv_filename = NULL;

static char default_dir[PATH_MAX + 1];

static const char dirNames[13][32] = { "",				// DEFAULT_DIR
		"",				// HOME_DIR
		"",				// ROMFILENAME_DIR
		"rom",			// ROM_DIR
		"sram",			// SRAM_DIR
		"savestate",	// SNAPSHOT_DIR
		"screenshot",	// SCREENSHOT_DIR
		"spc",			// SPC_DIR
		"cheat",		// CHEAT_DIR
		"patch",		// IPS_DIR
		"bios",			// BIOS_DIR
		"log",			// LOG_DIR
		"" };

struct SUnixSettings {
	bool8 JoystickEnabled;
	bool8 ThreadSound;
	uint32 SoundBufferSize;
	uint32 SoundFragmentSize;
};

struct SoundStatus {
	int sound_fd;
	uint32 fragment_size;
	uint32 err_counter;
	uint32 err_rate;
	int32 samples_mixed_so_far;
	int32 play_position;
};

static SUnixSettings unixSettings;
static SoundStatus so;

#ifndef NOSOUND
static uint8 Buf[SOUND_BUFFER_SIZE];
#endif

#ifdef USE_THREADS
static pthread_t thread;
static pthread_mutex_t mutex;
#endif

#ifdef JOYSTICK_SUPPORT
static uint8 js_mod[8] = {0, 0, 0, 0, 0, 0, 0, 0};
static int js_fd[8] = {-1, -1, -1, -1, -1, -1, -1, -1};
static const char *js_device[8] = {"/dev/js0", "/dev/js1", "/dev/js2", "/dev/js3", "/dev/js4", "/dev/js5", "/dev/js6", "/dev/js7"};
#endif

#ifdef NETPLAY_SUPPORT
static uint32 joypads[8];
static uint32 old_joypads[8];
#endif

bool8 S9xMapDisplayInput(const char *, s9xcommand_t *);
s9xcommand_t S9xGetDisplayCommandT(const char *);
char * S9xGetDisplayCommandName(s9xcommand_t);
void S9xHandleDisplayCommand(s9xcommand_t, int16, int16);
bool S9xDisplayPollButton(uint32, bool *);
bool S9xDisplayPollAxis(uint32, int16 *);
bool S9xDisplayPollPointer(uint32, int16 *, int16 *);

static long loge2(long);
static void SoundTrigger(void);
static void InitTimer(void);
static void NSRTControllerSetup(void);
static int make_snes9x_dirs(void);
#ifndef NOSOUND
static void * S9xProcessSound(void *);
#endif
#ifdef JOYSTICK_SUPPORT
static void InitJoysticks (void);
static void ReadJoysticks (void);
#endif

void _splitpath(const char *path, char *drive, char *dir, char *fname,
		char *ext) {
	*drive = 0;

	const char *slash = strrchr(path, SLASH_CHAR), *dot = strrchr(path, '.');

	if (dot && slash && dot < slash)
		dot = NULL;

	if (!slash) {
		*dir = 0;

		strcpy(fname, path);

		if (dot) {
			fname[dot - path] = 0;
			strcpy(ext, dot + 1);
		} else
			*ext = 0;
	} else {
		strcpy(dir, path);
		dir[slash - path] = 0;

		strcpy(fname, slash + 1);

		if (dot) {
			fname[dot - slash - 1] = 0;
			strcpy(ext, dot + 1);
		} else
			*ext = 0;
	}
}

void _makepath(char *path, const char *, const char *dir, const char *fname,
		const char *ext) {
	if (dir && *dir) {
		strcpy(path, dir);
		strcat(path, SLASH_STR);
	} else
		*path = 0;

	strcat(path, fname);

	if (ext && *ext) {
		strcat(path, ".");
		strcat(path, ext);
	}
}

static long loge2(long num) {
	long n = 0;

	while (num >>= 1)
		n++;

	return (n);
}

void S9xExtraUsage(void) {
	/*                               12345678901234567890123456789012345678901234567890123456789012345678901234567890 */

	S9xMessage(S9X_INFO, S9X_USAGE,
			"-multi                          Enable multi cartridge system");
	S9xMessage(S9X_INFO, S9X_USAGE,
			"-carta <filename>               ROM in slot A (use with -multi)");
	S9xMessage(S9X_INFO, S9X_USAGE,
			"-cartb <filename>               ROM in slot B (use with -multi)");
	S9xMessage(S9X_INFO, S9X_USAGE, "");

#ifdef JOYSTICK_SUPPORT
	S9xMessage(S9X_INFO, S9X_USAGE, "-nogamepad                      Disable gamepad reading");
	S9xMessage(S9X_INFO, S9X_USAGE, "-paddev1 <string>               Specify gamepad device 1");
	S9xMessage(S9X_INFO, S9X_USAGE, "-paddev1 <string>               Specify gamepad device 2");
	S9xMessage(S9X_INFO, S9X_USAGE, "-paddev1 <string>               Specify gamepad device 3");
	S9xMessage(S9X_INFO, S9X_USAGE, "-paddev1 <string>               Specify gamepad device 4");
	S9xMessage(S9X_INFO, S9X_USAGE, "");
#endif

#ifdef USE_THREADS
	S9xMessage(S9X_INFO, S9X_USAGE, "-threadsound                    Use a separate thread to output sound");
#endif
	S9xMessage(S9X_INFO, S9X_USAGE,
			"-buffersize                     Sound generating buffer size in millisecond");
	S9xMessage(S9X_INFO, S9X_USAGE,
			"-fragmentsize                   Sound playback buffer fragment size in bytes");
	S9xMessage(S9X_INFO, S9X_USAGE,
			"-sounddev <string>              Specify sound device");
	S9xMessage(S9X_INFO, S9X_USAGE, "");

	S9xMessage(S9X_INFO, S9X_USAGE,
			"-loadsnapshot                   Load snapshot file at start");
	S9xMessage(S9X_INFO, S9X_USAGE,
			"-playmovie <filename>           Start emulator playing the .smv file");
	S9xMessage(S9X_INFO, S9X_USAGE,
			"-recordmovie <filename>         Start emulator recording the .smv file");
	S9xMessage(S9X_INFO, S9X_USAGE,
			"-dumpstreams                    Save audio/video data to disk");
	S9xMessage(S9X_INFO, S9X_USAGE,
			"-dumpmaxframes <num>            Stop emulator after saving specified number of");
	S9xMessage(S9X_INFO, S9X_USAGE,
			"                                frames (use with -dumpstreams)");
	S9xMessage(S9X_INFO, S9X_USAGE, "");

	S9xExtraDisplayUsage();
}

void S9xParseArg(char **argv, int &i, int argc) {
	if (!strcasecmp(argv[i], "-multi"))
		Settings.Multi = TRUE;
	else if (!strcasecmp(argv[i], "-carta")) {
		if (i + 1 < argc)
			strncpy(Settings.CartAName, argv[++i], _MAX_PATH);
		else
			S9xUsage();
	} else if (!strcasecmp(argv[i], "-cartb")) {
		if (i + 1 < argc)
			strncpy(Settings.CartBName, argv[++i], _MAX_PATH);
		else
			S9xUsage();
	} else
#ifdef JOYSTICK_SUPPORT
	if (!strcasecmp(argv[i], "-nogamepad"))
	unixSettings.JoystickEnabled = FALSE;
	else
	if (!strncasecmp(argv[i], "-paddev", 7) &&
			argv[i][7] >= '1' && argv[i][7] <= '8' && argv[i][8] == '\0')
	{
		int j = argv[i][7] - '1';

		if (i + 1 < argc)
		js_device[j] = argv[++i];
		else
		S9xUsage();
	}
	else
#endif
#ifdef USE_THREADS
	if (!strcasecmp(argv[i], "-threadsound"))
	unixSettings.ThreadSound = TRUE;
	else
#endif
	if (!strcasecmp(argv[i], "-buffersize")) {
		if (i + 1 < argc)
			unixSettings.SoundBufferSize = atoi(argv[++i]);
		else
			S9xUsage();
	} else if (!strcasecmp(argv[i], "-fragmentsize")) {
		if (i + 1 < argc)
			unixSettings.SoundFragmentSize = atoi(argv[++i]);
		else
			S9xUsage();
	} else if (!strcasecmp(argv[i], "-sounddev")) {
		if (i + 1 < argc)
			sound_device = argv[++i];
		else
			S9xUsage();
	} else if (!strcasecmp(argv[i], "-loadsnapshot")) {
		if (i + 1 < argc)
			snapshot_filename = argv[++i];
		else
			S9xUsage();
	} else if (!strcasecmp(argv[i], "-playmovie")) {
		if (i + 1 < argc)
			play_smv_filename = argv[++i];
		else
			S9xUsage();
	} else if (!strcasecmp(argv[i], "-recordmovie")) {
		if (i + 1 < argc)
			record_smv_filename = argv[++i];
		else
			S9xUsage();
	} else if (!strcasecmp(argv[i], "-dumpstreams"))
		Settings.DumpStreams = TRUE;
	else if (!strcasecmp(argv[i], "-dumpmaxframes"))
		Settings.DumpStreamsMaxFrames = atoi(argv[++i]);
	else
		S9xParseDisplayArg(argv, i, argc);
}

static void NSRTControllerSetup(void) {
	if (!strncmp((const char *) Memory.NSRTHeader + 24, "NSRT", 4)) {
		// First plug in both, they'll change later as needed
		S9xSetController(0, CTL_JOYPAD, 0, 0, 0, 0);
		S9xSetController(1, CTL_JOYPAD, 1, 0, 0, 0);

		switch (Memory.NSRTHeader[29]) {
		case 0x00:	// Everything goes
			break;

		case 0x10:	// Mouse in Port 0
			S9xSetController(0, CTL_MOUSE, 0, 0, 0, 0);
			break;

		case 0x01:	// Mouse in Port 1
			S9xSetController(1, CTL_MOUSE, 1, 0, 0, 0);
			break;

		case 0x03:	// Super Scope in Port 1
			S9xSetController(1, CTL_SUPERSCOPE, 0, 0, 0, 0);
			break;

		case 0x06:	// Multitap in Port 1
			S9xSetController(1, CTL_MP5, 1, 2, 3, 4);
			break;

		case 0x66:	// Multitap in Ports 0 and 1
			S9xSetController(0, CTL_MP5, 0, 1, 2, 3);
			S9xSetController(1, CTL_MP5, 4, 5, 6, 7);
			break;

		case 0x08:	// Multitap in Port 1, Mouse in new Port 1
			S9xSetController(1, CTL_MOUSE, 1, 0, 0, 0);
			// There should be a toggle here for putting in Multitap instead
			break;

		case 0x04:	// Pad or Super Scope in Port 1
			S9xSetController(1, CTL_SUPERSCOPE, 0, 0, 0, 0);
			// There should be a toggle here for putting in a pad instead
			break;

		case 0x05:	// Justifier - Must ask user...
			S9xSetController(1, CTL_JUSTIFIER, 1, 0, 0, 0);
			// There should be a toggle here for how many justifiers
			break;

		case 0x20:	// Pad or Mouse in Port 0
			S9xSetController(0, CTL_MOUSE, 0, 0, 0, 0);
			// There should be a toggle here for putting in a pad instead
			break;

		case 0x22:	// Pad or Mouse in Port 0 & 1
			S9xSetController(0, CTL_MOUSE, 0, 0, 0, 0);
			S9xSetController(1, CTL_MOUSE, 1, 0, 0, 0);
			// There should be a toggles here for putting in pads instead
			break;

		case 0x24:	// Pad or Mouse in Port 0, Pad or Super Scope in Port 1
			// There should be a toggles here for what to put in, I'm leaving it at gamepad for now
			break;

		case 0x27:// Pad or Mouse in Port 0, Pad or Mouse or Super Scope in Port 1
			// There should be a toggles here for what to put in, I'm leaving it at gamepad for now
			break;

			// Not Supported yet
		case 0x99:	// Lasabirdie
			break;

		case 0x0A:	// Barcode Battler
			break;
		}
	}
}

void S9xParsePortConfig(ConfigFile &conf, int pass) {
	s9x_base_dir = conf.GetStringDup("Unix::BaseDir", default_dir);
	snapshot_filename = conf.GetStringDup("Unix::SnapshotFilename", NULL);
	play_smv_filename = conf.GetStringDup("Unix::PlayMovieFilename", NULL);
	record_smv_filename = conf.GetStringDup("Unix::RecordMovieFilename", NULL);

#ifdef JOYSTICK_SUPPORT
	unixSettings.JoystickEnabled = conf.GetBool ("Unix::EnableGamePad", true);
	js_device[0] = conf.GetStringDup("Unix::PadDevice1", NULL);
	js_device[1] = conf.GetStringDup("Unix::PadDevice2", NULL);
	js_device[2] = conf.GetStringDup("Unix::PadDevice3", NULL);
	js_device[3] = conf.GetStringDup("Unix::PadDevice4", NULL);
	js_device[4] = conf.GetStringDup("Unix::PadDevice5", NULL);
	js_device[5] = conf.GetStringDup("Unix::PadDevice6", NULL);
	js_device[6] = conf.GetStringDup("Unix::PadDevice7", NULL);
	js_device[7] = conf.GetStringDup("Unix::PadDevice8", NULL);
#endif

#ifdef USE_THREADS
	unixSettings.ThreadSound = conf.GetBool ("Unix::ThreadSound", false);
#endif
	unixSettings.SoundBufferSize = conf.GetUInt("Unix::SoundBufferSize", 100);
	unixSettings.SoundFragmentSize = conf.GetUInt("Unix::SoundFragmentSize",
			2048);
	sound_device = conf.GetStringDup("Unix::SoundDevice", "/dev/dsp");

	keymaps.clear();
	if (!conf.GetBool("Unix::ClearAllControls", false)) {
#if 0
		// Using an axis to control Pseudo-pointer #1
		keymaps.push_back(strpair_t("J00:Axis0", "AxisToPointer 1h Var"));
		keymaps.push_back(strpair_t("J00:Axis1", "AxisToPointer 1v Var"));
		keymaps.push_back(strpair_t("PseudoPointer1", "Pointer C=2 White/Black Superscope"));
#elif 0
		// Using an Axis for Pseudo-buttons
		keymaps.push_back(strpair_t("J00:Axis0", "AxisToButtons 1/0 T=50%"));
		keymaps.push_back(strpair_t("J00:Axis1", "AxisToButtons 3/2 T=50%"));
		keymaps.push_back(strpair_t("PseudoButton0", "Joypad1 Right"));
		keymaps.push_back(strpair_t("PseudoButton1", "Joypad1 Left"));
		keymaps.push_back(strpair_t("PseudoButton2", "Joypad1 Down"));
		keymaps.push_back(strpair_t("PseudoButton3", "Joypad1 Up"));
#else
		// Using 'Joypad# Axis'
		keymaps.push_back(
				strpair_t("J00:Axis0", "Joypad1 Axis Left/Right T=50%"));
		keymaps.push_back(strpair_t("J00:Axis1", "Joypad1 Axis Up/Down T=50%"));
#endif
		keymaps.push_back(strpair_t("J00:B0", "Joypad1 X"));
		keymaps.push_back(strpair_t("J00:B1", "Joypad1 A"));
		keymaps.push_back(strpair_t("J00:B2", "Joypad1 B"));
		keymaps.push_back(strpair_t("J00:B3", "Joypad1 Y"));
#if 1
		keymaps.push_back(strpair_t("J00:B6", "Joypad1 L"));
#else
		// Show off joypad-meta
		keymaps.push_back(strpair_t("J00:X+B6", "JS1 Meta1"));
		keymaps.push_back(strpair_t("J00:M1+B1", "Joypad1 Turbo A"));
#endif
		keymaps.push_back(strpair_t("J00:B7", "Joypad1 R"));
		keymaps.push_back(strpair_t("J00:B8", "Joypad1 Select"));
		keymaps.push_back(strpair_t("J00:B11", "Joypad1 Start"));
	}

	std::string section = S9xParseDisplayConfig(conf, 1);

	ConfigFile::secvec_t sec = conf.GetSection((section + " Controls").c_str());
	for (ConfigFile::secvec_t::iterator c = sec.begin(); c != sec.end(); c++)
		keymaps.push_back(*c);
}

static int make_snes9x_dirs(void) {
	if (strlen(s9x_base_dir) + 1 + sizeof(dirNames[0]) > PATH_MAX + 1)
		return (-1);

	mkdir(s9x_base_dir, 0755);

	for (int i = 0; i < LAST_DIR; i++) {
		if (dirNames[i][0]) {
			char s[PATH_MAX + 1];
			snprintf(s, PATH_MAX + 1, "%s%s%s", s9x_base_dir, SLASH_STR,
					dirNames[i]);
			mkdir(s, 0755);
		}
	}

	return (0);
}

const char * S9xGetDirectory(enum s9x_getdirtype dirtype) {
	static char s[PATH_MAX + 1];

	if (dirNames[dirtype][0])
		snprintf(s, PATH_MAX + 1, "%s%s%s", s9x_base_dir, SLASH_STR,
				dirNames[dirtype]);
	else {
		switch (dirtype) {
		case DEFAULT_DIR:
			strncpy(s, s9x_base_dir, PATH_MAX + 1);
			s[PATH_MAX] = 0;
			break;

		case HOME_DIR:
			strncpy(s, getenv("HOME"), PATH_MAX + 1);
			s[PATH_MAX] = 0;
			break;

		case ROMFILENAME_DIR:
			strncpy(s, Memory.ROMFilename, PATH_MAX + 1);
			s[PATH_MAX] = 0;

			for (int i = strlen(s); i >= 0; i--) {
				if (s[i] == SLASH_CHAR) {
					s[i] = 0;
					break;
				}
			}

			break;

		default:
			s[0] = 0;
			break;
		}
	}

	return (s);
}

const char * S9xGetFilename(const char *ex, enum s9x_getdirtype dirtype) {
	static char s[PATH_MAX + 1];
	char drive[_MAX_DRIVE + 1], dir[_MAX_DIR + 1], fname[_MAX_FNAME + 1],
			ext[_MAX_EXT + 1];

	_splitpath(Memory.ROMFilename, drive, dir, fname, ext);
	snprintf(s, PATH_MAX + 1, "%s%s%s%s", S9xGetDirectory(dirtype), SLASH_STR,
			fname, ex);

	return (s);
}

const char * S9xGetFilenameInc(const char *ex, enum s9x_getdirtype dirtype) {
	static char s[PATH_MAX + 1];
	char drive[_MAX_DRIVE + 1], dir[_MAX_DIR + 1], fname[_MAX_FNAME + 1],
			ext[_MAX_EXT + 1];

	unsigned int i = 0;
	const char *d;
	struct stat buf;

	_splitpath(Memory.ROMFilename, drive, dir, fname, ext);
	d = S9xGetDirectory(dirtype);

	do
		snprintf(s, PATH_MAX + 1, "%s%s%s.%03d%s", d, SLASH_STR, fname, i++,
				ex);
	while (stat(s, &buf) == 0 && i < 1000);

	return (s);
}

const char * S9xBasename(const char *f) {
	const char *p;

	if ((p = strrchr(f, '/')) != NULL || (p = strrchr(f, '\\')) != NULL)
		return (p + 1);

	return (f);
}

const char * S9xChooseFilename(bool8 read_only) {
	char s[PATH_MAX + 1];
	char drive[_MAX_DRIVE + 1], dir[_MAX_DIR + 1], fname[_MAX_FNAME + 1],
			ext[_MAX_EXT + 1];

	const char *filename;
	char title[64];

	_splitpath(Memory.ROMFilename, drive, dir, fname, ext);
	snprintf(s, PATH_MAX + 1, "%s.frz", fname);
	sprintf(title, "%s snapshot filename",
			read_only ? "Select load" : "Choose save");

	S9xSetSoundMute(TRUE);
	filename = S9xSelectFilename(s, S9xGetDirectory(SNAPSHOT_DIR), "frz",
			title);
	S9xSetSoundMute(FALSE);

	return (filename);
}

const char * S9xChooseMovieFilename(bool8 read_only) {
	char s[PATH_MAX + 1];
	char drive[_MAX_DRIVE + 1], dir[_MAX_DIR + 1], fname[_MAX_FNAME + 1],
			ext[_MAX_EXT + 1];

	const char *filename;
	char title[64];

	_splitpath(Memory.ROMFilename, drive, dir, fname, ext);
	snprintf(s, PATH_MAX + 1, "%s.smv", fname);
	sprintf(title, "Choose movie %s filename",
			read_only ? "playback" : "record");

	S9xSetSoundMute(TRUE);
	filename = S9xSelectFilename(s, S9xGetDirectory(HOME_DIR), "smv", title);
	S9xSetSoundMute(FALSE);

	return (filename);
}

bool8 S9xOpenSnapshotFile(const char *filename, bool8 read_only, STREAM *file)
{
	char s[PATH_MAX + 1];
	char drive[_MAX_DRIVE + 1], dir[_MAX_DIR + 1], fname[_MAX_FNAME + 1], ext[_MAX_EXT + 1];

	_splitpath(filename, drive, dir, fname, ext);

	if (*drive || *dir == SLASH_CHAR || (strlen(dir) > 1 && *dir == '.' && *(dir + 1) == SLASH_CHAR))
	{
		strncpy(s, filename, PATH_MAX + 1);
		s[PATH_MAX] = 0;
	}
	else
	snprintf(s, PATH_MAX + 1, "%s%s%s", S9xGetDirectory(SNAPSHOT_DIR), SLASH_STR, fname);

	if (!*ext && strlen(s) <= PATH_MAX - 4)
	strcat(s, ".frz");

	if ((*file = OPEN_STREAM(s, read_only ? "rb" : "wb")))
	return (TRUE);

	return (FALSE);
}

void S9xCloseSnapshotFile(STREAM file)
{
	CLOSE_STREAM(file);
}

bool8 S9xInitUpdate(void) {
	return (TRUE);
}

bool8 S9xDeinitUpdate(int width, int height) {
	S9xPutImage(width, height);
	return (TRUE);
}

bool8 S9xContinueUpdate(int width, int height) {
	return (TRUE);
}

void S9xToggleSoundChannel(int c) {
	static uint8 sound_switch = 255;

	if (c == 8)
		sound_switch = 255;
	else
		sound_switch ^= 1 << c;

	S9xSetSoundControl(sound_switch);
}

void S9xAutoSaveSRAM(void) {
	Memory.SaveSRAM(S9xGetFilename(".srm", SRAM_DIR));
}

//#include <opencv/highgui.h>

void S9xSyncSpeed(void) {

	IPPU.FrameSkip = 0;
	IPPU.SkippedFrames = 0;
	IPPU.RenderThisFrame = TRUE;

	//cv::waitKey(50);
	return;

#ifndef NOSOUND
	if (Settings.SoundSync) {
		while (!S9xSyncSound())
			usleep(0);
	}
#endif

	if (Settings.DumpStreams)
		return;

#ifdef NETPLAY_SUPPORT
	if (Settings.NetPlay && NetPlay.Connected)
	{
#if defined(NP_DEBUG) && NP_DEBUG == 2
		printf("CLIENT: SyncSpeed @%d\n", S9xGetMilliTime());
#endif

		S9xNPSendJoypadUpdate(old_joypads[0]);
		for (int J = 0; J < 8; J++)
		joypads[J] = S9xNPGetJoypad(J);

		if (!S9xNPCheckForHeartBeat())
		{
			NetPlay.PendingWait4Sync = !S9xNPWaitForHeartBeatDelay(100);
#if defined(NP_DEBUG) && NP_DEBUG == 2
			if (NetPlay.PendingWait4Sync)
			printf("CLIENT: PendingWait4Sync1 @%d\n", S9xGetMilliTime());
#endif

			IPPU.RenderThisFrame = TRUE;
			IPPU.SkippedFrames = 0;
		}
		else
		{
			NetPlay.PendingWait4Sync = !S9xNPWaitForHeartBeatDelay(200);
#if defined(NP_DEBUG) && NP_DEBUG == 2
			if (NetPlay.PendingWait4Sync)
			printf("CLIENT: PendingWait4Sync2 @%d\n", S9xGetMilliTime());
#endif

			if (IPPU.SkippedFrames < NetPlay.MaxFrameSkip)
			{
				IPPU.RenderThisFrame = FALSE;
				IPPU.SkippedFrames++;
			}
			else
			{
				IPPU.RenderThisFrame = TRUE;
				IPPU.SkippedFrames = 0;
			}
		}

		if (!NetPlay.PendingWait4Sync)
		{
			NetPlay.FrameCount++;
			S9xNPStepJoypadHistory();
		}

		return;
	}
#endif

	if (Settings.HighSpeedSeek > 0)
		Settings.HighSpeedSeek--;

	if (Settings.TurboMode) {
		if ((++IPPU.FrameSkip >= Settings.TurboSkipFrames)
				&& !Settings.HighSpeedSeek) {
			IPPU.FrameSkip = 0;
			IPPU.SkippedFrames = 0;
			IPPU.RenderThisFrame = TRUE;
		} else {
			IPPU.SkippedFrames++;
			IPPU.RenderThisFrame = FALSE;
		}

		return;
	}

	static struct timeval next1 = { 0, 0 };
	struct timeval now;

	while (gettimeofday(&now, NULL) == -1)
		;

	// If there is no known "next" frame, initialize it now.
	if (next1.tv_sec == 0) {
		next1 = now;
		next1.tv_usec++;
	}

	// If we're on AUTO_FRAMERATE, we'll display frames always only if there's excess time.
	// Otherwise we'll display the defined amount of frames.
	unsigned limit =
			(Settings.SkipFrames == AUTO_FRAMERATE) ?
					(timercmp(&next1, &now, <) ? 10 : 1) : Settings.SkipFrames;

	IPPU.RenderThisFrame = (++IPPU.SkippedFrames >= limit) ? TRUE : FALSE;

	if (IPPU.RenderThisFrame)
		IPPU.SkippedFrames = 0;
	else {
		// If we were behind the schedule, check how much it is.
		if (timercmp(&next1, &now, <)) {
			unsigned lag = (now.tv_sec - next1.tv_sec) * 1000000 + now.tv_usec
					- next1.tv_usec;
			if (lag >= 500000) {
				// More than a half-second behind means probably pause.
				// The next line prevents the magic fast-forward effect.
				next1 = now;
			}
		}
	}

	// Delay until we're completed this frame.
	// Can't use setitimer because the sound code already could be using it. We don't actually need it either.
	while (timercmp(&next1, &now, >)) {
		// If we're ahead of time, sleep a while.
		unsigned timeleft = (next1.tv_sec - now.tv_sec) * 1000000
				+ next1.tv_usec - now.tv_usec;
		usleep(timeleft);

		while (gettimeofday(&now, NULL) == -1)
			;
		// Continue with a while-loop because usleep() could be interrupted by a signal.
	}

	// Calculate the timestamp of the next frame.
	next1.tv_usec += Settings.FrameTime;
	if (next1.tv_usec >= 1000000) {
		next1.tv_sec += next1.tv_usec / 1000000;
		next1.tv_usec %= 1000000;
	}
}

bool8 S9xMapInput(const char *n, s9xcommand_t *cmd) {
	int i, j, d;
	char *c;
	char buf[4] = "M1+";

	if (!strncmp(n, "PseudoPointer", 13) && n[13] >= '1' && n[13] <= '8'
			&& n[14] == '\0')
		return (S9xMapPointer(PseudoPointerBase + (n[13] - '1'), *cmd, false));

	if (!strncmp(n, "PseudoButton", 12) && isdigit(n[12])
			&& (j = strtol(n + 12, &c, 10)) < 256 && (c == NULL || *c == '\0'))
		return (S9xMapButton(PseudoButtonBase + j, *cmd, false));

	if (n[0] != 'J' || !isdigit(n[1]) || !isdigit(n[2]) || n[3] != ':')
		goto unrecog;

	d = ((n[1] - '0') * 10 + (n[2] - '0')) << 24;
	d |= 0x80000000;
	i = 4;
	if (!strncmp(n + i, "X+", 2)) {
		d |= 0x4000;
		i += 2;
	} else {
		for (buf[1] = '1'; buf[1] <= '8'; buf[1]++) {
			if (!strncmp(n + i, buf, 3)) {
				d |= 1 << (buf[1] - '1' + 16);
				i += 3;
			}
		}
	}

	if (!strncmp(n + i, "Axis", 4)) {
		d |= 0x8000;
		i += 4;
	} else if (n[i] == 'B')
		i++;
	else
		goto unrecog;

	d |= j = strtol(n + i, &c, 10);
	if ((c != NULL && *c != '\0') || j > 0x3fff)
		goto unrecog;

	if (d & 0x8000)
		return (S9xMapAxis(d, *cmd, false));

	return (S9xMapButton(d, *cmd, false));

	unrecog: return (S9xMapDisplayInput(n, cmd));
}

bool S9xPollButton(uint32 id, bool *pressed) {
	return (S9xDisplayPollButton(id, pressed));
}

bool S9xPollAxis(uint32 id, int16 *value) {
	return (S9xDisplayPollAxis(id, value));
}

bool S9xPollPointer(uint32 id, int16 *x, int16 *y) {
	return (S9xDisplayPollPointer(id, x, y));
}

s9xcommand_t S9xGetPortCommandT(const char *n) {
	s9xcommand_t cmd;

	cmd.type = S9xBadMapping;
	cmd.multi_press = 0;
	cmd.button_norpt = 0;
	cmd.port[0] = 0;
	cmd.port[1] = 0;
	cmd.port[2] = 0;
	cmd.port[3] = 0;

	if (!strncmp(n, "JS", 2) && n[2] >= '1' && n[2] <= '8') {
		if (!strncmp(n + 3, " Meta", 5) && n[8] >= '1' && n[8] <= '8'
				&& n[9] == '\0') {
			cmd.type = S9xButtonPort;
			cmd.port[1] = 0;
			cmd.port[2] = n[2] - '1';
			cmd.port[3] = 1 << (n[8] - '1');

			return (cmd);
		} else if (!strncmp(n + 3, " ToggleMeta", 11) && n[14] >= '1'
				&& n[14] <= '8' && n[15] == '\0') {
			cmd.type = S9xButtonPort;
			cmd.port[1] = 1;
			cmd.port[2] = n[2] - '1';
			cmd.port[3] = 1 << (n[13] - '1');

			return (cmd);
		}
	}

	return (S9xGetDisplayCommandT(n));
}

char * S9xGetPortCommandName(s9xcommand_t cmd) {
	std::string x;

	switch (cmd.type) {
	case S9xButtonPort:
		if (cmd.port[0] != 0)
			break;

		switch (cmd.port[1]) {
		case 0:
			x = "JS";
			x += (int) cmd.port[2];
			x += " Meta";
			x += (int) cmd.port[3];
			return (strdup(x.c_str()));

		case 1:
			x = "JS";
			x += (int) cmd.port[2];
			x += " ToggleMeta";
			x += (int) cmd.port[3];
			return (strdup(x.c_str()));
		}

		break;

	case S9xAxisPort:
		break;

	case S9xPointerPort:
		break;
	}

	return (S9xGetDisplayCommandName(cmd));
}

void S9xHandlePortCommand(s9xcommand_t cmd, int16 data1, int16 data2) {
#ifdef JOYSTICK_SUPPORT
	switch (cmd.type)
	{
		case S9xButtonPort:
		if (cmd.port[0] != 0)
		break;

		switch (cmd.port[1])
		{
			case 0:
			if (data1)
			js_mod[cmd.port[2]] |= cmd.port[3];
			else
			js_mod[cmd.port[2]] &= ~cmd.port[3];
			break;

			case 1:
			if (data1)
			js_mod[cmd.port[2]] ^= cmd.port[3];
			break;
		}

		break;

		case S9xAxisPort:
		break;

		case S9xPointerPort:
		break;
	}

	S9xHandleDisplayCommand(cmd, data1, data2);
#endif
}

void S9xSetupDefaultKeymap(void) {
	s9xcommand_t cmd;

	S9xUnmapAllControls();

	for (ConfigFile::secvec_t::iterator i = keymaps.begin(); i != keymaps.end();
			i++) {
		cmd = S9xGetPortCommandT(i->second.c_str());

		if (cmd.type == S9xBadMapping) {
			cmd = S9xGetCommandT(i->second.c_str());
			if (cmd.type == S9xBadMapping) {
				std::string s("Unrecognized command '");
				s += i->second + "'";
				perror(s.c_str());
				continue;
			}
		}

		if (!S9xMapInput(i->first.c_str(), &cmd)) {
			std::string s("Could not map '");
			s += i->second + "' to '" + i->first + "'";
			perror(s.c_str());
			continue;
		}
	}

	keymaps.clear();
}

void S9xInitInputDevices(void) {
#ifdef JOYSTICK_SUPPORT
	InitJoysticks();
#endif
}

#ifdef JOYSTICK_SUPPORT

static void InitJoysticks (void)
{
#ifdef JSIOCGVERSION
	int version;
	unsigned char axes, buttons;

	if ((js_fd[0] = open(js_device[0], O_RDONLY | O_NONBLOCK)) == -1)
	{
		fprintf(stderr, "joystick: No joystick found.\n");
		return;
	}

	if (ioctl(js_fd[0], JSIOCGVERSION, &version) == -1)
	{
		fprintf(stderr, "joystick: You need at least driver version 1.0 for joystick support.\n");
		close(js_fd[0]);
		return;
	}

	for (int i = 1; i < 8; i++)
	js_fd[i] = open(js_device[i], O_RDONLY | O_NONBLOCK);

#ifdef JSIOCGNAME
	char name[130];

	bzero(name, 128);

	if (ioctl(js_fd[0], JSIOCGNAME(128), name) > 0)
	{
		printf("Using %s (%s) as joystick1\n", name, js_device[0]);

		for (int i = 1; i < 8; i++)
		{
			if (js_fd[i] > 0)
			{
				ioctl(js_fd[i], JSIOCGNAME(128), name);
				printf ("  and %s (%s) as joystick%d\n", name, js_device[i], i + 1);
			}
		}
	}
	else
#endif
	{
		ioctl(js_fd[0], JSIOCGAXES, &axes);
		ioctl(js_fd[0], JSIOCGBUTTONS, &buttons);
		printf("Using %d-axis %d-button joystick (%s) as joystick1\n", axes, buttons, js_device[0]);

		for (int i = 1; i < 8; i++)
		{
			if (js_fd[i] > 0)
			{
				ioctl(js_fd[i], JSIOCGAXES, &axes);
				ioctl(js_fd[i], JSIOCGBUTTONS, &buttons);
				printf("  and %d-axis %d-button joystick (%s) as joystick%d\n", axes, buttons, js_device[i], i + 1);
			}
		}
	}
#endif
}

static void ReadJoysticks (void)
{
#ifdef JSIOCGVERSION
	struct js_event js_ev;

	for (int i = 0; i < 8 && js_fd[i] >= 0; i++)
	{
		while (read(js_fd[i], &js_ev, sizeof(struct js_event)) == sizeof(struct js_event))
		{
			switch (js_ev.type & ~JS_EVENT_INIT)
			{
				case JS_EVENT_AXIS:
				S9xReportAxis(0x8000c000 | (i << 24) | js_ev.number, js_ev.value);
				S9xReportAxis(0x80008000 | (i << 24) | (js_mod[i] << 16) | js_ev.number, js_ev.value);
				break;

				case JS_EVENT_BUTTON:
				S9xReportButton(0x80004000 | (i << 24) | js_ev.number, js_ev.value);
				S9xReportButton(0x80000000 | (i << 24) | (js_mod[i] << 16) | js_ev.number, js_ev.value);
				break;
			}
		}
	}
#endif
}

#endif

static void SoundTrigger(void) {
#ifndef NOSOUND
	S9xProcessSound(NULL);
#endif
}

static void InitTimer(void) {
#ifndef NOSOUND
	struct itimerval timeout;
#endif
	struct sigaction sa;

#ifdef USE_THREADS
	if (unixSettings.ThreadSound)
	{
		pthread_mutex_init(&mutex, NULL);
		pthread_create(&thread, NULL, S9xProcessSound, NULL);
		return;
	}
#endif

	sa.sa_handler = (SIG_PF) SoundTrigger;

#ifdef SA_RESTART
	sa.sa_flags = SA_RESTART;
#else
	sa.sa_flags = 0;
#endif

#ifndef NOSOUND // FIXME: Kludge to get calltree running. Remove later.
	sigemptyset(&sa.sa_mask);
	sigaction(SIGALRM, &sa, NULL);

	timeout.it_interval.tv_sec = 0;
	timeout.it_interval.tv_usec = 10000;
	timeout.it_value.tv_sec = 0;
	timeout.it_value.tv_usec = 10000;
	if (setitimer(ITIMER_REAL, &timeout, NULL) < 0)
		perror("setitimer");
#endif
}

bool8 S9xOpenSoundDevice(void) {
#ifndef NOSOUND
	int J, K;

	so.sound_fd = open(sound_device, O_WRONLY | O_NONBLOCK);
	if (so.sound_fd == -1)
		return (FALSE);

	J = loge2(unixSettings.SoundFragmentSize) | (3 << 16);
	if (ioctl(so.sound_fd, SNDCTL_DSP_SETFRAGMENT, &J) == -1)
		return (FALSE);

	J = K = Settings.SixteenBitSound ? AFMT_S16_NE : AFMT_U8;
	if (ioctl(so.sound_fd, SNDCTL_DSP_SETFMT, &J) == -1 || J != K)
		return (FALSE);

	J = K = Settings.Stereo ? 1 : 0;
	if (ioctl(so.sound_fd, SNDCTL_DSP_STEREO, &J) == -1 || J != K)
		return (FALSE);

	J = K = Settings.SoundPlaybackRate;
	if (ioctl(so.sound_fd, SNDCTL_DSP_SPEED, &J) == -1 || J != K)
		return (FALSE);

	J = 0;
	if (ioctl(so.sound_fd, SNDCTL_DSP_GETBLKSIZE, &J) == -1)
		return (FALSE);

	so.fragment_size = J;
	printf("fragment size: %d\n", J);
#endif

	return (TRUE);
}

#ifndef NOSOUND

static void * S9xProcessSound(void *) {
	// If threads in use, this is to loop indefinitely.
	// If not, this will be called by timer.

	audio_buf_info info;
	if (!unixSettings.ThreadSound
			&& (ioctl(so.sound_fd, SNDCTL_DSP_GETOSPACE, &info) == -1
					|| info.bytes < (int) so.fragment_size))
		return (NULL);

#ifdef USE_THREADS
	do
	{
#endif

	int sample_count = so.fragment_size;
	if (Settings.SixteenBitSound)
		sample_count >>= 1;

#ifdef USE_THREADS
	if (unixSettings.ThreadSound)
	pthread_mutex_lock(&mutex);
	else
#endif
	if (block_signal)
		return (NULL);

	block_generate_sound = TRUE;

	if (so.samples_mixed_so_far < sample_count) {
		unsigned ofs =
				so.play_position
						+ (Settings.SixteenBitSound ?
								(so.samples_mixed_so_far << 1) :
								so.samples_mixed_so_far);
		S9xMixSamples(Buf + (ofs & SOUND_BUFFER_SIZE_MASK),
				sample_count - so.samples_mixed_so_far);
		so.samples_mixed_so_far = sample_count;
	}

	unsigned bytes_to_write = sample_count;
	if (Settings.SixteenBitSound)
		bytes_to_write <<= 1;

	unsigned byte_offset = so.play_position;
	so.play_position += bytes_to_write;
	so.play_position &= SOUND_BUFFER_SIZE_MASK;

#ifdef USE_THREADS
	if (unixSettings.ThreadSound)
	pthread_mutex_unlock(&mutex);
#endif

	block_generate_sound = FALSE;

	for (;;) {
		int I = bytes_to_write;
		if (byte_offset + I > SOUND_BUFFER_SIZE)
			I = SOUND_BUFFER_SIZE - byte_offset;
		if (I == 0)
			break;

		I = write(so.sound_fd, (char *) Buf + byte_offset, I);
		if (I > 0) {
			bytes_to_write -= I;
			byte_offset += I;
			byte_offset &= SOUND_BUFFER_SIZE_MASK;
		} else if (I < 0 && errno != EINTR)
			break;
	}

	so.samples_mixed_so_far -= sample_count;

#ifdef USE_THREADS
}while (unixSettings.ThreadSound);
#endif

	return (NULL);
}

#endif

void S9xExit(void) {
	S9xMovieShutdown();

	S9xSetSoundMute(TRUE);
	Settings.StopEmulation = TRUE;

#ifdef NETPLAY_SUPPORT
	if (Settings.NetPlay)
	S9xNPDisconnect();
#endif

	Memory.SaveSRAM(S9xGetFilename(".srm", SRAM_DIR));
	S9xSaveCheatFile(S9xGetFilename(".cht", CHEAT_DIR));
	S9xResetSaveTimer(FALSE);

	S9xUnmapAllControls();
	S9xDeinitDisplay();
	Memory.Deinit();
	S9xDeinitAPU();

	exit(0);
}

#ifdef DEBUGGER
static void sigbrkhandler (int)
{
	CPU.Flags |= DEBUG_MODE_FLAG;
	//signal(SIGINT, (SIG_PF) sigbrkhandler);
}
#endif

SNSInterface::SNSInterface(char* rom_path) {
	m_rom = NULL;
	m_snn_initiated = false;
	m_rom_loaded = false;
	m_framerate = DEFAULT_FRAMERATE;
	m_argc = 2;
	m_argv = (char**)malloc(2*sizeof(char*));
	m_argv[0] = (char*)malloc(64*sizeof(char));
	m_argv[1] = (char*)malloc((strlen(rom_path)+1)*sizeof(char));
	strcpy(m_argv[0], "./sns");
	strcpy(m_argv[1], rom_path);
	m_config = SNSConfig();

	// Initialize with 0 in all positions
	m_buttons_map = (int*) calloc(TOTAL_BUTTONS, sizeof(int));
	m_bot_behavior = -1;
	m_currentRomState = 0;
}

SNSInterface::~SNSInterface() {
	free(m_argv[0]);
	free(m_argv[1]);
	free(m_argv);

	S9xMovieShutdown();

	S9xSetSoundMute(TRUE);
	Settings.StopEmulation = TRUE;

#ifdef NETPLAY_SUPPORT
	if (Settings.NetPlay)
	S9xNPDisconnect();
#endif

	Memory.SaveSRAM(S9xGetFilename(".srm", SRAM_DIR));
	S9xSaveCheatFile(S9xGetFilename(".cht", CHEAT_DIR));
	S9xResetSaveTimer(FALSE);

	S9xUnmapAllControls();
	S9xDeinitDisplay();
	Memory.Deinit();
	S9xDeinitAPU();
}

void SNSInterface::init() {
	if (m_snn_initiated) {
		return;
	}

	loadConfig(createDefaultConfig());

	snprintf(default_dir, PATH_MAX + 1, "%s%s%s", getenv("HOME"), SLASH_STR,
			".snes9x");
	s9x_base_dir = default_dir;

	ZeroMemory(&Settings, sizeof(Settings));
	Settings.MouseMaster = TRUE;
	Settings.SuperScopeMaster = TRUE;
	Settings.JustifierMaster = TRUE;
	Settings.MultiPlayer5Master = TRUE;
	Settings.FrameTimePAL = 20000;
	Settings.FrameTimeNTSC = m_config.framerate;
	Settings.SixteenBitSound = TRUE;
	Settings.Stereo = TRUE;
	Settings.SoundPlaybackRate = 32000;
	Settings.SoundInputRate = 32000;
	Settings.SupportHiRes = TRUE;
	Settings.Transparency = TRUE;
	Settings.AutoDisplayMessages = TRUE;
	Settings.InitialInfoStringTimeout = 120;
	Settings.HDMATimingHack = 100;
	Settings.BlockInvalidVRAMAccessMaster = TRUE;
	Settings.StopEmulation = TRUE;
	Settings.WrongMovieStateProtection = TRUE;
	Settings.DumpStreamsMaxFrames = -1;
	Settings.StretchScreenshots = 1;
	Settings.SnapshotScreenshots = TRUE;
	Settings.SkipFrames = AUTO_FRAMERATE;
	Settings.TurboSkipFrames = 0;
	Settings.CartAName[0] = 0;
	Settings.CartBName[0] = 0;

#ifdef NETPLAY_SUPPORT
	Settings.ServerName[0] = 0;
#endif

#ifdef JOYSTICK_SUPPORT
	unixSettings.JoystickEnabled = TRUE;
#else
	unixSettings.JoystickEnabled = FALSE;
#endif

	unixSettings.ThreadSound = TRUE;
	unixSettings.SoundBufferSize = 100;
	unixSettings.SoundFragmentSize = 2048;

	ZeroMemory(&so, sizeof(so));

	CPU.Flags = 0;
	S9xLoadConfigFiles(m_argv, m_argc);
	rom_filename = S9xParseArgs(m_argv, m_argc);

	make_snes9x_dirs();

	if (!Memory.Init() || !S9xInitAPU()) {
		fprintf(stderr,
				"Snes9x: Memory allocation failure - not enough RAM/virtual memory available.\nExiting...\n");
		Memory.Deinit();
		S9xDeinitAPU();
		exit(1);
	}

	S9xInitSound(unixSettings.SoundBufferSize, 0);
	S9xSetSoundMute(TRUE);

	S9xReportControllers();

#ifdef GFX_MULTI_FORMAT
	S9xSetRenderPixelFormat(RGB565);
#endif

	// @FILIPE: Speed up do jogo! Tem um loadconfig depois das configurações iniciais lá de cima.
	// Acho que por isso as modificacoes do topo da funcao nao estavam surgindo efeito.
	Settings.FrameAdvance = FALSE;
	Settings.DisplayFrameRate = TRUE;
	Settings.DisplayMovieFrame = TRUE;
	Settings.SkipFrames = 0;
	//Settings.FrameTime = 100;
	////Settings.SoundSync = FALSE;
	Settings.TurboMode = FALSE; // TRUEE
	Settings.HighSpeedSeek = 0; // 80
	Settings.TurboSkipFrames = 0; // 2

	m_snn_initiated = true;
}

void SNSInterface::init(ConfigVect config) {
	if (m_snn_initiated) {
		return;
	}

	srand((int) time(0));

	loadConfig(config);

	snprintf(default_dir, PATH_MAX + 1, "%s%s%s", getenv("HOME"), SLASH_STR,
			".snes9x");
	s9x_base_dir = default_dir;

	ZeroMemory(&Settings, sizeof(Settings));
	//Settings.MouseMaster = TRUE;
	//Settings.SuperScopeMaster = TRUE;
	Settings.JustifierMaster = TRUE;
	//Settings.MultiPlayer5Master = TRUE;
	Settings.SixteenBitSound = TRUE;
	Settings.Stereo = TRUE;
	Settings.SoundPlaybackRate = 32000;
	Settings.SoundInputRate = 32000;
	Settings.SupportHiRes = TRUE;
	Settings.Transparency = TRUE;
	Settings.AutoDisplayMessages = TRUE;
	Settings.InitialInfoStringTimeout = 120;
	Settings.HDMATimingHack = 100;
	Settings.BlockInvalidVRAMAccessMaster = TRUE;
	Settings.StopEmulation = TRUE;
	Settings.WrongMovieStateProtection = TRUE;
	Settings.DumpStreamsMaxFrames = -1;
	Settings.StretchScreenshots = 1;
	Settings.SnapshotScreenshots = TRUE;
	Settings.CartAName[0] = 0;
	Settings.CartBName[0] = 0;

#ifdef NETPLAY_SUPPORT
	Settings.ServerName[0] = 0;
#endif

#ifdef JOYSTICK_SUPPORT
	unixSettings.JoystickEnabled = TRUE;
#else
	unixSettings.JoystickEnabled = FALSE;
#endif

	unixSettings.ThreadSound = TRUE;
	unixSettings.SoundBufferSize = 100;
	unixSettings.SoundFragmentSize = 2048;

	ZeroMemory(&so, sizeof(so));

	CPU.Flags = 0;

	S9xLoadConfigFiles(m_argv, m_argc);
	rom_filename = S9xParseArgs(m_argv, m_argc);

	make_snes9x_dirs();

	if (!Memory.Init() || !S9xInitAPU()) {
		fprintf(stderr,
				"Snes9x: Memory allocation failure - not enough RAM/virtual memory available.\nExiting...\n");
		Memory.Deinit();
		S9xDeinitAPU();
		exit(1);
	}

	S9xInitSound(unixSettings.SoundBufferSize, 0);
	S9xSetSoundMute(TRUE);

	S9xReportControllers();

#ifdef GFX_MULTI_FORMAT
	S9xSetRenderPixelFormat(RGB565);
#endif

	m_snn_initiated = true;
}

void SNSInterface::loadROM() {
	uint32 saved_flags = CPU.Flags;
	bool8 loaded = FALSE;

	if (Settings.Multi) {
		loaded = Memory.LoadMultiCart(Settings.CartAName, Settings.CartBName);

		if (!loaded) {
			char s1[PATH_MAX + 1], s2[PATH_MAX + 1];
			char drive[_MAX_DRIVE + 1], dir[_MAX_DIR + 1],
					fname[_MAX_FNAME + 1], ext[_MAX_EXT + 1];

			s1[0] = s2[0] = 0;

			if (Settings.CartAName[0]) {
				_splitpath(Settings.CartAName, drive, dir, fname, ext);
				snprintf(s1, PATH_MAX + 1, "%s%s%s", S9xGetDirectory(ROM_DIR),
						SLASH_STR, fname);
				if (ext[0] && (strlen(s1) <= PATH_MAX - 1 - strlen(ext))) {
					strcat(s1, ".");
					strcat(s1, ext);
				}
			}

			if (Settings.CartBName[0]) {
				_splitpath(Settings.CartBName, drive, dir, fname, ext);
				snprintf(s2, PATH_MAX + 1, "%s%s%s", S9xGetDirectory(ROM_DIR),
						SLASH_STR, fname);
				if (ext[0] && (strlen(s2) <= PATH_MAX - 1 - strlen(ext))) {
					strcat(s2, ".");
					strcat(s2, ext);
				}
			}

			loaded = Memory.LoadMultiCart(s1, s2);
		}
	} else if (rom_filename) {
		loaded = Memory.LoadROM(rom_filename);

		if (!loaded && rom_filename[0]) {
			char s[PATH_MAX + 1];
			char drive[_MAX_DRIVE + 1], dir[_MAX_DIR + 1],
					fname[_MAX_FNAME + 1], ext[_MAX_EXT + 1];

			_splitpath(rom_filename, drive, dir, fname, ext);
			snprintf(s, PATH_MAX + 1, "%s%s%s", S9xGetDirectory(ROM_DIR),
					SLASH_STR, fname);
			if (ext[0] && (strlen(s) <= PATH_MAX - 1 - strlen(ext))) {
				strcat(s, ".");
				strcat(s, ext);
			}

			loaded = Memory.LoadROM(s);
		}
	}

	if (!loaded) {
		fprintf(stderr, "Error opening the ROM file.\n");
		exit(1);
	}

	// Set Setting for sf2 - Must need to be rewrite for a better coding
	m_rom = buildRomRLWrapper(ROMNAME_STREETFIGHTER2);
	NSRTControllerSetup();
	Memory.LoadSRAM(S9xGetFilename(".srm", SRAM_DIR));
	S9xLoadCheatFile(S9xGetFilename(".cht", CHEAT_DIR));

	CPU.Flags = saved_flags;
	Settings.StopEmulation = FALSE;

#ifdef DEBUGGER
	struct sigaction sa;
	sa.sa_handler = sigbrkhandler;
#ifdef SA_RESTART
	sa.sa_flags = SA_RESTART;
#else
	sa.sa_flags = 0;
#endif
	sigemptyset(&sa.sa_mask);
	//sigaction(SIGINT, &sa, NULL);
#endif

	S9xInitInputDevices();
	S9xInitDisplay(0, { });
	S9xSetupDefaultKeymap();
	S9xTextMode();

#ifdef NETPLAY_SUPPORT
	if (strlen(Settings.ServerName) == 0)
	{
		char *server = getenv("S9XSERVER");
		if (server)
		{
			strncpy(Settings.ServerName, server, 127);
			Settings.ServerName[127] = 0;
		}
	}

	char *port = getenv("S9XPORT");
	if (Settings.Port >= 0 && port)
	Settings.Port = atoi(port);
	else
	if (Settings.Port < 0)
	Settings.Port = -Settings.Port;

	if (Settings.NetPlay)
	{
		NetPlay.MaxFrameSkip = 10;

		if (!S9xNPConnectToServer(Settings.ServerName, Settings.Port, Memory.ROMName))
		{
			fprintf(stderr, "Failed to connect to server %s on port %d.\n", Settings.ServerName, Settings.Port);
			S9xExit();
		}

		fprintf(stderr, "Connected to server %s on port %d as player #%d playing %s.\n", Settings.ServerName, Settings.Port, NetPlay.Player, Memory.ROMName);
	}
#endif

	if (play_smv_filename) {
		uint32 flags = CPU.Flags & (DEBUG_MODE_FLAG | TRACE_FLAG);
		if (S9xMovieOpen(play_smv_filename, TRUE) != SUCCESS)
			exit(1);
		CPU.Flags |= flags;
	} else if (record_smv_filename) {
		uint32 flags = CPU.Flags & (DEBUG_MODE_FLAG | TRACE_FLAG);
		if (S9xMovieCreate(record_smv_filename, 0xFF, MOVIE_OPT_FROM_RESET,
				NULL, 0) != SUCCESS)
			exit(1);
		CPU.Flags |= flags;
	} else if (snapshot_filename) {
		uint32 flags = CPU.Flags & (DEBUG_MODE_FLAG | TRACE_FLAG);
		if (!S9xUnfreezeGame(snapshot_filename))
			exit(1);
		CPU.Flags |= flags;
	}

	S9xGraphicsMode();

	sprintf(String, "\"%s\" %s: %s", Memory.ROMName, TITLE, VERSION);
	S9xSetTitle(String);

	InitTimer();
	S9xSetSoundMute(FALSE);

	m_rom_loaded = true;
}

reward_t SNSInterface::act() {
	if (!m_rom_loaded) {
		return 0;
	}

	//	// TODO: m_rom->isLegal é realmente necessario? a chamada ao metodo ja foi feita antes da chamada desse metodo
//	if (act != NONE && m_rom->isLegal(act)) {
//		int press = isPress(act);
//		if (act <= 10) {
//			m_buttons_map[act-1] = press;
//		} else {
//			m_buttons_map[act-11] = press;
//		}
//
//		S9xApplyCommand(S9xGetCommandT((action_to_string(act)).c_str()), press, 0);
//	}

//	if (m_bot_behavior == 1) {
//		if (m_bot_behavior) {
//			static std::vector<Action> acts = {PLAYER2_UP_PRESS,
//					PLAYER2_RIGHT_PRESS, PLAYER2_DOWN_PRESS, PLAYER2_LEFT_PRESS,
//					PLAYER2_A_PRESS, PLAYER2_B_PRESS, PLAYER2_X_PRESS, PLAYER2_Y_PRESS,
//					PLAYER2_L_PRESS, PLAYER2_R_PRESS, PLAYER2_UP_RELEASE,
//					PLAYER2_RIGHT_RELEASE, PLAYER2_DOWN_RELEASE, PLAYER2_LEFT_RELEASE,
//					PLAYER2_A_RELEASE, PLAYER2_B_RELEASE, PLAYER2_X_RELEASE, PLAYER2_Y_RELEASE,
//					PLAYER2_L_RELEASE, PLAYER2_R_RELEASE };
//			static int i;
//			i = rand() % (acts.size());
//			// printf("\t\t\t\t\t\t\t\t %s\t%s\n", action_to_string(acts[i]).c_str(), (isPress(acts[i])) ? ("PRESS") : ("RELEASE"));
//			int press = isPress(acts[i]);
//			S9xApplyCommand(S9xGetCommandT((action_to_string(acts[i])).c_str()), press, 0);
//		}
//	}

	reward_t r = 0;
	r = mainLoop();
	return r;
}

bool SNSInterface::gameOver() const {
	return m_rom->isTerminal();
}

void SNSInterface::resetGame() {
	m_rom->reset();
	memset(m_buttons_map, 0, TOTAL_BUTTONS*sizeof(*m_buttons_map));
	
	S9xApplyCommand(S9xGetCommandT(m_rom->loadState(m_currentRomState).c_str()), 1, 0);
	printf("State %d - %s\n", m_currentRomState, m_rom->loadState(m_currentRomState).c_str());

//	if (m_bot_behavior != -1) {
//		S9xApplyCommand(S9xGetCommandT("QuickLoad001"), 1, 0);
//	} else {
//		S9xApplyCommand(S9xGetCommandT("QuickLoad000"), 1, 0);
//	}
}

cv::Mat SNSInterface::getScreen() {
	int width = IPPU.RenderedScreenWidth;
	int height = IPPU.RenderedScreenHeight;

	cv::Mat img = cv::Mat(cv::Size(width, height), CV_8UC3);

	uint16 *screen = GFX.Screen;
	uint32 r, g, b;

	for (int y = 0; y < height; y++, screen += GFX.RealPPL) {
		for (int x = 0; x < width; x++) {
			DECOMPOSE_PIXEL(screen[x], r, g, b);

			img.data[3 * (y * width + x) + 0] = (uchar) (b << 3);
			img.data[3 * (y * width + x) + 1] = (uchar) (g << 3);
			img.data[3 * (y * width + x) + 2] = (uchar) (r << 3);
		}
	}

	return img;
}

std::vector<Action> SNSInterface::getLegalActionSet() {
	return (m_rom_loaded) ? m_rom->getMinimalActionSet() : std::vector<Action>(NONE);
}

bool SNSInterface::actionIsLegal(const int player) {
	return m_rom->canAct(player);
}

std::vector<Action> SNSInterface::getMinimalActionSet() {
	return (m_rom_loaded) ? m_rom->getMinimalActionSet() : std::vector<Action>(NONE);
}

reward_t SNSInterface::mainLoop() {

#ifdef JOYSTICK_SUPPORT
	uint32 JoypadSkip = 0;
#endif

#ifdef NETPLAY_SUPPORT
	bool8 NP_Activated = Settings.NetPlay;

	if (NP_Activated)
	{
		if (NetPlay.PendingWait4Sync && !S9xNPWaitForHeartBeatDelay(100))
		{
			S9xProcessEvents(FALSE);
			return 0;
		}

		for (int J = 0; J < 8; J++)
		old_joypads[J] = MovieGetJoypad(J);

		for (int J = 0; J < 8; J++)
		MovieSetJoypad(J, joypads[J]);

		if (NetPlay.Connected)
		{
			if (NetPlay.PendingWait4Sync)
			{
				NetPlay.PendingWait4Sync = FALSE;
				NetPlay.FrameCount++;
				S9xNPStepJoypadHistory();
			}
		}
		else
		{
			fprintf(stderr, "Lost connection to server.\n");
			S9xExit();
		}
	}
#endif

#ifdef DEBUGGER
	if (!Settings.Paused || (CPU.Flags & (DEBUG_MODE_FLAG | SINGLE_STEP_FLAG)))
#else
	if (!Settings.Paused)
#endif
		S9xMainLoop();
#ifdef NETPLAY_SUPPORT
	if (NP_Activated)
	{
		for (int J = 0; J < 8; J++)
		MovieSetJoypad(J, old_joypads[J]);
	}
#endif

#ifdef DEBUGGER
	if (Settings.Paused || (CPU.Flags & DEBUG_MODE_FLAG))
#else
	if (Settings.Paused)
#endif
		S9xSetSoundMute(FALSE);

#ifdef DEBUGGER
	if (CPU.Flags & DEBUG_MODE_FLAG)
	S9xDoDebug();
	else
#endif
	if (Settings.Paused) {
		S9xProcessEvents(FALSE);
		usleep(100000);
	}

#ifdef JOYSTICK_SUPPORT
	if (unixSettings.JoystickEnabled && (JoypadSkip++ & 1) == 0)
	ReadJoysticks();
#endif

	S9xProcessEvents(FALSE);

#ifdef DEBUGGER
	if (!Settings.Paused && !(CPU.Flags & DEBUG_MODE_FLAG))
#else
	if (!Settings.Paused)
#endif
		S9xSetSoundMute(FALSE);

	m_rom->update();

	return m_rom->getReward();
}

ConfigVect SNSInterface::createDefaultConfig() {
	ConfigVect config;
	config.push_back(NO_SOUND);
	config.push_back(FRAME_RATE_100);
	return config;
}

void SNSInterface::loadConfig(ConfigVect config) {
	if (config.size() == 0) {
		config = createDefaultConfig();
	}

	for (ConfigSystem cs : config) {
		switch (cs) {
		case FRAME_RATE_0:
			m_config.framerate = 0;
			break;
		case FRAME_RATE_1:
			m_config.framerate = DEFAULT_FRAMERATE * 0.01;
			break;
		case FRAME_RATE_10:
			m_config.framerate = DEFAULT_FRAMERATE * 0.10;
			break;
		case FRAME_RATE_20:
			m_config.framerate = DEFAULT_FRAMERATE * 0.20;
			break;
		case FRAME_RATE_30:
			m_config.framerate = DEFAULT_FRAMERATE * 0.30;
			break;
		case FRAME_RATE_40:
			m_config.framerate = DEFAULT_FRAMERATE * 0.40;
			break;
		case FRAME_RATE_50:
			m_config.framerate = DEFAULT_FRAMERATE * 0.50;
			break;
		case FRAME_RATE_60:
			m_config.framerate = DEFAULT_FRAMERATE * 0.60;
			break;
		case FRAME_RATE_70:
			m_config.framerate = DEFAULT_FRAMERATE * 0.70;
			break;
		case FRAME_RATE_80:
			m_config.framerate = DEFAULT_FRAMERATE * 0.80;
			break;
		case FRAME_RATE_90:
			m_config.framerate = DEFAULT_FRAMERATE * 0.90;
			break;
		case FRAME_RATE_100:
			m_config.framerate = DEFAULT_FRAMERATE;
			break;
		default:
			// Nothing
			break;
		}
	}
}

std::vector<uint8> SNSInterface::getInformation() {
	if (m_rom_loaded) {
		return m_rom->getInformation();
	}

	return std::vector<uint8>();
}

std::vector<Action> SNSInterface::getStartingActions() {
	if (m_rom_loaded) {
		return m_rom->getStartingActions();
	}

	return std::vector<Action>();
}

void SNSInterface::loadRomState(int state) {
	m_currentRomState = state;
}

void SNSInterface::performAction(Action action) {
	if (action != NONE && m_rom->isLegal(action)) {
		int press = isPress(action);
		S9xApplyCommand(S9xGetCommandT((action_to_string(action)).c_str()), press, 0);
	}
}

