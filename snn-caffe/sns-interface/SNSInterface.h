/*
 * SNSInterface.h
 *
 *  Created on: 18 de jul de 2016
 *      Author: fernando
 */

#ifndef SNES9X_SNSINTERFACE_H_
#define SNES9X_SNSINTERFACE_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <dirent.h>
#include <signal.h>
#include <errno.h>
#include <string.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif
#ifdef USE_THREADS
#include <sched.h>
#include <pthread.h>
#endif
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#ifdef HAVE_SYS_IOCTL_H
#include <sys/ioctl.h>
#endif
#ifndef NOSOUND
#include <sys/soundcard.h>
#include <sys/mman.h>
#endif
#ifdef JOYSTICK_SUPPORT
#include <linux/joystick.h>
#endif
#include <memory>

#include "./snes9x/snes9x.h"
#include "./snes9x/memmap.h"
#include "./snes9x/apu/apu.h"
#include "./snes9x/gfx.h"
#include "./snes9x/snapshot.h"
#include "./snes9x/controls.h"
#include "./snes9x/cheats.h"
#include "./snes9x/movie.h"
#include "./snes9x/logger.h"
#include "./snes9x/display.h"
#include "./snes9x/conffile.h"
#ifdef NETPLAY_SUPPORT
#include "./snes9x/netplay.h"
#endif
#ifdef DEBUGGER
#include "./snes9x/debug.h"
#endif

#ifdef NETPLAY_SUPPORT
#ifdef _DEBUG
#define NP_DEBUG 2
#endif
#endif

#include "./games/supported/StreetFighter2.h"
#include "./games/RomsUtils.h"
#include "./games/Roms.h"
#include "./games/RomSettings.h"
#include "./Constants.h"

#define DEFAULT_FRAMERATE 16667

typedef unsigned char pixel_t;

class SNSConfig {
public:
	SNSConfig() {
		framerate = FRAME_RATE_100;
	};
	uint32 framerate;
};

class SNSInterface {
public:
	SNSInterface(char* rom_path);
	~SNSInterface();

	// Initialize the emulator with the default config
	void init();

	// Initialize the emulator with a custom config
	void init(ConfigVect config);

	// Load the rom file
	void loadROM();

	void performAction(Action action);

	// Aplies an action to the rom
	reward_t act();

	// Indicates if the rom has ended
	bool gameOver() const;

	// Reset only the rom to initial state
	void resetGame();

	// Save the current screen as a png file
	cv::Mat getScreen();

	// Return useful information about the rom
	std::vector<uint8> getInformation();

	// Returns the vector of legal actions. This should be called only
	// after the rom is loaded.
	std::vector<Action> getLegalActionSet();

	bool actionIsLegal(const int player);

	// Returns the vector of the minimal set of actions needed to play
	// the game.
	std::vector<Action> getMinimalActionSet();

	// Returns the vector of the minimal set for pass through the initial screen of
	// the game
	std::vector<Action> getStartingActions();

	// Indicates if the rom is initiated
	inline bool isGameInitiated() { return m_rom->isInitiated(); };

	// Indicates if the emulator has successfully initialized
	inline bool isSnnInitiated() { return m_snn_initiated; };

	inline int* getButtonsState() { return m_buttons_map; }

	inline void	setOneState(const uint16 &state) { m_rom->setOneState(state); };

	inline std::vector<uint16> getAllStates() const { return m_rom->getAllStates(); }

	void loadRomState(int state);

	// HACK TODO : fix this later
	inline void changeBotBehavior(int behavior) { m_bot_behavior = behavior; }

	// HACK TODO : fix this later
	inline int getCurrentRomState() { return m_currentRomState; }
private:
	reward_t mainLoop();
	ConfigVect createDefaultConfig();
	void loadConfig(ConfigVect config);

	SNSConfig m_config;
	RomSettings* m_rom;
	uint32 m_framerate;
	bool m_snn_initiated;
	bool m_rom_loaded;
	int* m_buttons_map; // ** 0 - not pressed ** 1 - pressed
	int m_argc;
	char** m_argv;

	int m_currentRomState;
	int m_bot_behavior; // ** -1 - normal ** 0 - static ** 1 - random
};

#endif /* SNES9X_SNSINTERFACE_H_ */
