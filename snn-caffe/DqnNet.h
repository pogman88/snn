
#ifndef DQNNET_H_
#define DQNNET_H_

#include "DqnEpisode.h"

#define USE_OPENCV

#include <caffe/caffe.hpp>
#include <caffe/solver.hpp>
#include <caffe/sgd_solvers.hpp>
#include <caffe/util/io.hpp>
#include <caffe/layers/memory_data_layer.hpp>

class DqnNet
{
	char *_solver_file;

	boost::shared_ptr<caffe::Solver<float>> solver_;
	boost::shared_ptr<caffe::Net<float>> net_;
	caffe::SolverParameter solver_param_;

	boost::shared_ptr<caffe::Blob<float>> q_values_blob_;

	boost::shared_ptr<caffe::MemoryDataLayer<float>> frames_input_layer_;
	boost::shared_ptr<caffe::MemoryDataLayer<float>> target_input_layer_;
	boost::shared_ptr<caffe::MemoryDataLayer<float>> filter_input_layer_;
	boost::shared_ptr<caffe::MemoryDataLayer<float>> reset_input_layer_;
	boost::shared_ptr<caffe::MemoryDataLayer<float>> additional_data_input_layer_;

	vector<caffe::Datum> dummy_target_datum_vector;
	vector<caffe::Datum> dummy_filter_datum_vector;
	vector<caffe::Datum> reset_datum_vector;
	vector<caffe::Datum> maintain_datum_vector;
	//vector<caffe::Datum> test_input_datum_vector;
	vector<caffe::Datum> test_add_data_datum_vector;

	vector<caffe::Datum> train_target_datum_vector;
	vector<caffe::Datum> train_filter_datum_vector;
	//vector<caffe::Datum> train_input_datum_vector;
	vector<caffe::Datum> train_add_data_datum_vector;

	int num_forwards_before_train;

	int _iter;
	double _gamma;

	template<typename Dtype>
	bool _HasBlobSize(caffe::Blob<Dtype>& blob, int batch, int channels, int height, int width);

	void _AddDatumToVector(vector<caffe::Datum> *datums, caffe::Datum d);

	caffe::Datum _BuildEmptyDatum(int rows, int cols, int channels);
	caffe::Datum _BuildConstantDatum(int rows, int cols, int channels, float value);
	caffe::Datum _BuildInputDatum(vector<Mat*> &m);
	caffe::Datum _BuildAddDataDatum(int* buttons_states,
									std::deque<int> *last_commands,
									std::vector<uint8_t> *rom_info);

	// void _TrainQLearning(DqnEpisode *episode);
	// void _TrainRewardWithDecay(DqnEpisode *episode);

	void _TrainTransitionQLearning(DqnEpisode *episode, int transition_id, int reset = 0, int dqn_q_learning_initialization_active = 0);
	void _TrainTransitionRewardWithDecay(DqnEpisode *episode, int transition_id, int reset = 0);

	public:

		DqnNet(char *solver_file);

		// the first element is the action, and the second the estimated reward
		std::pair<int, double> SelectAction(vector<Mat*> input,
				int* buttons_states,
				std::deque<int> *last_commands,
				std::vector<uint8_t> *rom_info,
				int reset = 0);

		//void Train(DqnEpisode *episode);
		void TrainTransition(DqnEpisode *episode, int transition_id, int reset = 0,  int dqn_q_learning_initialization_active = 0);
		
		void SaveTrain();
		void LoadTrain(char *filename);

		double Gamma() { return _gamma; }

		double LearningRate()
		{
			double g = (solver_->param().gamma() > 0) ? (solver_->param().gamma()) : 1.0;
			double s = (solver_->param().stepsize() > 0) ? (solver_->param().stepsize()) : 1.0;
			double current_lr = solver_->param().base_lr() * pow(g, solver_->iter() / s);

			return current_lr;
		}

		long SolverIter() { return solver_->iter(); }
};

#endif
