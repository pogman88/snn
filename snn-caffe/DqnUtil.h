
#ifndef DQNUTIL_H_
#define DQNUTIL_H_

#include <string>
#include <vector>

using namespace std;

double get_time();
void show_control(int *button_state, int command);

template < typename T >
std::string to_string(const T& n);

#endif
