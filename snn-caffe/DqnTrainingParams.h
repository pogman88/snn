/*
 * DqnEmulatorParams.h
 *
 *  Created on: Dec 5, 2016
 *      Author: fernando
 */

#ifndef SNN_CAFFE_DQNTRAININGPARAMS_H_
#define SNN_CAFFE_DQNTRAININGPARAMS_H_

#define TRAINPARAMS_NUMBER_TRAINING 2000
#define TRAINPARAMS_BATCH_TEST 30
#define TRAINPARAMS_PERCENTAGE_BATCH_VICTORYS_TEST 1.0
#define TRAINPARAMS_PERIODIC_TEST 100

#define TRAINPARAMS_ALTERNATE_EPSILON 1

#endif /* SNN_CAFFE_DQNTRAININGPARAMS_H_ */
