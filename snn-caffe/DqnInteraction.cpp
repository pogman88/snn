

#include <cstring>
#include "DqnInteraction.h"
#include "sns-interface/Constants.h"


DqnInteration::DqnInteration()
{
	input = NULL;
	immediate_reward = -DBL_MAX;
	action = -1;

	buttons_states = NULL;
	last_commands = NULL;
	rom_info = NULL;

}


DqnInteration::DqnInteration(DqnInteration *iter)
{
	input = new Mat();
	iter->input->copyTo(*input);

	immediate_reward = iter->immediate_reward;
	action = iter->action;

	buttons_states = (int *) calloc (TOTAL_BUTTONS, sizeof(int));
	memcpy(buttons_states, iter->buttons_states, TOTAL_BUTTONS * sizeof(int));

	last_commands = new std::deque<int>(*(iter->last_commands));
	rom_info = new std::vector<uint8_t>(*(iter->rom_info));
}


DqnInteration::DqnInteration(Mat *input_p, double immediate_reward_p,
		int action_p, int *buttons_states_p,
		std::deque<int> *last_commands_p,
		std::vector<uint8_t> *rom_info_p)
{
	input = input_p;
	immediate_reward = immediate_reward_p;
	action = action_p;

	buttons_states = buttons_states_p;
	last_commands = last_commands_p;
	rom_info = rom_info_p;
}
