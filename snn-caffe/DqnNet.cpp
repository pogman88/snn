#include "DqnUtil.h"
#include "DqnNet.h"
#include "DqnParams.h"
#include <string>
#include <opencv/highgui.h>


template<typename Dtype>
bool DqnNet::_HasBlobSize(caffe::Blob<Dtype>& blob, int batch_size, int channels, int height, int width)
{
	return ((blob.num() == batch_size) &&
			(blob.channels() == channels) &&
			(blob.height() == height) &&
			(blob.width() == width));
}


void
DqnNet::_AddDatumToVector(vector<caffe::Datum> *datums, caffe::Datum d)
{
	datums->push_back(d);
}


caffe::Datum
DqnNet::_BuildEmptyDatum(int rows, int cols, int channels)
{
	return _BuildConstantDatum(rows, cols, channels, 0);
}


caffe::Datum
DqnNet::_BuildConstantDatum(int rows, int cols, int channels, float value)
{
	caffe::Datum d;

	d.set_channels(channels);
	d.set_height(rows);
	d.set_width(cols);
	d.set_label(0);

	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			for (int k = 0; k < channels; k++)
				d.add_float_data(value);

	return d;
}


caffe::Datum
DqnNet::_BuildInputDatum(vector<Mat*> &ms)
{
	int i, j, n, k;
	int img_p, line_size;
	float pixel;

	assert(ms.size() > 0);
	assert(ms[0]->rows == DQN_FRAME_DIM);
	assert(ms[0]->cols == DQN_FRAME_DIM);
	assert(ms[0]->channels() == DQN_FRAME_CHANNELS);

	line_size = DQN_FRAME_DIM * DQN_FRAME_CHANNELS;

	static int first = 1;
	static caffe::Datum d;

	if (first)
	{
		d.set_channels(DQN_FRAME_CHANNELS * DQN_NUM_INPUT_FRAMES);
		d.set_height(DQN_FRAME_DIM);
		d.set_width(DQN_FRAME_DIM);
		d.set_label(0);

		for (i = 0; i < DQN_FRAME_DIM; i++)
			for (j = 0; j < DQN_FRAME_DIM; j++)
				for (n = 0; n < DQN_NUM_INPUT_FRAMES; n++)
					for (k = 0; k < DQN_FRAME_CHANNELS; k++)
						d.add_float_data(0);

		first = 0;
	}

	//std::string buffer(DQN_FRAME_CHANNELS * DQN_NUM_INPUT_FRAMES * DQN_FRAME_DIM * DQN_FRAME_DIM, ' ');
	//d.mutable_float_data()->Reserve(DQN_FRAME_CHANNELS * DQN_NUM_INPUT_FRAMES * DQN_FRAME_DIM * DQN_FRAME_DIM);

	for (n = 0; n < DQN_NUM_INPUT_FRAMES; n++)
	{
		for (i = 0; i < DQN_FRAME_DIM; i++)
		{
			for (j = 0; j < DQN_FRAME_DIM; j++)
			{
				for (k = 0; k < DQN_FRAME_CHANNELS; k++)
				{
					// veja http://caffe.berkeleyvision.org/tutorial/net_layer_blob.html . A conta da posicao foi extraida de la.
					int input_p =  ((n * DQN_FRAME_CHANNELS + k) * DQN_FRAME_DIM + i) * DQN_FRAME_DIM + j;

					if (n < ms.size())
					{
						img_p = i * line_size + j * DQN_FRAME_CHANNELS + k;
						pixel = (float) ms[n]->data[img_p];
						d.set_float_data(input_p, pixel);
						//input_data[input_p] = pixel;
						//buffer[input_p] = pixel;
					}
					else
						//input_data[input_p] = 0.0;
						//buffer[input_p] = 0.0;
						d.set_float_data(input_p, 0.0);
				}
			}
		}
	}

	return d;
}


vector<Mat>
BuildMat(std::vector<cv::Mat*> inputs)
{
	std::vector<cv::Mat> array_to_merge;

	for (int i = 0; i < inputs.size(); i++)
		array_to_merge.push_back(*(inputs[i]));

	cv::Mat merged;
	cv::merge(array_to_merge, merged);

	vector<Mat> v;
	v.push_back(merged);

	return v;
}


caffe::Datum
DqnNet::_BuildAddDataDatum(int* buttons_states,
							std::deque<int> *last_commands,
							std::vector<uint8_t> *rom_info)
{
	int i, n;
	caffe::Datum d;

	assert(last_commands->size() == DQN_NUM_PAST_COMMANDS_TO_STORE);
	assert(last_commands->size() + rom_info->size() + TOTAL_BUTTONS == DQN_NUM_ADDITIONAL_DATA);

	d.set_channels(DQN_NUM_ADDITIONAL_DATA * DQN_NUM_COPIES_OF_ADDITIONAL_DATA);
	d.set_height(1);
	d.set_width(1);
	d.set_label(0);

	for (n = 0; n < DQN_NUM_COPIES_OF_ADDITIONAL_DATA; n++)
	{
		for (i = 0; i < TOTAL_BUTTONS; i++)
			d.add_float_data(buttons_states[i]);

		for (i = 0; i < last_commands->size(); i++)
			d.add_float_data(last_commands->at(i) / 10.0);

		for (i = 0; i < rom_info->size(); i++)
			d.add_float_data(rom_info->at(i) / 100.0);
	}

	return d;
}


DqnNet::DqnNet(char *solver_file)
{
	if (DQN_USE_LSTM)
		num_forwards_before_train = 4;
	else
		num_forwards_before_train = 0;

	_gamma = DQN_GAMMA;
	_iter = 0;

	_solver_file = solver_file;

#ifdef CPU_ONLY
	caffe::Caffe::set_mode(caffe::Caffe::CPU);
#else
	caffe::Caffe::set_mode(caffe::Caffe::GPU);
	caffe::Caffe::SetDevice(0);
#endif

	caffe::ReadProtoFromTextFileOrDie(solver_file, &solver_param_);
	solver_.reset(caffe::SolverRegistry<float>::CreateSolver(solver_param_));

	net_ = solver_->net();

	frames_input_layer_ = boost::dynamic_pointer_cast<caffe::MemoryDataLayer<float>>(net_->layer_by_name(DQN_INPUT_LAYER_NAME));
	target_input_layer_ = boost::dynamic_pointer_cast<caffe::MemoryDataLayer<float>>(net_->layer_by_name(DQN_TARGET_LAYER_NAME));
	filter_input_layer_ = boost::dynamic_pointer_cast<caffe::MemoryDataLayer<float>>(net_->layer_by_name(DQN_FILTER_LAYER_NAME));
	additional_data_input_layer_ = boost::dynamic_pointer_cast<caffe::MemoryDataLayer<float>>(net_->layer_by_name(DQN_ADD_DATA_LAYER_NAME));

	if (DQN_USE_LSTM) reset_input_layer_ = boost::dynamic_pointer_cast<caffe::MemoryDataLayer<float>>(net_->layer_by_name("reset"));

	assert(frames_input_layer_);
	assert(target_input_layer_);
	assert(filter_input_layer_);
	assert(additional_data_input_layer_);

	if (DQN_USE_LSTM) assert(reset_input_layer_);

	assert(_HasBlobSize(*net_->blob_by_name(DQN_INPUT_BLOB_NAME), DQN_MINI_BATCH_SIZE, DQN_FRAME_CHANNELS * DQN_NUM_INPUT_FRAMES, DQN_FRAME_DIM, DQN_FRAME_DIM));
	assert(_HasBlobSize(*net_->blob_by_name(DQN_TARGET_BLOB_NAME), DQN_MINI_BATCH_SIZE, DQN_NUM_COMMANDS, 1, 1));
	assert(_HasBlobSize(*net_->blob_by_name(DQN_FILTER_BLOB_NAME), DQN_MINI_BATCH_SIZE, DQN_NUM_COMMANDS, 1, 1));
	assert(_HasBlobSize(*net_->blob_by_name(DQN_ADD_DATA_BLOB_NAME), DQN_MINI_BATCH_SIZE, DQN_TOTAL_ADDITIONAL_DATA_SIZE, 1,1));

	// add empty datums to fill the batch
	for (int i = 0; i < DQN_MINI_BATCH_SIZE; i++)
	{
		if (DQN_USE_LSTM)
		{
			_AddDatumToVector(&reset_datum_vector, _BuildEmptyDatum(1, 1, 1));
			_AddDatumToVector(&maintain_datum_vector, _BuildConstantDatum(1, 1, 1, 1.0));
		}

		_AddDatumToVector(&dummy_target_datum_vector, _BuildEmptyDatum(1, 1, DQN_NUM_COMMANDS));
		_AddDatumToVector(&dummy_filter_datum_vector, _BuildEmptyDatum(1, 1, DQN_NUM_COMMANDS));
		//_AddDatumToVector(&test_input_datum_vector, _BuildEmptyDatum(DQN_FRAME_DIM, DQN_FRAME_DIM, DQN_FRAME_CHANNELS * DQN_NUM_INPUT_FRAMES));
		_AddDatumToVector(&test_add_data_datum_vector, _BuildEmptyDatum(1, 1, DQN_TOTAL_ADDITIONAL_DATA_SIZE));

		_AddDatumToVector(&train_target_datum_vector, _BuildEmptyDatum(1, 1, DQN_NUM_COMMANDS));
		_AddDatumToVector(&train_filter_datum_vector, _BuildEmptyDatum(1, 1, DQN_NUM_COMMANDS));
		//_AddDatumToVector(&train_input_datum_vector, _BuildEmptyDatum(DQN_FRAME_DIM, DQN_FRAME_DIM, DQN_FRAME_CHANNELS * DQN_NUM_INPUT_FRAMES));
		_AddDatumToVector(&train_add_data_datum_vector, _BuildEmptyDatum(1, 1, DQN_TOTAL_ADDITIONAL_DATA_SIZE));
	}

	for (int i = 1; i <= DQN_NUM_INPUT_FRAMES; i++) {
		cv::namedWindow(to_string(i));
		cv::moveWindow(to_string(i), 100+((i-1)*200), 100+((i%3)*300));
	}


//	cv::moveWindow("1", 100, 100);
//	cv::moveWindow("2", 300, 100);
//	cv::moveWindow("3", 500, 100);
//	cv::moveWindow("4", 100, 300);
//	cv::moveWindow("5", 300, 300);
//	cv::moveWindow("6", 500, 300);
}


std::pair<int, double>
DqnNet::SelectAction(vector<Mat*> input, int* buttons_states,
					std::deque<int> *last_commands,
					std::vector<uint8_t> *rom_info,
					int reset)
{
	int i;
	float loss;

	assert(rom_info);
	assert(last_commands);
	assert(buttons_states);
	assert(input.size() == DQN_NUM_INPUT_FRAMES);
	assert(last_commands->size() == DQN_NUM_PAST_COMMANDS_TO_STORE);

	// check if it can be optimized
	// OBS: As instrucoes abaixo assumem que os vetores de datums estao preenchidos com zero e sempre
	// sobrescrevem apenas a primeira posicao do vetor.

	//test_input_datum_vector[0].CopyFrom(_BuildInputDatum(input));
	//frames_input_layer_->AddDatumVector(test_input_datum_vector);
	vector<int> labels; labels.push_back(0);
	frames_input_layer_->AddMatVector(BuildMat(input), labels);
	//_InputFramesInTheNet(input);

	test_add_data_datum_vector[0].CopyFrom(_BuildAddDataDatum(buttons_states, last_commands, rom_info));
	additional_data_input_layer_->AddDatumVector(test_add_data_datum_vector);

	target_input_layer_->AddDatumVector(dummy_target_datum_vector);
	filter_input_layer_->AddDatumVector(dummy_filter_datum_vector);

	if (DQN_USE_LSTM)
	{
		if (reset) reset_input_layer_->AddDatumVector(reset_datum_vector);
		else reset_input_layer_->AddDatumVector(maintain_datum_vector);
	}

	net_->ForwardPrefilled(&loss);

	std::pair<int, double> action_and_reward(-1, -DBL_MAX);

	// select the action with max. Q
	const float *output = net_->blob_by_name(DQN_OUTPUT_BLOB_NAME)->cpu_data();

	for (i = 0; i < DQN_NUM_COMMANDS; i++)
	{
//		if (n >= 100)
		//printf("(%s, %.4lf) ", action_to_string(Action(i)).c_str(), output[i]);
		//if (i == (DQN_NUM_COMMANDS / 2)) printf("\n");

		if (output[i] > action_and_reward.second)
		{
			action_and_reward.first = i;
			action_and_reward.second = output[i];
		}
	}

	return action_and_reward;
}


void
DqnNet::_TrainTransitionRewardWithDecay(DqnEpisode *episode, int transition_id, int reset)
{
	// ********************************************************************1
	// OBS: ASSUME QUE O BATCH EH DE TAMANHO 1. VOU CORRIGIR EM BREVE!
	// ********************************************************************
	int j, k, base_pos, sample_pos;
	static int fast_forward = 0;
	float loss;

	j = 0;

	sample_pos = transition_id;

	// the last mini batch will be incomplete, so we fill it with random interactions
	if (sample_pos >= episode->GetInteractions()->size())
		sample_pos = (rand() % (episode->GetInteractions()->size() - DQN_NUM_INPUT_FRAMES)) + DQN_NUM_INPUT_FRAMES;

	vector<Mat*> inputs;
	
	if (DQN_NUM_INPUT_FRAMES > 1)
	{
		inputs.push_back(episode->GetInteractions()->at(0)->input);

		for (k = (DQN_NUM_INPUT_FRAMES - 2); k >= 1; k--)
			inputs.push_back(episode->GetInteractions()->at(sample_pos - k)->input);
	}
	inputs.push_back(episode->GetInteractions()->at(sample_pos)->input);

	if (fast_forward)
		return;

	// additional data datum
	train_add_data_datum_vector[j].CopyFrom(_BuildAddDataDatum(
			episode->GetInteractions()->at(sample_pos)->buttons_states,
			episode->GetInteractions()->at(sample_pos)->last_commands,
			episode->GetInteractions()->at(sample_pos)->rom_info));

	if (DQN_USE_LSTM)
	{
		if (reset) reset_input_layer_->AddDatumVector(reset_datum_vector);
		else reset_input_layer_->AddDatumVector(maintain_datum_vector);
	}

	//printf("buttons: ");
	//for (k = 0; k < TOTAL_BUTTONS; k++)
	//	printf("%d ", episode->GetInteractions()->at(sample_pos)->buttons_states[k]);
	//printf("\n");
	//printf("commands: \n");
	//for (k = 0; k < episode->GetInteractions()->at(sample_pos)->last_commands->size(); k++)
	//	printf("%s ", action_to_string(Action(episode->GetInteractions()->at(sample_pos)->last_commands->at(k))).c_str());
	//printf("\n");
	//printf("Info: ");
	//for (k = 0; k < episode->GetInteractions()->at(sample_pos)->rom_info->size(); k++)
	//	printf("%d ", episode->GetInteractions()->at(sample_pos)->rom_info->at(k));
	//printf("\n");
	//waitKey(-1);

	// target and filter datums (set all outputs to zero and then set the action that we want to train)

	// the target is the decayed reward computed previously
	double target = episode->GetInteractions()->at(sample_pos)->immediate_reward;
	// set the action performed in the episode and its respective value
	train_target_datum_vector[j].set_float_data(episode->GetInteractions()->at(sample_pos)->action, target);
	train_filter_datum_vector[j].set_float_data(episode->GetInteractions()->at(sample_pos)->action, 1.0);

	//int num_pressed_buttons = 0;
	//
	//for (k = 1; k < DQN_NUM_COMMANDS; k++)
	//{
	//	if (episode->GetInteractions()->at(sample_pos)->buttons_states[k - 1])
	//	{
	//		train_target_datum_vector[j].set_float_data(k, target);
	//		train_filter_datum_vector[j].set_float_data(k, 1.0);
	//		num_pressed_buttons++;
	//	}
	//	else
	//	{
	//		train_target_datum_vector[j].set_float_data(k, 0.0);
	//		train_filter_datum_vector[j].set_float_data(k, 0.0);
	//	}
	//}
    //
	//if (num_pressed_buttons == 0)
	//{
	//	train_target_datum_vector[j].set_float_data(0, target);
	//	train_filter_datum_vector[j].set_float_data(0, 1.0);
	//}
	//else
	//{
	//	train_target_datum_vector[j].set_float_data(0, 0.0);
	//	train_filter_datum_vector[j].set_float_data(0, 0.0);
	//}

	/*if (i % 20 == 0 && j == 0)
	{
		printf("%d de %ld: REW: %.4lf TARGET: %.4lf\n", sample_pos,
				episode->GetInteractions()->size() - 1,
				episode->GetInteractions()->at(sample_pos)->immediate_reward,
				target);
	}*/

	// add the datums to the net
	//train_input_datum_vector[j].CopyFrom(_BuildInputDatum(inputs));
	//frames_input_layer_->AddDatumVector(train_input_datum_vector);
	vector<int> labels; labels.push_back(0);
	frames_input_layer_->AddMatVector(BuildMat(inputs), labels);
	//_InputFramesInTheNet(inputs);

	additional_data_input_layer_->AddDatumVector(train_add_data_datum_vector);
	target_input_layer_->AddDatumVector(train_target_datum_vector);
	filter_input_layer_->AddDatumVector(train_filter_datum_vector);

	//printf("\nQs Pred: ");
	//for (k = 0; k < DQN_NUM_COMMANDS; k++)
	//	printf("%.3lf ", net_->blob_by_name("q_values")->cpu_data()[k]);
	//printf("\nQs Pred Filtered: ");
	//for (k = 0; k < DQN_NUM_COMMANDS; k++)
	//	printf("%.3lf ", net_->blob_by_name("filtered_q_values")->cpu_data()[k]);
	//printf("\nTarget: ");
	//for (k = 0; k < DQN_NUM_COMMANDS; k++)
	//	printf("%.3lf %s ", net_->blob_by_name("target")->cpu_data()[k], (net_->blob_by_name("target")->cpu_data()[k] != 0) ? ("<<<<<<<<<<<<<<<<") : (""));
	//printf("\n");

	// train
	solver_->Step(4);

	assert(!isnan(net_->layer_by_name("conv1_layer")->blobs().front()->data_at(1, 0, 0, 0)));
	assert(!isnan(net_->layer_by_name("conv2_layer")->blobs().front()->data_at(1, 0, 0, 0)));
	assert(!isnan(net_->layer_by_name("ip1_layer")->blobs().front()->data_at(1, 0, 0, 0)));
	//assert(!isnan(net_->layer_by_name("ip1.5_layer")->blobs().front()->data_at(1, 0, 0, 0)));
	assert(!isnan(net_->layer_by_name("ip2_layer")->blobs().front()->data_at(1, 0, 0, 0)));

	cv::imshow("1", *(inputs[0]));
	cv::imshow("2", *(inputs[1]));
	cv::imshow("3", *(inputs[2]));
	cv::imshow("4", *(inputs[3]));
	cv::imshow("5", *(inputs[4]));
	cv::imshow("6", *(inputs[5]));

	static int step = 0;
	char c = ' ';

	show_control(episode->GetInteractions()->at(sample_pos)->buttons_states,
			episode->GetInteractions()->at(sample_pos)->action);

	if (step) c = cv::waitKey(-1);
	else c = cv::waitKey(5);

	if (c == 's') { step = !step; printf("Step: %d\n", step); }
	else if (c == 'f') { fast_forward = !fast_forward; printf("FF: %d\n", fast_forward); }
}


void
DqnNet::_TrainTransitionQLearning(DqnEpisode *episode, int transition_id, int reset, int dqn_q_learning_initialization_active)
{
	// ********************************************************************1
	// OBS: ASSUME QUE O BATCH EH DE TAMANHO 1. VOU CORRIGIR EM BREVE!
	// ********************************************************************
	int j, k, base_pos, sample_pos;
	static int fast_forward = 0, step = 0;
	float loss;

	sample_pos = transition_id;
	j = 0;

	// the last mini batch will be incomplete, so we fill it with random interactions
	if (sample_pos >= episode->GetInteractions()->size())
		sample_pos = (rand() % (episode->GetInteractions()->size() - DQN_NUM_INPUT_FRAMES)) + DQN_NUM_INPUT_FRAMES;

	vector<Mat*> inputs;

	if (DQN_NUM_INPUT_FRAMES > 1)
	{
		if (DQN_USE_FIRST_FRAME) {
			inputs.push_back(episode->GetInteractions()->at(0)->input);

			for (k = (DQN_NUM_INPUT_FRAMES - 2); k >= 1; k--)
				inputs.push_back(episode->GetInteractions()->at(sample_pos - k)->input);
		} else {
			for (k = (DQN_NUM_INPUT_FRAMES - 1); k >= 1; k--)
				inputs.push_back(episode->GetInteractions()->at(sample_pos - k)->input);
		}
	}
	inputs.push_back(episode->GetInteractions()->at(sample_pos)->input);

	if (fast_forward) { printf("FAST FORWARDING...\n"); return; }

//	if (DQN_USE_LSTM)
//	{
//		if (reset)
//			num_forwards_before_train = 4;
//		else
//		{
//			num_forwards_before_train--;
//
//			if (num_forwards_before_train <= 0)
//				num_forwards_before_train = 0;
//		}
//	}

	/* Compute the future reward using the model*/
	double target;

	// if it is the last sample, we train only the immediate reward
	if (sample_pos == episode->GetInteractions()->size() - 1 || dqn_q_learning_initialization_active)
	{
		target = episode->GetInteractions()->at(sample_pos)->immediate_reward;

		/*
		if (i % 20 == 0 && j == 0)
			printf("%d de %ld: REW: %.4lf FUT: ------ TARGET: %.4lf\n", sample_pos,
					episode->GetInteractions()->size() - 1,
					episode->GetInteractions()->at(sample_pos)->immediate_reward,
					target);
		*/
	}
	// else we estimate the future reward and compute the temporal difference
	else
	{
		vector<Mat*> future_inputs;

		if (DQN_NUM_INPUT_FRAMES > 1)
		{
			future_inputs.push_back(episode->GetInteractions()->at(0)->input);

			for (k = (DQN_NUM_INPUT_FRAMES - 3); k >= 0; k--)
				future_inputs.push_back(episode->GetInteractions()->at(sample_pos - k)->input);
		}

		//int pos = sample_pos + 10;
		//int pos = sample_pos + 4;
		int pos = sample_pos + 1; ;
		if (pos >= episode->GetInteractions()->size())
			pos = (episode->GetInteractions()->size() - 1);

		future_inputs.push_back(episode->GetInteractions()->at(pos)->input);

		static int n = 0;

		if (n > 20)
		{
			for (int i = 0; i < inputs.size(); i++) {
				cv::imshow(to_string(i+1), *(inputs[i]));
			}


			if (sample_pos < episode->GetInteractions()->size() - 1)
			{
				cv::imshow("present", *(episode->GetInteractions()->at(sample_pos)->input));
				cv::imshow("future", *(future_inputs[future_inputs.size() - 1]));
			}

			char c = ' ';

			if (step) c = cv::waitKey(-1);
			else c = cv::waitKey(10);

			show_control(episode->GetInteractions()->at(pos)->buttons_states,
					episode->GetInteractions()->at(pos)->action);

			if (c == 's') { step = !step; printf("Step: %d\n", step); }
			else if (c == 'f') { fast_forward = !fast_forward; printf("FF: %d\n", fast_forward); }

			n = 0;
		}
		else
			n++;

		// OBS: I am performing a forward for each sample of the batch and it is obviously a inefficient solution.
		// I kept it so far to maintain the code clean. In the future I will change it.
		std::pair<int, double> action_and_reward = SelectAction(future_inputs,
				episode->GetInteractions()->at(pos)->buttons_states,
				episode->GetInteractions()->at(pos)->last_commands,
				episode->GetInteractions()->at(pos)->rom_info,
				reset);

		target = episode->GetInteractions()->at(sample_pos)->immediate_reward + _gamma * action_and_reward.second;

		/*if (i % 20 == 0 && j == 0)
		{
			printf("%d de %ld: REW: %.4lf FUT: %.4lf TARGET: %.4lf\n", sample_pos, episode->GetInteractions()->size() - 1,
					episode->GetInteractions()->at(sample_pos)->immediate_reward,
					action_and_reward.second,
					target);
		}*/
	}
	if (num_forwards_before_train <= 0 || !DQN_USE_LSTM)
	{
		train_add_data_datum_vector[j].CopyFrom(_BuildAddDataDatum(
				episode->GetInteractions()->at(sample_pos)->buttons_states,
				episode->GetInteractions()->at(sample_pos)->last_commands,
				episode->GetInteractions()->at(sample_pos)->rom_info));
		train_target_datum_vector[j].CopyFrom(dummy_target_datum_vector[0]);
		train_filter_datum_vector[j].CopyFrom(dummy_filter_datum_vector[0]);
		train_target_datum_vector[j].set_float_data(episode->GetInteractions()->at(sample_pos)->action, target);
		train_filter_datum_vector[j].set_float_data(episode->GetInteractions()->at(sample_pos)->action, 1.0);
		vector<int> labels; labels.push_back(0);
		frames_input_layer_->AddMatVector(BuildMat(inputs), labels);
		if (DQN_USE_LSTM)
			reset_input_layer_->AddDatumVector(maintain_datum_vector);

		additional_data_input_layer_->AddDatumVector(train_add_data_datum_vector);
		target_input_layer_->AddDatumVector(train_target_datum_vector);
		filter_input_layer_->AddDatumVector(train_filter_datum_vector);
		solver_->Step(4);

		assert(!isnan(net_->layer_by_name("conv1_layer")->blobs().front()->data_at(1, 0, 0, 0)));
		assert(!isnan(net_->layer_by_name("conv2_layer")->blobs().front()->data_at(1, 0, 0, 0)));
		assert(!isnan(net_->layer_by_name("ip1_layer")->blobs().front()->data_at(1, 0, 0, 0)));
		assert(!isnan(net_->layer_by_name("ip2_layer")->blobs().front()->data_at(1, 0, 0, 0)));
	}
}


//void
//DqnNet::Train(DqnEpisode *episode)
//{
//	if (DQN_TRAINING_MODE == DQN_MODE_Q_LEARNING)
//		_TrainQLearning(episode);
//	else if (DQN_TRAINING_MODE == DQN_MODE_REWARD_WITH_DECAY)
//		_TrainRewardWithDecay(episode);
//	else
//		exit(printf("Training mode not found..."));
//}


void
DqnNet::TrainTransition(DqnEpisode *episode, int transition_id, int reset,  int dqn_q_learning_initialization_active)
{
	//if (current_lr > 1e-20)
	{
		if (DQN_TRAINING_MODE == DQN_MODE_Q_LEARNING)
			_TrainTransitionQLearning(episode, transition_id, reset, dqn_q_learning_initialization_active);
		else if (DQN_TRAINING_MODE == DQN_MODE_REWARD_WITH_DECAY)
			_TrainTransitionRewardWithDecay(episode, transition_id, reset);
		else
			exit(printf("Training mode not found..."));
	}
	//else
		//printf(">> Learning rate decayed to 0! Jumping train!\n");

	_iter++;

	if (_iter > DQN_GAMMA_DECAY_STEP)
	{
		_iter = 0;
		_gamma *= DQN_GAMMA_DECAY_MULT;
	}
}


void
DqnNet::SaveTrain()
{
	solver_->Snapshot();
}


void
DqnNet::LoadTrain(char *caffemodel_file)
{
	net_->CopyTrainedLayersFrom(caffemodel_file);
}

