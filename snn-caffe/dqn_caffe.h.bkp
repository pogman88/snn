
#ifndef __DQN_H_
#define __DQN_H_

#include <vector>
#include <deque>
#include <string>
#include <list>

#include <caffe/caffe.hpp>
#include <caffe/solver.hpp>
#include <caffe/layers/memory_data_layer.hpp>


#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "sns-interface/Constants.h"
#include "sns-interface/snes9x/port.h"

double get_time();
std::vector<int> parseGPUIds(std::string gpu_ids_param);
int fast_atoi(const char* str);

enum DqnTrainingModel
{
	DQN_Q_LEARNING = 0,
	DQN_TOTAL_REWARD,
	DQN_REWARD_WITH_DECAY,
	DQN_INFINITY_HORIZON_DISCOUNTED_MODEL
};


class DqnParams
{
public:
	bool USE_GPU;  					// "Use GPU to brew Caffe"
	bool USE_MULTI_GPU;				// "Use Multi GPU"
	std::vector<int> GPU_IDS;			// "GPU IDS"
	char* SOLVER_FILE; 				// "Solver parameter file (*.prototxt)"
	long REPLAY_MEMORY_SIZE; 		// "Capacity of replay memory"
	long NUM_ITERATIONS; 			// "Number of iterations needed for epsilon to reach 0.1"
	double GAMMA; 					// "Discount factor of future rewards (0,1]"
	int NUM_WARMUP_TRANSITIONS;		// "Enough amount of transitions to start learning"
	int SKIP_FRAME; 				// "Number of frames skipped"
	char *MODEL_FILE; 				// "Model file to load"
	bool EVALUATION_MODE;			// "Evaluation mode: only playing a game, no updates"
	double EVALUATE_WITH_EPSILON; 	// "Epsilon value to be used in evaluation mode"
	int REPEAT_GAMES; 				// "Number of games played in evaluation mode"

	static const int kNumCopyData = 30;
	static const int kNumAdditionalData = 1290;
	static const int kCroppedFrameSize = 160;
	static const int kCroppedFrameDataSize = kCroppedFrameSize * kCroppedFrameSize;
	static const int kInputFrameCount = 4; // @@ SNES: um frame é sempre o primeiro
	static const int kInputDataSize = kCroppedFrameDataSize * kInputFrameCount;
	static const int kMinibatchSize = 128;
	static const int kMinibatchDataSize = kInputDataSize * kMinibatchSize;
	static const int kNumCommands = TOTAL_ACTIONS; // @@ SNES: Mudar numero de comandos aqui e no arquivo prototxt - OK
	static const DqnTrainingModel TrainingModel = DQN_REWARD_WITH_DECAY;

	DqnParams(bool USE_MULTI_GPU_param = false, std::string gpu_ids_param = "0")
	{
		USE_GPU = true;
		USE_MULTI_GPU = USE_MULTI_GPU_param;
		GPU_IDS = parseGPUIds(gpu_ids_param);
		SOLVER_FILE = "dqn_solver.prototxt";
		REPLAY_MEMORY_SIZE = 30000;
		NUM_ITERATIONS = 1000000;
		GAMMA = 0.9;
		NUM_WARMUP_TRANSITIONS = 500;
		SKIP_FRAME = 1;
		MODEL_FILE = "";
		EVALUATION_MODE = false;
		EVALUATE_WITH_EPSILON = 0.05;
		REPEAT_GAMES = 1;
	}
};


template<class T, size_t array_size>
class MyArray : public std::vector<T>
{
public:
	MyArray<T, array_size>() : std::vector<T>(/*array_size*/)
	{
		this->reserve(array_size);
	}
};


typedef MyArray<uint8_t, DqnParams::kCroppedFrameDataSize> FrameData;
typedef boost::shared_ptr<FrameData> FrameDataSp;
//typedef MyArray<FrameDataSp, DqnParams::kInputFrameCount> InputFrames;
typedef std::deque<FrameDataSp> InputFrames;


typedef MyArray<float, DqnParams::kMinibatchDataSize> FramesLayerInputData;
typedef MyArray<float, DqnParams::kMinibatchSize * DqnParams::kNumCommands> TargetLayerInputData;
typedef MyArray<float, DqnParams::kMinibatchSize * DqnParams::kNumCommands> FilterLayerInputData;
typedef MyArray<float, DqnParams::kMinibatchSize * DqnParams::kNumAdditionalData> AdditionalDataLayerInputData;


class Transition
{
public:
	InputFrames input_frames;
	int action;
	boost::shared_ptr<FrameData> frame_after_action;
	double reward;
	double estimated_reward;
	bool is_final_state;
	std::list<int> commands;
	int* map_buttons;
	std::vector<uint8> rom_information;

	Transition() {
		reward = 0;
		action = 0;
		is_final_state = true;
		estimated_reward = 0;
		map_buttons = NULL;
	}

	Transition(InputFrames input_frames_param,
			int action_param,
			double reward_param,
			boost::shared_ptr<FrameData> frame_after_action_param,
			bool is_final_state_param,
			double estimated_reward_param,
			std::list<int> commands_param,
			int* map_buttons_param,
			std::vector<uint8> rom_information_param)
	{
		frame_after_action = frame_after_action_param;
		estimated_reward = estimated_reward_param;
		is_final_state = is_final_state_param;
		input_frames = input_frames_param;
		action = action_param;
		reward = reward_param;
		commands = commands_param;
		map_buttons = map_buttons_param;
		rom_information = rom_information_param;
	}
};


class DqnCaffe
{
	DqnParams _params;

	boost::shared_ptr<caffe::Blob<float> > q_values_blob_;
	boost::shared_ptr<caffe::Solver<float> > solver_;
	boost::shared_ptr<caffe::Net<float> > net_;

	std::string solver_param_;

	size_t replay_memory_capacity_;
	int current_iter_;
	double gamma_;

	std::deque<Transition> replay_memory_;

	boost::shared_ptr<caffe::MemoryDataLayer<float> > frames_input_layer_;
	boost::shared_ptr<caffe::MemoryDataLayer<float> > target_input_layer_;
	boost::shared_ptr<caffe::MemoryDataLayer<float> > filter_input_layer_;
	boost::shared_ptr<caffe::MemoryDataLayer<float> > additional_data_input_layer_;

	TargetLayerInputData dummy_input_data_;

	void InputDataIntoLayers(const FramesLayerInputData& frames_data,
			const TargetLayerInputData& target_data,
			const FilterLayerInputData& filter_data,
			const AdditionalDataLayerInputData &additional_data_input);

	std::pair<int, float> SelectActionGreedily(const InputFrames& last_frames, std::list<int> commands, int* map_buttons, std::vector<uint8> rom_information, std::vector<std::vector<float> > *qs = NULL);
	std::vector<std::pair<int, float> > SelectActionGreedily(const std::vector<InputFrames>& last_frames, std::vector<std::list<int> > commands_batch, std::vector<int*> map_buttons_batch, std::vector<std::vector<uint8> > rom_information_batch, std::vector<std::vector<float> > *qs = NULL);

public:

	DqnCaffe(DqnParams params, char *program_name, std::string (*action_to_string) (int));

	/**
	 * Initialize DQN. Must be called before calling any other method.
	 */
	void Initialize();

	/**
	 * Load a trained model from a file.
	 */
	void LoadTrainedModel(const std::string& model_file);

	/**
	 * Save a trained model from a file.
	 */
	void SaveTrainedModel();

	/**
	 * Select an action by epsilon-greedy.
	 */
	std::pair<int, float> SelectAction(const InputFrames& last_frames, double epsilon, std::list<int> commands, int* map_buttons, std::vector<uint8> rom_information);

	/**
	 * Add a transition to replay memory
	 */
	void AddTransition(const Transition& transition);

	/**
	 * Update DQN using one minibatch
	 */
	void Update();

	int memory_size() const { return replay_memory_.size(); }
	int current_iteration() const { return current_iter_; }
	DqnParams* params() { return &_params; }

	std::string (*_action_to_string) (int);
	std::string PrintQValues(const std::vector<float>& q_values);

	boost::shared_ptr<caffe::Solver<float> > GetSolver() { return solver_; }
};

void show_input_frames(InputFrames frames, int id);

#endif
