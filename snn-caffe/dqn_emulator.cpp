
// snapshots/dqn_iter_1472426.caffemodel

#include <list>
#include <math.h>
#include <time.h>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <signal.h>
#include <opencv/cv.h>
#include <caffe/caffe.hpp>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "sns-interface/Constants.h"
#include "sns-interface/SNSInterface.h"

#include "DqnUtil.h"
#include "DqnNet.h"
#include "DqnParams.h"
#include "DqnEpisode.h"
#include "DqnTrainingParams.h"

int DQN_Q_LEARNING_INITIALIZATION_ACTIVE = 0;

using namespace std;
using namespace cv;

/*********************************/
/** INTERNAL PARAMETERS - BEGIN **/
/*********************************/
const double EPSILON_INITIAL = 1.0;
/*********************************/
/** INTERNAL PARAMETERS - END ***/
/*********************************/
int how_update_epsilon = DQN_UPDATE_EPSILON_POLICY_ALLGREDY;
double EPSILON_MIN = 0.0;
double EPSILON_DELTA = (EPSILON_INITIAL - EPSILON_MIN) / DQM_UPDATE_EPSILON_DEFAULT_DECAYRATE; // subtracted from current epsilon (it will take 10^6 iters. to go from epsilon_initial to epsilon_final)

int current_state_index = 0;
int step_active = 0;
int use_greedy_policy_always = 0;
int only_run = 0;
int ai_bot = 0;
int bot_behavior = 0;
double last_game_reward = 0;
int game_over = 0;

double min_delayed_reward = DBL_MAX;
double max_delayed_reward = -DBL_MAX;
double max_reward_so_far = -DBL_MAX;
double episode_total_reward = 0;
int current_action = 1;
float current_q = 0;
double epsilon = EPSILON_INITIAL;
double total_game_time = 0;
int dqn_use_greedy_policy = 0;
int is_first_call_of_the_episode = 0;
double time_experiment_started = 0;

DqnNet *dqn;
DqnEpisode* episode;
deque<DqnEpisode*> best_episodes;
deque<DqnEpisode*> episodes;

vector<uint16> enemies;
vector<string> enemies_names;

SNSInterface *emulator;
std::deque<int> last_commands;
//int* buttons_map = (int*) malloc(TOTAL_BUTTONS*sizeof(int));

deque<Action> p1_actions;
deque<Action> p2_actions;

int player_controlled;
int bot_controlled;

double porcentagem_vitorias_corrente = 0;
double soma_sangue_lutas = 0;
vector<double> tempo_lutas;
deque<double> ultimas_vitorias;

void
update_epsilon_policy() {
	if (how_update_epsilon == DQN_UPDATE_EPSILON_POLICY_DECAYUNTIL10) {
		EPSILON_MIN = 0.10;
		EPSILON_DELTA = (EPSILON_INITIAL - EPSILON_MIN) / DQM_UPDATE_EPSILON_DEFAULT_DECAYRATE;
	} else if (how_update_epsilon == DQN_UPDATE_EPSILON_POLICY_DECAYUNTIL0_FAST) {
		EPSILON_MIN = 0.0;
		EPSILON_DELTA = (EPSILON_INITIAL - EPSILON_MIN) / (DQM_UPDATE_EPSILON_DEFAULT_DECAYRATE / 2);
	} else if (how_update_epsilon == DQN_UPDATE_EPSILON_POLICY_DECAYUNTIL0_SLOW) {
		EPSILON_MIN = 0.0;
		EPSILON_DELTA = (EPSILON_INITIAL - EPSILON_MIN) / DQM_UPDATE_EPSILON_DEFAULT_DECAYRATE;
	} else { // GREDY
		EPSILON_MIN = 0.0;
		EPSILON_DELTA = 1.0;
		epsilon = 0.0;
	}
}


/* @@ SNES - OK */
std::string actionToString(int action) {
	return action_to_string(Action(action));
}

Action intToAction(int action) {
	return int_to_action(action);
}


void start_game()
{
	while (!emulator->isGameInitiated())
	{
		emulator->performAction(PLAYER1_A_PRESS);
		emulator->performAction(PLAYER2_A_PRESS);
		emulator->act();
		emulator->performAction(PLAYER1_A_RELEASE);
		emulator->performAction(PLAYER2_A_RELEASE);
	}

	for (int i = 0; i < 170; i++)
		emulator->act();
}


void show_frame(Mat *frame)
{
	int romstate;

	Mat zoom;
	Size size(frame->cols * 1, frame->rows * 1);

	if (DQN_FRAME_CHANNELS == 1)
		zoom = Mat(size, CV_8UC1);
	else
		zoom = Mat(size, CV_8UC3);

	resize(*frame, zoom, zoom.size());
	imshow("frame", zoom);
	char key = ' ';

	if (step_active)
		key = waitKey(-1);
	else
		key = waitKey(4);

	switch (key) {
	case 'g':
		//use_greedy_policy_always = !use_greedy_policy_always;
		//fprintf(stderr, "*** USE GREEDY: %d\n", use_greedy_policy_always);
		dqn_use_greedy_policy = !dqn_use_greedy_policy;
		use_greedy_policy_always = !use_greedy_policy_always;
		fprintf(stderr, "*** USE GREEDY: %d\n", dqn_use_greedy_policy);
		break;
	case 'r':
		only_run = !only_run;
		fprintf(stderr, "*** ONLY_RUN: %d\n", only_run);
		break;
	case 's':
		step_active = !step_active;
		fprintf(stderr, "*** STEP_ACTIVE: %d\n", step_active);
		break;
	case 'p':
		/**
		 * a funcao changeBotBehavior pode receber tres valores, -1, 0 ou 1.
		 * Se ela receber -1, a IA do jogo controla o bot.
		 * Se ela receber 0, o bot fica parada.
		 * Se ela receber 1, o bot usa movimentos aleatorios.
		 */
		if (ai_bot != -1) {
			bot_behavior = !bot_behavior;
			emulator->changeBotBehavior(bot_behavior);
			fprintf(stderr, "*** BOT_BEHAVIOR: %d\n", bot_behavior);
		}
		break;
	case 'o':
		if (ai_bot != -1) {
			ai_bot = -1;
		} else {
			ai_bot = bot_behavior;
		}
		fprintf(stderr, "*** BOT_AI: %d\n", ai_bot);
		break;
	case 'n': // diminui a dificuldade (exceto o estado 0)
		// @@ FIXME : encontrar uma forma melhor de mudar os estados
		romstate = emulator->getCurrentRomState();
		if (romstate <= 0) {
			emulator->loadRomState(4);
			//printf("STATE NOT CHANGED. CURRENT IS : %d\n", romstate);
			printf("STATE CHANGED TO : %d\n", 4);
		} else {
			emulator->loadRomState(romstate - 1);
			printf("STATE CHANGED TO : %d\n", romstate - 1);
		}
		break;
	case 'm': // aumenta a dificuldade (exceto o estado 0)
		// @@ FIXME : encontrar uma forma melhor de mudar os estadoss
		romstate = emulator->getCurrentRomState();
		if (romstate >= 4) {
			emulator->loadRomState(0);
			//printf("STATE NOT CHANGED. CURRENT IS : %d\n", romstate);
			printf("STATE CHANGED TO : %d\n", 0);
		} else {
			emulator->loadRomState(romstate + 1);
			printf("STATE CHANGED TO : %d\n", romstate + 1);
		}
		break;
	}
}


// Capture the Ctrl+C command
void
shutdown_module(int signo)
{
	if (signo == SIGINT)
	{
		free(emulator);
		static bool stoped = false;

		if (!stoped)
		{
			dqn->SaveTrain();
			stoped = true;
		}

		exit(0);
	}
}

void
reinitize_experiment_configuration()
{
	last_commands.clear();

	for (int i = 0; i < DQN_NUM_PAST_COMMANDS_TO_STORE; i++)
		last_commands.push_back((int) PLAYER1_NONE);

	is_first_call_of_the_episode = 1;
	game_over = 0;
	last_game_reward = 0;
	episode_total_reward = 0;
	current_action = 1;
	current_q = 0;

	static int k = 0;

#if 0
	if (use_greedy_policy_always)
		epsilon = 0.0;
	else if (k < 0)
	{
		epsilon = 1.0;
		k++;
	}
	else
	{
#if 0
		epsilon = 1.0;
#else
		if (n_episolon < 1)
		{
			epsilon = 1.0;
			n_episolon++;
		}
		else if (n_episolon < 2)
		{
			epsilon = 0.1;
			n_episolon++;
		}
		else if (n_episolon < 3)
		{
			epsilon = 0.0;
			n_episolon = 0;
		}
		else
		{
			epsilon = 1.0;
			//n = 0;
		}
#endif

	}
#endif

#if 0
	if (n < 1)
	{
		epsilon = 1.0;
	}
	//else if (n <= 15)  //if (use_greedy_policy_always || n == 1) /*(greedy_prob < DQN_GREEDY_PROB))*/
	//{
	//	epsilon = 0.1;
	//}
	//else if (n <= 20)
	//{
	//	epsilon = 0.0;
	//}
	else
	{
		epsilon = 0.0;
		//n = 0;
	}
	n++;
#endif

}

void
reset_experiment()
{
	reinitize_experiment_configuration();

	int random_enemy = rand() % enemies.size();

	// Definindo lado do player e do bot
	if (static_cast <float> (rand()) / static_cast <float> (RAND_MAX) <= 0.5) {
		fprintf(stderr, "Player Controlled by DQN: Player 1\n");
		player_controlled = 1;
		bot_controlled = 2;
		if (current_state_index % 2 != 0) {
			current_state_index--;
		}
	} else {
		fprintf(stderr, "Player Controlled by DQN: Player 1\n");
		player_controlled = 1;
		bot_controlled = 2;
		if (current_state_index % 2 != 0) {
			current_state_index--;
		}
		//player_controlled = 2;
		//bot_controlled = 1;
		//if (current_state_index % 2 == 0) {
			//current_state_index++;
		//}
	}

	if (current_state_index <= 1)
	{
		ai_bot = 1;
		bot_behavior = 1;
		fprintf(stderr, "Dificulty level: Random\n");
	}
	else
	{
		ai_bot = 0;
		bot_behavior = 0;
		fprintf(stderr, "Dificulty level: %d\n", current_state_index / 2);
	}

	emulator->changeBotBehavior(ai_bot);
	emulator->loadRomState(current_state_index);
	emulator->resetGame();
	emulator->setOneState(MASK_RYU << 8 | enemies[random_enemy]);

	fprintf(stderr, "Enemy: %s\n", enemies_names[random_enemy].c_str());

	p1_actions.clear();
	p2_actions.clear();

	start_game();
	total_game_time = emulator->getInformation()[0]; //get_time(); // @@ SNES: deve ser o tempo do jogo - OK

	episode = new DqnEpisode();
	time_experiment_started = get_time();
	//memset(buttons_map, 0, TOTAL_BUTTONS * sizeof(int));
}


double
compute_agent_reward(double current_game_time /* unused */, double current_game_reward)
{
	static int first = 1;
	double imediate_reward;

	double punishment_for_pressed_buttons = 0;
	double punishment_for_acting = 0;

//	for (int i = 0; i < TOTAL_BUTTONS; i++)
//		if (emulator->getButtonsState()[i])
//			punishment_for_pressed_buttons += 0.0001;

	if (current_action != 1)
		punishment_for_acting = 0.0005;

	// coloquei o if porque na primeira iteracao, a conta abaixo vai retornar o sangue do oponente inteiro.
	if (!first)
	{
		//printf("Current rw: %lf last rw: %lf im: %lf\n", current_game_reward, last_reward, current_game_reward - last_reward);
		imediate_reward = current_game_reward - last_game_reward;
	}
	else
		imediate_reward = 0.0;

	//imediate_reward = imediate_reward - punishment_for_acting - punishment_for_pressed_buttons;

	episode_total_reward += imediate_reward;
	last_game_reward = current_game_reward;
	first = 0;

	return imediate_reward;
}

//void add_frame_to_list(InputFrames *frames, FrameDataSp frame, FrameDataSp firstFrame)
//{
//	frames->push_back(frame);
//
//	if ((int) frames->size() > DqnParams::kInputFrameCount)
//	{
//		//printf("OI\n");
//		//for (int i = 1; i < (int) (frames->size() - 1); i++)
//		//{
//		//	printf("COPY %d to %d\n", i, i - 1);
//		//	frames->at(i - 1) = frames->at(i);
//		//}
//
//		frames->pop_front();
//	}
//
//	if (firstFrame.get() == NULL)
//		printf("FIRST FRAME IS NULL\n");
//
//	if (firstFrame.get() == frames->at(0).get())
//		printf("EQUAL POINTER\n");
//	else
//		frames->at(0).reset(firstFrame.get());
//
//	if ((int) frames->size() > DqnParams::kInputFrameCount)
//		show_input_frames(*frames, 1);
//}


//double
//calculate_delayed_reward(DqnEpisode* episode, int t)
//{
//	double delayed_reward = 0;
//
//	for (int i = t; i < episode->GetInteractions()->size(); i++)
//		delayed_reward += (episode->GetInteractions()->at(i)->immediate_reward * pow(DQN_GAMMA, i - t));
//
//	if (delayed_reward < min_delayed_reward)
//		min_delayed_reward = delayed_reward;
//
//	if (delayed_reward > max_delayed_reward)
//		max_delayed_reward = delayed_reward;
//
//	return delayed_reward;
//}


void
apply_bonuses_for_time_of_fight(DqnEpisode* episode)
{
	int i;

	for (i = 0; i < episode->GetInteractions()->size(); i++)
	{
		//printf("imediate_reward[%d]: %.4lf -> %.4lf (%.4lf)\n", i, episode->at(i)->imediate_reward,
			//episode->at(i)->imediate_reward - ((double) i / (double) 200.0),
			//-((double) i / (double) 200.0));
		episode->GetInteractions()->at(i)->immediate_reward -= ((double) i / (double) 5000.0);
	}
}


void
apply_bonuses_for_winning_or_losing(DqnEpisode* episode)
{
	int i;
	int life_dif = fabs((int) emulator->getInformation()[bot_controlled] - (int) emulator->getInformation()[player_controlled]);
	// give the rewards just to the final transition of the fight
	int bound = 5;
	if (episode->GetInteractions()->size() < 6) {
		bound = episode->GetInteractions()->size() - 1;
	}

	if (1)
	{
		if (emulator->getInformation()[bot_controlled] < emulator->getInformation()[player_controlled])
		{

			for (int i = 1; i <= bound; i++)
			{
				// play 1 won
				episode->GetInteractions()->at(episode->GetInteractions()->size() - i)->immediate_reward += life_dif;
				printf("REWARDING VICTORY! NEW TOTAL REWARD = %lf\n", episode_total_reward += life_dif);
			}
		}
		else
		{
			for (int i = 1; i <= bound; i++)
			{
				// play 2 won
				episode->GetInteractions()->at(episode->GetInteractions()->size() - i)->immediate_reward -= life_dif;
				printf("PUNISHING LOSS! NEW TOTAL REWARD = %lf\n", episode_total_reward -= life_dif);
			}
		}

		/// play 1 won by K.O. got additional reward
		if (emulator->getInformation()[bot_controlled] <= 0)
		{
			for (int i = 1; i <= bound; i++)
			{
				episode->GetInteractions()->at(episode->GetInteractions()->size() - i)->immediate_reward += life_dif;
				printf("REWARDING K.O.! NEW TOTAL REWARD = %lf\n", episode_total_reward += life_dif);
			}
		}

		/// play 1 loss by K.O. got additional punishment
		if (emulator->getInformation()[player_controlled] <= 0)
		{
			for (int i = 1; i <= bound; i++)
			{
				episode->GetInteractions()->at(episode->GetInteractions()->size() - i)->immediate_reward -= life_dif;
				printf("PUNISHING K.O.! NEW TOTAL REWARD = %lf\n", episode_total_reward -= life_dif);
			}
		}
	}

	// give the rewards for all transitions of the fight
	if (0)
	{
		for (i = 0; i < episode->GetInteractions()->size(); i++)
		{
			if (emulator->getInformation()[bot_controlled] < emulator->getInformation()[player_controlled])
				// play 1 won
				episode->GetInteractions()->at(i)->immediate_reward += 0.5;
			else
				// play 2 won
				episode->GetInteractions()->at(i)->immediate_reward -= 0.5;

			/// play 1 won by K.O. got additional reward
			if (emulator->getInformation()[bot_controlled] <= 0)
				episode->GetInteractions()->at(i)->immediate_reward += 0.5;
		}
	}
}


void
apply_reward_decay_to_episode_rewards(DqnEpisode* episode)
{
	int i, j;
	double future_reward, decayed_reward;

	for (i = 0; i < episode->GetInteractions()->size(); i++)
	{
		episode->GetInteractions()->at(i)->immediate_reward *= 10;
	}

	for (i = 0; i < episode->GetInteractions()->size(); i++)
	{
		// A ideia aqui eh assim que encontrar uma recompensa imediata diferente de zero, eu vou backpropagando
		// a recompensa para os eventos anteriores ate esbarrar em outra recompensa imediata.
		if (episode->GetInteractions()->at(i)->immediate_reward != 0)
		{
			j = i - 1;
			future_reward = episode->GetInteractions()->at(i)->immediate_reward;

			while (j >= 0)
			{
				if (episode->GetInteractions()->at(j)->immediate_reward != 0)
					break;

				decayed_reward = future_reward * pow(dqn->Gamma(), i - j);
				episode->GetInteractions()->at(j)->immediate_reward = decayed_reward;

				//if (fabs(decayed_reward) < 0.01)
					//break;

				j--;
			}

		}
	}

	//for (i = 0; i < episode->GetInteractions()->size(); i++)
	//{
	//	printf("%d => %lf\n", i, episode->GetInteractions()->at(i)->immediate_reward);
	//}
}


void
update_rewards_to_episode_discounted_reward(DqnEpisode* episode)
{
	int i, j;
	double discounted_reward;

	for (i = 0; i < episode->GetInteractions()->size(); i++)
	{
		discounted_reward = 0.0;

		for (j = i; j < episode->GetInteractions()->size(); j++)
			discounted_reward += (pow(dqn->Gamma(), j - i) * episode->GetInteractions()->at(j)->immediate_reward);

		episode->GetInteractions()->at(i)->immediate_reward = discounted_reward;
	}

//	for (i = 0; i < episode->GetInteractions()->size(); i++)
//		printf("%d => %lf\n", i, episode->GetInteractions()->at(i)->immediate_reward);
//	exit(0);
}

/*
void add_transitions_to_replay_memory() {
	int i;
	InputFrames frames;
	double total_reward = episode_total_reward;
	double reward;

	if (episode.size() < DqnParams::kInputFrameCount + 1)
		return;

	if (DqnParams::TrainingModel == DQN_REWARD_WITH_DECAY)
	{
		apply_reward_decay_to_episode_rewards(&episode);
		//apply_bonuses_for_time_of_fight(&episode);
	}

	apply_bonuses_for_won_or_lost(&episode);

	for (i = 0; i < DqnParams::kInputFrameCount; i++)
		frames.push_back(episode[i]->frame);

	for (i = DqnParams::kInputFrameCount; i < (episode.size() - 1); i++) {
		FrameDataSp current_frame = episode[i]->frame;
		FrameDataSp frame_after_action = episode[i + 1]->frame;

		if (DqnParams::TrainingModel == DQN_Q_LEARNING || DqnParams::TrainingModel == DQN_REWARD_WITH_DECAY)
			reward = episode[i]->imediate_reward;
		// @filipe: o modelo de recompensa com desconto nao eh beeeem assim nao...
		else if (DqnParams::TrainingModel == DQN_TOTAL_REWARD)
			reward = total_reward;
		else if (DqnParams::TrainingModel == DQN_INFINITY_HORIZON_DISCOUNTED_MODEL)
			reward = calculate_delayed_reward(&episode, i);
		else
			exit(printf("ERROR: Unknown training model!\n"));

		// @@ SNES - ADICIONAR O PRIMEIRO FRAME que esta em episodies[0]
		add_frame_to_list(&frames, current_frame, episode[0]->frame);
		dqn_caffe->AddTransition(
				Transition(frames, episode[i]->action, reward,
						frame_after_action, false,
						episode[i]->q_value_of_selected_action,
						episode[i]->commands,
						episode[i]->map_buttons,
						episode[i]->rom_information));
	}

	if (DqnParams::TrainingModel == DQN_Q_LEARNING
			|| DqnParams::TrainingModel == DQN_INFINITY_HORIZON_DISCOUNTED_MODEL
			|| DqnParams::TrainingModel == DQN_REWARD_WITH_DECAY)
		reward = episode[i]->imediate_reward;
	// @filipe: esse foi um novo modelo de recompensa com desconto que eu inventei...
	else if (DqnParams::TrainingModel == DQN_TOTAL_REWARD)
		reward = total_reward;
	else
		exit(printf("ERROR: Unknown training model!\n"));

	// the last transition is a special case
	dqn_caffe->AddTransition(
			Transition(frames, episode[i]->action, reward, FrameDataSp(), true,
					episode[i]->q_value_of_selected_action,
					episode[i]->commands,
					episode[i]->map_buttons,
					episode[i]->rom_information));
}
*/


void
nullify_episode_rewards_except_final_reward(DqnEpisode* episode)
{
	for (int i = 0; i < episode->GetInteractions()->size() - 1; i++)
		episode->GetInteractions()->at(i)->immediate_reward = 0;

	episode->GetInteractions()->at(episode->GetInteractions()->size() - 1)->immediate_reward += episode_total_reward;
	printf("FINAL REWARD: %lf\n", episode->GetInteractions()->at(episode->GetInteractions()->size() - 1)->immediate_reward);
}

void
perform_episode_post_processing()
{
	apply_bonuses_for_winning_or_losing(episode);

	if (DQN_Q_LEARNING_INITIALIZATION_ACTIVE)
		update_rewards_to_episode_discounted_reward(episode);

	if (DQN_TRAINING_MODE == DQN_MODE_REWARD_WITH_DECAY)
	{
		apply_reward_decay_to_episode_rewards(episode);
		//apply_bonuses_for_time_of_fight(episode);
	}

	//nullify_episode_rewards_except_final_reward(episode);
}


void
update_epsilon()
{
	epsilon -= EPSILON_DELTA;

	if (epsilon <= EPSILON_MIN)
		epsilon = EPSILON_MIN;
}


void
save_train_if_necessary()
{
	static int n = 0;

	if (n >= 100)
	{
		dqn->SaveTrain();
		n = 0;
	}
	else
		n++;
}

void
delete_episode(DqnEpisode* episode) {
	for (int i = 0; i < episode->GetInteractions()->size(); i++)
	{
		delete(episode->GetInteractions()->at(i)->input);
		delete(episode->GetInteractions()->at(i)->rom_info);
		delete(episode->GetInteractions()->at(i)->last_commands);
		//free(episode->GetInteractions()->at(i)->buttons_states);
		delete(episode->GetInteractions()->at(i));
	}

	delete(episode);
	//episode.pop_front();
}


void
add_episode_to_list()
{
	episodes.push_back(episode);

	if (episodes.size() > DQN_NUM_EPISODES_TO_STORE)
	{
		delete_episode(episodes[0]);
		episodes.pop_front();
	}
}

void
summarize_experiment()
{
	perform_episode_post_processing();
	add_episode_to_list();

	printf("Learning rate: %.20lf Gamma: %lf\n", dqn->LearningRate(), dqn->Gamma());
	printf("NUM FRAMES: %ld NUM FRAMES PER SECOND: %.2lf\n", episode->GetInteractions()->size(),
			((double) episode->GetInteractions()->size()) / ((double) get_time() - time_experiment_started));

	if (!only_run)
	{
		int i, j, k;
		int random_ep, random_trans;
		for (i = 0; i < 1; i++)
		{
			int sequence_size = 1;// (episodes[episodes.size() - 1]->GetInteractions()->size() - DQN_NUM_INPUT_FRAMES);
			int n = (episodes[episodes.size() - 1]->GetInteractions()->size() - DQN_NUM_INPUT_FRAMES);

			for (j = 0; j < n; j++)
			{
				/***********************************/
				/** Random from the last episode **/
				/***********************************/

				random_ep = rand() % (episodes.size());
				random_trans = (rand() % (episodes[random_ep]->GetInteractions()->size() - 1 - DQN_NUM_INPUT_FRAMES - sequence_size)) +
										DQN_NUM_INPUT_FRAMES;

				for (k = 0; k < sequence_size; k++)
				{
					if ((j * sequence_size + k) % 50 == 0)
						printf("Training frame %d of %d from sequence %d of %d\n", k, sequence_size, j, n);

					if (k == 0) dqn->TrainTransition(episodes[random_ep], random_trans + k, 1, DQN_Q_LEARNING_INITIALIZATION_ACTIVE);
					else dqn->TrainTransition(episodes[random_ep], random_trans + k, 0, DQN_Q_LEARNING_INITIALIZATION_ACTIVE);
				}
			}
		}


		update_epsilon();
		save_train_if_necessary();
	}
}


Mat*
PreprocessScreen(Mat *raw_screen)
{
	static int first = 1;

	static Mat *gray = NULL;
	static Mat *resized = NULL;
	//static Mat *float_img = NULL;

	// in the first time the function is called, the matrices are allocated
	if (first)
	{
		//float_img = new Mat(Size(raw_screen->cols, raw_screen->rows), CV_32FC3);

		if (DQN_FRAME_CHANNELS == 1)
		{
			gray = new Mat(Size(raw_screen->cols, raw_screen->rows), CV_32FC1);
			resized = new Mat(Size(DQN_FRAME_DIM, DQN_FRAME_DIM), CV_32FC1);
		}
		else
		{
			resized = new Mat(Size(DQN_FRAME_DIM, DQN_FRAME_DIM), CV_32FC3);
		}

		first = 0;
	}

	//raw_screen->convertTo(*float_img, CV_32FC3);
	//(*float_img) /= 255.0;

	if (DQN_FRAME_CHANNELS == 1)
	{
		cvtColor(*raw_screen /*float_img*/, *gray, CV_BGR2GRAY);
		resize(*gray, *resized, resized->size());
	}
	else
	{
		resize(*raw_screen, *resized, resized->size());
	}

	show_frame(resized);

	return resized;
}


void
select_next_command(Mat *frame, int* buttons_states,
					std::deque<int> *last_commands,
					std::vector<uint8> *rom_info)
{
	if (episode->GetInteractions()->size() >= DQN_NUM_INPUT_FRAMES)
	{
		double prob = (double) rand() / (double) RAND_MAX;
		if (prob > epsilon)
		{
			std::pair<int, float> action_and_q;

			vector<Mat*> input;

			if (DQN_NUM_INPUT_FRAMES > 1)
			{
				input.push_back(episode->GetInteractions()->at(0)->input);

				for (int k = DQN_NUM_INPUT_FRAMES - 2; k >= 1 ; k--)
					input.push_back(episode->GetInteractions()->at(episode->GetInteractions()->size() - k)->input);
			}

			input.push_back(frame);

			if (is_first_call_of_the_episode)
			{
				action_and_q = dqn->SelectAction(input, buttons_states, last_commands, rom_info, 1);
				is_first_call_of_the_episode = 0;
			}
			else
				action_and_q = dqn->SelectAction(input, buttons_states, last_commands, rom_info, 0);

			current_action = action_and_q.first;
			current_q = action_and_q.second;
		}
		else
		{
			// TODO : Fix total commands
			current_action = rand() % DQN_NUM_COMMANDS;
			current_q = 0.0;
		}
	}
	else
	{
		current_action = 1;
		current_q = 0.0;
	}
}

/*
void
print_report()
{
	static double time_last_printf = 0;

	static int ncalls = 0;
	ncalls++;

	// printf after a given ammount of  seconds
	if (abs(get_time() - time_last_printf) > 30.0) {
		printf("Epsilon: %lf num calls per second: %.2f\n", epsilon,
				(float) ncalls / (get_time() - time_last_printf));

		if (DqnParams::TrainingModel == DQN_INFINITY_HORIZON_DISCOUNTED_MODEL) {
			if (max_delayed_reward == -DBL_MAX)
				printf(
						"MAX DELAYED REWARD: <not finished an experiment yet> MIN DELAYED REWARD: <not finished an experiment yet>\n");
			else
				printf("MAX DELAYED REWARD: %lf MIN DELAYED REWARD: %lf\n",
						max_delayed_reward, min_delayed_reward);
		}

		if (max_reward_so_far == -DBL_MAX)
			printf("MAX REWARD ACHIEVED: <not finished an experiment yet>\n");
		else
			printf("MAX REWARD ACHIEVED: %lf\n", max_reward_so_far);

		time_last_printf = get_time();
		ncalls = 0;
	}
}
*/

Scalar PixelToScalarRGB(const pixel_t& pixel) {
	int ntsc_to_rgb[] = { 0x000000, 0, 0x4a4a4a, 0, 0x6f6f6f, 0, 0x8e8e8e, 0,
			0xaaaaaa, 0, 0xc0c0c0, 0, 0xd6d6d6, 0, 0xececec, 0, 0x484800, 0,
			0x69690f, 0, 0x86861d, 0, 0xa2a22a, 0, 0xbbbb35, 0, 0xd2d240, 0,
			0xe8e84a, 0, 0xfcfc54, 0, 0x7c2c00, 0, 0x904811, 0, 0xa26221, 0,
			0xb47a30, 0, 0xc3903d, 0, 0xd2a44a, 0, 0xdfb755, 0, 0xecc860, 0,
			0x901c00, 0, 0xa33915, 0, 0xb55328, 0, 0xc66c3a, 0, 0xd5824a, 0,
			0xe39759, 0, 0xf0aa67, 0, 0xfcbc74, 0, 0x940000, 0, 0xa71a1a, 0,
			0xb83232, 0, 0xc84848, 0, 0xd65c5c, 0, 0xe46f6f, 0, 0xf08080, 0,
			0xfc9090, 0, 0x840064, 0, 0x97197a, 0, 0xa8308f, 0, 0xb846a2, 0,
			0xc659b3, 0, 0xd46cc3, 0, 0xe07cd2, 0, 0xec8ce0, 0, 0x500084, 0,
			0x68199a, 0, 0x7d30ad, 0, 0x9246c0, 0, 0xa459d0, 0, 0xb56ce0, 0,
			0xc57cee, 0, 0xd48cfc, 0, 0x140090, 0, 0x331aa3, 0, 0x4e32b5, 0,
			0x6848c6, 0, 0x7f5cd5, 0, 0x956fe3, 0, 0xa980f0, 0, 0xbc90fc, 0,
			0x000094, 0, 0x181aa7, 0, 0x2d32b8, 0, 0x4248c8, 0, 0x545cd6, 0,
			0x656fe4, 0, 0x7580f0, 0, 0x8490fc, 0, 0x001c88, 0, 0x183b9d, 0,
			0x2d57b0, 0, 0x4272c2, 0, 0x548ad2, 0, 0x65a0e1, 0, 0x75b5ef, 0,
			0x84c8fc, 0, 0x003064, 0, 0x185080, 0, 0x2d6d98, 0, 0x4288b0, 0,
			0x54a0c5, 0, 0x65b7d9, 0, 0x75cceb, 0, 0x84e0fc, 0, 0x004030, 0,
			0x18624e, 0, 0x2d8169, 0, 0x429e82, 0, 0x54b899, 0, 0x65d1ae, 0,
			0x75e7c2, 0, 0x84fcd4, 0, 0x004400, 0, 0x1a661a, 0, 0x328432, 0,
			0x48a048, 0, 0x5cba5c, 0, 0x6fd26f, 0, 0x80e880, 0, 0x90fc90, 0,
			0x143c00, 0, 0x355f18, 0, 0x527e2d, 0, 0x6e9c42, 0, 0x87b754, 0,
			0x9ed065, 0, 0xb4e775, 0, 0xc8fc84, 0, 0x303800, 0, 0x505916, 0,
			0x6d762b, 0, 0x88923e, 0, 0xa0ab4f, 0, 0xb7c25f, 0, 0xccd86e, 0,
			0xe0ec7c, 0, 0x482c00, 0, 0x694d14, 0, 0x866a26, 0, 0xa28638, 0,
			0xbb9f47, 0, 0xd2b656, 0, 0xe8cc63, 0, 0xfce070, 0 };

	int rgb = ntsc_to_rgb[pixel];
	int r = rgb >> 16;
	int g = (rgb >> 8) & 0xFF;
	int b = rgb & 0xFF;

	return Scalar(b, g, r);
}


template<class T>
T* copy_array(T* v, int n)
{
	int* w = (T*) calloc (n, sizeof(T));
	memcpy(w, v, n * sizeof(T));
	return w;
}

vector<int>
func_action_0(int action, int player) {
	vector<int> actions;
	actions.push_back(PLAYER1_NONE);
	return actions;
}

vector<int>
func_action_1(int action, int player) {
	vector<int> actions;
	actions.push_back(action);
	actions.push_back(PLAYER1_NONE);
	actions.push_back(action+TOTAL_BUTTONS);
	return actions;
}

vector<int>
func_action_11(int action, int player) {
	vector<int> actions;
	if (player == player_controlled) {
		if (emulator->getInformation()[3] == LEFT) {
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_L_PRESS);
			actions.push_back(PLAYER1_RIGHT_RELEASE);
			actions.push_back(PLAYER1_L_RELEASE);
		} else {
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_L_PRESS);
			actions.push_back(PLAYER1_LEFT_RELEASE);
			actions.push_back(PLAYER1_L_RELEASE);
		}
	} else {
		if (emulator->getInformation()[3] == LEFT) {
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_L_PRESS);
			actions.push_back(PLAYER1_LEFT_RELEASE);
			actions.push_back(PLAYER1_L_RELEASE);
		} else {
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_L_PRESS);
			actions.push_back(PLAYER1_RIGHT_RELEASE);
			actions.push_back(PLAYER1_L_RELEASE);
		}
	}
	return actions;
}

vector<int>
func_action_12(int action, int player) {
	vector<int> actions;
	if (player == player_controlled) {
		if (emulator->getInformation()[3] == LEFT) {
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_R_PRESS);
			actions.push_back(PLAYER1_LEFT_RELEASE);
			actions.push_back(PLAYER1_R_RELEASE);
		} else {
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_R_PRESS);
			actions.push_back(PLAYER1_RIGHT_RELEASE);
			actions.push_back(PLAYER1_R_RELEASE);
		}
	} else {
		if (emulator->getInformation()[3] == LEFT) {
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_R_PRESS);
			actions.push_back(PLAYER1_RIGHT_RELEASE);
			actions.push_back(PLAYER1_R_RELEASE);
		} else {
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_R_PRESS);
			actions.push_back(PLAYER1_LEFT_RELEASE);
			actions.push_back(PLAYER1_R_RELEASE);
		}
	}
	return actions;
}

vector<int>
func_action_13(int action, int player) {
	vector<int> actions;
	if (player == player_controlled) {
		if (emulator->getInformation()[3] == LEFT) {
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_RIGHT_RELEASE);
			actions.push_back(PLAYER1_L_PRESS);
			actions.push_back(PLAYER1_L_RELEASE);
		} else {
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_LEFT_RELEASE);
			actions.push_back(PLAYER1_L_PRESS);
			actions.push_back(PLAYER1_L_RELEASE);
		}
	} else {
		if (emulator->getInformation()[3] == LEFT) {
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_LEFT_RELEASE);
			actions.push_back(PLAYER1_L_PRESS);
			actions.push_back(PLAYER1_L_RELEASE);
		} else {
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_DOWN_PRESS);
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_DOWN_RELEASE);
			actions.push_back(PLAYER1_RIGHT_RELEASE);
			actions.push_back(PLAYER1_L_PRESS);
			actions.push_back(PLAYER1_L_RELEASE);
		}
	}
	return actions;
}

vector<int>
func_action_14(int action, int player) {
	vector<int> actions;
	if (player == player_controlled) {
		if (emulator->getInformation()[3] == LEFT) {
			actions.push_back(PLAYER1_UP_PRESS);
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_UP_RELEASE);
			actions.push_back(PLAYER1_RIGHT_RELEASE);
		} else {
			actions.push_back(PLAYER1_UP_PRESS);
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_UP_RELEASE);
			actions.push_back(PLAYER1_LEFT_RELEASE);
		}
	} else {
		if (emulator->getInformation()[3] == LEFT) {
			actions.push_back(PLAYER1_UP_PRESS);
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_UP_RELEASE);
			actions.push_back(PLAYER1_LEFT_RELEASE);
		} else {
			actions.push_back(PLAYER1_UP_PRESS);
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_UP_RELEASE);
			actions.push_back(PLAYER1_RIGHT_RELEASE);
		}
	}
	return actions;
}

vector<int>
func_action_15(int action, int player) {
	vector<int> actions;
	if (player == player_controlled) {
		if (emulator->getInformation()[3] == LEFT) {
			actions.push_back(PLAYER1_UP_PRESS);
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_UP_RELEASE);
			actions.push_back(PLAYER1_LEFT_RELEASE);
		} else {
			actions.push_back(PLAYER1_UP_PRESS);
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_UP_RELEASE);
			actions.push_back(PLAYER1_RIGHT_RELEASE);
		}
	} else {
		if (emulator->getInformation()[3] == LEFT) {
			actions.push_back(PLAYER1_UP_PRESS);
			actions.push_back(PLAYER1_RIGHT_PRESS);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_UP_RELEASE);
			actions.push_back(PLAYER1_RIGHT_RELEASE);
		} else {
			actions.push_back(PLAYER1_UP_PRESS);
			actions.push_back(PLAYER1_LEFT_PRESS);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_NONE);
			actions.push_back(PLAYER1_UP_RELEASE);
			actions.push_back(PLAYER1_LEFT_RELEASE);
		}
	}
	return actions;
}

vector<int>
func_action_16(int action, int player) {
	vector<int> actions;
	actions.push_back(PLAYER1_DOWN_PRESS);
	actions.push_back(PLAYER1_R_PRESS);
	actions.push_back(PLAYER1_DOWN_RELEASE);
	actions.push_back(PLAYER1_R_RELEASE);
	return actions;
}

vector<int>
func_action_17(int action, int player) {
	vector<int> actions;
	actions.push_back(PLAYER1_DOWN_PRESS);
	actions.push_back(PLAYER1_L_PRESS);
	actions.push_back(PLAYER1_DOWN_RELEASE);
	actions.push_back(PLAYER1_L_RELEASE);
	return actions;
}

vector<int> (*func_actions_ptr[DQN_NUM_COMMANDS])(int, int) = {
		func_action_0,
		func_action_1,
		func_action_1,
		func_action_1,
		func_action_1,
		func_action_1,
		func_action_1,
		func_action_1,
		func_action_1,
		func_action_1,
		func_action_1,
		func_action_11,
		func_action_12,
		func_action_13,
		func_action_14,
		func_action_15,
		func_action_16,
		func_action_17
};

bool
load_action(deque<Action>* deque, int action, int player) {
	vector<int> actions = (*func_actions_ptr[action])(action, player);

	for (int a : actions)
		deque->push_back(intToAction(a + 21*(player-1)));

	return true;
}

void
perform_aibot_action() {
	if (p2_actions.size() == 0) {
		int p2_action = rand() % DQN_NUM_COMMANDS;
		load_action(&p2_actions, p2_action, bot_controlled);

		emulator->performAction(p2_actions[0]);
#if 0
			printf("Bot -> Nova Acao para Player %d\n", bot_controlled);
			if (act2 == 0) {
				printf("Acao Atual Player 2: NONE -> %s\n", actionToString(p2_actions[0]).c_str());
			} else if (act2 > 0 && act < TOTAL_BUTTONS) {
				printf("Acao Atual Player 2: %d ->%s => %d\n",act2, actionToString(p2_actions[0]).c_str(), p2_actions[0]);
			} else if (act2 == 11) { // Haduken
				printf("Acao Atual Player 2: Haduken -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
			} else if (act2 == 12) { // Giratoria
				printf("Acao Atual Player 2: Giratoria -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
				//printf("Acao Atual Player 1: Giratoria\n");
			} else if (act2 == 13) { // Shoryuken
				printf("Acao Atual Player 2: Shoryuken -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
				//printf("Acao Atual Player 1: Shoryuken\n");
			} else if (act2 == 14) { // Cima Lado
				printf("Acao Atual Player 2: NONE -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
				//printf("Acao Atual Player 1: Giratoria\n");
			} else if (act2 == 15) { // Baixo Rasteira
				printf("Acao Atual Player 2: NONE -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
				//printf("Acao Atual Player 1: Giratoria\n");
			} else if (act2 == 16) { // Baixo Soco
				printf("Acao Atual Player 2: NONE -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
				//printf("Acao Atual Player 1: Giratoria\n");
			} else {
				printf("Acao Atual Player 2: NONE -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
				//printf("Acao Atual Player 1: NONE\n");
			}
#endif
		p2_actions.pop_front();
	} else {
#if 0
		if (act2 == 0) {
			printf("Acao Atual Player 2: NONE -> %s\n", actionToString(p2_actions[0]).c_str());
		} else if (act2 > 0 && act < TOTAL_BUTTONS) {
			printf("Acao Atual Player 2: %d ->%s => %d\n",act2, actionToString(p2_actions[0]).c_str(), p2_actions[0]);
		} else if (act2 == 11) { // Haduken
			printf("Acao Atual Player 2: Haduken -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
		} else if (act2 == 12) { // Giratoria
			printf("Acao Atual Player 2: Giratoria -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
			//printf("Acao Atual Player 1: Giratoria\n");
		} else if (act2 == 13) { // Shoryuken
			printf("Acao Atual Player 2: Shoryuken -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
			//printf("Acao Atual Player 1: Shoryuken\n");
		} else if (act2 == 14) { // Cima Lado
			printf("Acao Atual Player 2: NONE -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
			//printf("Acao Atual Player 1: Giratoria\n");
		} else if (act2 == 15) { // Baixo Rasteira
			printf("Acao Atual Player 2: NONE -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
			//printf("Acao Atual Player 1: Giratoria\n");
		} else if (act2 == 16) { // Baixo Soco
			printf("Acao Atual Player 2: NONE -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
			//printf("Acao Atual Player 1: Giratoria\n");
		} else {
			printf("Acao Atual Player 2: NONE -> %s => %d\n", actionToString(p2_actions[0]).c_str(),p2_actions[0]);
			//printf("Acao Atual Player 1: NONE\n");
		}
#endif
		emulator->performAction(p2_actions[0]);
		p2_actions.pop_front();
	}
}

double
perform_action()
{
	double reward = 0;

	load_action(&p1_actions, current_action, player_controlled);

	emulator->performAction(p1_actions[0]);
#if 0
			printf("DQN -> Nova Acao para Player %d\n", player_controlled);
			if (act == 0) {
				printf("Acao Atual Player 1: NONE -> %s\n", actionToString(p1_actions[0]).c_str());
			} else if (act > 0 && act < TOTAL_BUTTONS) {
				printf("Acao Atual Player 1: %d ->%s => %d\n",act, actionToString(p1_actions[0]).c_str(), p1_actions[0]);
			} else if (act == 11) { // Haduken
				printf("Acao Atual Player 1: Haduken -> %s => %d\n", actionToString(p1_actions[0]).c_str(),p1_actions[0]);
			} else if (act == 12) { // Giratoria
				printf("Acao Atual Player 1: Giratoria -> %s => %d\n", actionToString(p1_actions[0]).c_str(),p1_actions[0]);
				//printf("Acao Atual Player 1: Giratoria\n");
			} else if (act == 13) { // Shoryuken
				printf("Acao Atual Player 1: Shoryuken -> %s => %d\n", actionToString(p1_actions[0]).c_str(),p1_actions[0]);
				//printf("Acao Atual Player 1: Shoryuken\n");
			} else if (act == 14) { // Cima Lado
				printf("Acao Atual Player 1: Cima Direita -> %s => %d\n", actionToString(p1_actions[0]).c_str(),p1_actions[0]);
				//printf("Acao Atual Player 1: Giratoria\n");
			} else if (act == 15) { // Cima Lado
				printf("Acao Atual Player 1: Cima Esquerda -> %s => %d\n", actionToString(p1_actions[0]).c_str(),p1_actions[0]);
				//printf("Acao Atual Player 1: Giratoria\n");
			} else if (act == 16) { // Baixo Rasteira
				printf("Acao Atual Player 1: Baixo Rasteira -> %s => %d\n", actionToString(p1_actions[0]).c_str(),p1_actions[0]);
				//printf("Acao Atual Player 1: Giratoria\n");
			} else if (act == 17) { // Baixo Soco
				printf("Acao Atual Player 1: Baixo Soco -> %s => %d\n", actionToString(p1_actions[0]).c_str(),p1_actions[0]);
				//printf("Acao Atual Player 1: Giratoria\n");
			} else {
				printf("Acao Atual Player 1: NONE -> %s => %d\n", actionToString(p1_actions[0]).c_str(),p1_actions[0]);
				//printf("Acao Atual Player 1: NONE\n");
			}
#endif
	p1_actions.pop_front();

	if (ai_bot == 1) {
		perform_aibot_action();
	}

	reward = emulator->act();

	// fica alguns frames realizando acoes vazias para pular alguns frames
	for (int i = 0; (i < 1) && (!game_over); i++)
	{
		reward = emulator->act();

		if (emulator->gameOver())
			game_over = 1;
	}

	if (emulator->gameOver())
		game_over = 1;

	return reward / 100.0;
}


void
dqn_step()
{
	double start_time = get_time();

	if (!emulator->actionIsLegal(player_controlled) || p1_actions.size() > 0) {
		if (ai_bot == 1) {
			perform_aibot_action();
		}

		if (p1_actions.size() > 0) {
			emulator->performAction(p1_actions[0]);
			p1_actions.pop_front();
		}

		emulator->act();

		if (emulator->gameOver())
			game_over = 1;

		return;
	}

	for (int i = 0; i < DQN_REPEAT_ACTIONS && !game_over; i++)
	{
		Mat raw_frame = emulator->getScreen();
		Mat *frame = PreprocessScreen(&raw_frame);

		double imediate_reward = -DBL_MAX;
		double reward = -DBL_MAX;

		int *button_state = copy_array<int>(emulator->getButtonsState(), TOTAL_BUTTONS);
		std::vector<uint8_t> *infos = new std::vector<uint8_t>(emulator->getInformation());
		Mat *frame_copy = new Mat(Size(frame->cols, frame->rows), frame->type());
		frame->copyTo(*frame_copy);
		deque<int> *last_commands_copy = new deque<int>(last_commands);

		// apenas na primeira iteracao escolhe qual acao realizar. nas outras
		// a acao escolhida eh repetida.
		if (i == 0)
			select_next_command(frame, button_state, &last_commands, infos);

		show_control(emulator->getButtonsState(), current_action);
		reward = perform_action();

		last_commands.push_back(current_action);
		last_commands.pop_front();

		// @@ SNES: obter a recompensa
		double current_game_time = (double) emulator->getInformation()[0];
		imediate_reward = compute_agent_reward(total_game_time - current_game_time, reward); /* @@ SNES: o tempo deve ser o tempo do jogo **/

		// immediate rewards bigger than 1.0 usually lead to gradient explosion.
		// note: in deep q-learning, if the average immediate reward is not zero, the net estimations tend to infinity (positive or negative).
		// I believe it is due to the temporal difference error formula: error = Q(t) - immediate_reward + gamma * Q(t + 1). A non-zero immediate
		// reward will make the net prediction grow (positively or negatively) by summing a non-zero term on its estimation.
		assert(fabs(imediate_reward) <= 10.0);

		if (use_greedy_policy_always && step_active)
		{
			fprintf(stderr, "CURR REWARD: %lf LAST REWARD: %lf (IMED = CURR - LAST) NUM FRAMES: %ld COMMAND: %d EST RW: %lf\n",
							reward, last_game_reward, episode->GetInteractions()->size(), current_action, current_q);
			fprintf(stderr, "imediate reward: %.2lf total reward: %.2lf\n",
						imediate_reward, episode_total_reward);
		}

		// add the data to the episode
		episode->AddInteration(new DqnInteration(frame_copy, imediate_reward,
				current_action, button_state, last_commands_copy, infos));
	}

	static int n = 0;
	if (n >= 25)
	{
		if (dqn_use_greedy_policy || epsilon == 0) fprintf(stderr, "** dqn_use_greedy_policy\tCURRENT ACTION %d\n", current_action);
		fprintf(stderr, "STEP DURATION: %.2lf EPISLON: %.2lf DELTA: %.2f\n", get_time() - start_time, epsilon, EPSILON_DELTA);
		n = 0;
	}
	else
		n++;
}

//void initialize_caffe()
//{
//	char *program_name = "dqn";
//	DqnParams params;
//	dqn_caffe = new DqnCaffe(params, program_name, actionToString);
//	dqn_caffe->Initialize();
//
//	// Load the pre-trained model
//	if (model_file_path.size() > 0) {
//		const std::string path = model_file_path;
//		dqn_caffe->LoadTrainedModel(path);
//	}
//}

void
initialize_dqn()
{
	if (DQN_USE_LSTM)
		dqn = new DqnNet("dqn_solver-lstm.prototxt");
	else
		dqn = new DqnNet("dqn_solver.prototxt");
}


void
initialize_emulator(int argc, char** argv)
{
	/** @@ SNES **/
	emulator = new SNSInterface(argv[1]);
	emulator->init();

	if (emulator->isSnnInitiated())
	{
		emulator->loadROM();
	}
	else
	{
		printf("Wasn't possible to initiate the emulator");
		exit(1);
	}
}


void
message_how_use()
{
	printf("Enter with the rom directory.\nOptional: Enter with the caffemodel name");
}

void
test_current_model() {
	double last_epsilon = epsilon;

	reset_experiment();

#if 1
	// USE GREDLY
	epsilon = 0.0;
#endif

	while (!game_over)
	{
		dqn_step();
	}

	for (int i = 0; i < 10; i++)
		emulator->act();

	delete_episode(episode);

	epsilon = last_epsilon;

	soma_sangue_lutas += emulator->getInformation()[player_controlled] - emulator->getInformation()[bot_controlled];

	double value_vit = 0.0;
	if (emulator->getInformation()[bot_controlled] < emulator->getInformation()[player_controlled]) {
		porcentagem_vitorias_corrente++;
		value_vit = 1.0;
	}

	if (ultimas_vitorias.size() >= TRAINPARAMS_BATCH_TEST)
		ultimas_vitorias.pop_front();

	ultimas_vitorias.push_back(value_vit);

	tempo_lutas.push_back(153.0 - (double) emulator->getInformation()[0]);
}

void
complex_test(char* name, int difficult) {
	current_state_index = difficult;
	tempo_lutas.clear();
	soma_sangue_lutas = 0;
	porcentagem_vitorias_corrente = 0;
	ultimas_vitorias.clear();
	test_current_model();
	double total_tempo = 0;
	for (double tempo : tempo_lutas) {
		total_tempo += tempo;
	}
	fprintf(stderr, "%s-RESULTADOS-%d::\tDIFSANGUE\t%.4f\tVIT\t%.4f\tMEDIATEMPO\t%.4f\tDPTEMPO\t%.4f\tTOTALTEMPO\t%.4f\tBATCHVITORIAS\t%.4f\tEPSILON\t%.4f\n\n",
						name,
						1,
						soma_sangue_lutas,
						porcentagem_vitorias_corrente,
						total_tempo,
						0.0,
						total_tempo,
						porcentagem_vitorias_corrente,
						0.0);
}

void
complex_test_current_model(char* prefix = "") {
	int difficult = current_state_index;

	// Testing with AL
	current_state_index = 0;
	tempo_lutas.clear();
	soma_sangue_lutas = 0;
	porcentagem_vitorias_corrente = 0;
	ultimas_vitorias.clear();

	for (int i = 1; i <= TRAINPARAMS_BATCH_TEST; i++) {
		test_current_model();

		// Avaliate the test
		long int total_tempo = 0;
		double media_tempo = 0;
		double desviopadrao_tempo = 0;

		for (double tempo : tempo_lutas) {
			total_tempo += tempo;
		}

		media_tempo = total_tempo / (double) i;

		for (double tempo : tempo_lutas) {
			desviopadrao_tempo += (tempo - media_tempo) * (tempo - media_tempo);
		}

		desviopadrao_tempo = sqrt(desviopadrao_tempo / (double) i);

		double pvc = porcentagem_vitorias_corrente / (double) i;
		if (ultimas_vitorias.size() >= TRAINPARAMS_BATCH_TEST) {
			pvc = 0;
			for (double value : ultimas_vitorias) {
				pvc += value;
			}
			pvc /= (double) TRAINPARAMS_BATCH_TEST;
		}

		fprintf(stderr, "%sTESTE-AL-RESULTADOS-%d::\tDIFSANGUE\t%.4f\tVIT\t%.4f\tMEDIATEMPO\t%.4f\tDPTEMPO\t%.4f\tTOTALTEMPO\t%ld\tBATCHVITORIAS\t%.4f\tEPSILON\t%.4f\n\n",
					prefix,
					i,
					soma_sangue_lutas / (double) i,
					porcentagem_vitorias_corrente / (double) i,
					media_tempo,
					desviopadrao_tempo,
					total_tempo,
					pvc,
					epsilon);
	}

	// Testing with N0
	char aux[50];
	strcpy(aux, prefix);
	complex_test(strcat(aux, "TESTE-N0"), 2);

	// Testing with N1
	strcpy(aux, prefix);
	complex_test(strcat(aux, "TESTE-N1"), 4);

	// Testing with N2
	strcpy(aux, prefix);
	complex_test(strcat(aux, "TESTE-N2"), 6);

	// Testing with N3
	strcpy(aux, prefix);
	complex_test(strcat(aux, "TESTE-N3"), 8);

	current_state_index = difficult;
}

void
avaliate_training(int iter) {
	fprintf(stderr, "\nAvaliando Treinamento...\n\n");

	static int ii = 1;

	test_current_model();

	// Avaliate the test
	long int total_tempo = 0;
	double media_tempo = 0;
	double desviopadrao_tempo = 0;

	for (double tempo : tempo_lutas) {
		total_tempo += tempo;
	}

	media_tempo = total_tempo / (double) ii;

	for (double tempo : tempo_lutas) {
		desviopadrao_tempo += (tempo - media_tempo) * (tempo - media_tempo);
	}

	desviopadrao_tempo = sqrt(desviopadrao_tempo / (double) ii);

	double pvc = porcentagem_vitorias_corrente / (double) ii;
	if (ultimas_vitorias.size() >= TRAINPARAMS_BATCH_TEST) {
		pvc = 0;
		for (double value : ultimas_vitorias) {
			pvc += value;
		}
		pvc /= (double) TRAINPARAMS_BATCH_TEST;
	}

	fprintf(stderr, "RESULTADOS-%d::\tDIFSANGUE\t%.4f\tVIT\t%.4f\tMEDIATEMPO\t%.4f\tDPTEMPO\t%.4f\tTOTALTEMPO\t%ld\tBATCHVITORIAS\t%.4f\tEPSILON\t%.4f\n",
				iter,
				soma_sangue_lutas / (double) ii,
				porcentagem_vitorias_corrente / (double) ii,
				media_tempo,
				desviopadrao_tempo,
				total_tempo,
				pvc,
				epsilon);
	ii++;

	fprintf(stderr, "\nFim Avaliacao...\n\n");

	// If win 100% then
	if ( pvc >= TRAINPARAMS_PERCENTAGE_BATCH_VICTORYS_TEST && ii >= TRAINPARAMS_BATCH_TEST) {
		fprintf(stderr, "Fim Treinamento. 30 vitorias em sequencia alcancada\n\nIniciando Teste Complexo...\n\n");

		complex_test_current_model();

		fprintf(stderr, "\nFim Teste Complexo\n. Aperte Ctrl + C para salvar.\n");

		while (1) {}
	}

	if (iter % TRAINPARAMS_PERIODIC_TEST == 0) {

		char prefix[50] = "";
		sprintf(prefix, "PERIODICO-%d-", iter);
		fprintf(stderr, "Teste Periodico Iter: %d\n\n", iter);
		complex_test_current_model(prefix);
		fprintf(stderr, "\nFim Teste Periodico: %d\n\n", iter);
	}

	fprintf(stderr, "Continuando Treinamento...\n");
}

int
main(int argc, char **argv)
{
	if (argc < 2 || argc > 3)
	{
		message_how_use();
		return 0;
	}

	signal(SIGINT, shutdown_module);

	//initialize_caffe();
	initialize_emulator(argc, argv);
	initialize_dqn();

	if (argc == 3)
		dqn->LoadTrain(argv[2]);

	srand(time(NULL));

	ai_bot = 1;
	bot_behavior = 1;

	enemies.push_back(MASK_KEN);
	enemies_names.push_back("MASK_KEN");

	step_active = 0;
	only_run = 0;
	use_greedy_policy_always = 0;
	how_update_epsilon = DQN_UPDATE_EPSILON_POLICY_DECAYUNTIL0_SLOW;
	update_epsilon_policy();

	int iter = 1;

	while (iter <= TRAINPARAMS_NUMBER_TRAINING)
	{
		reset_experiment();

		while (!game_over)
		{
			dqn_step();
		}

		for (int i = 0; i < 10; i++)
			emulator->act();

		summarize_experiment();

		avaliate_training(iter);

		iter++;
	}

	fprintf(stderr, "Numero máximo de iteracoes alcancado. Sem ocorrer convergencia...\n");

	fprintf(stderr, "\nFim Teste Complexo\n. Aperte Ctrl + C para salvar.\n");

	while(1) {};

	return (0);
}

