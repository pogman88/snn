#include "dqn_caffe.h" // @@ SNES: Mudar numero de comandos aqui e no arquivo prototxt - OK

#include <vector>
#include <iostream>
#include <stdlib.h>

#include <caffe/caffe.hpp>
#include <caffe/solver.hpp>
#include <caffe/sgd_solvers.hpp>
#include <caffe/util/io.hpp>
#include <caffe/layers/memory_data_layer.hpp>

#include <boost/smart_ptr.hpp>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>

int fast_atoi(const char* str) {
	int val = 0;
	while(*str) {
		val = val*10 + (*str++ - '0');
	}
	return val;
}

std::vector<int> parseGPUIds(std::string gpu_ids_param) {
	std::vector<int> gpu_ids;

	char* scopy = (char*) malloc(gpu_ids_param.size()*sizeof(char));
	strcpy(scopy, gpu_ids_param.c_str());
	char* ids = strtok(scopy, ",");

	if (gpu_ids_param.size() == 0 || ids == NULL) {
		gpu_ids.push_back(0);
		return gpu_ids;
	} else if (gpu_ids_param.size() == 1) {
		gpu_ids.push_back(fast_atoi(gpu_ids_param.c_str()));
		return gpu_ids;
	}

	while (ids != NULL) {
		gpu_ids.push_back(atoi(ids));
		ids = strtok(NULL, ",");
	}

	free(scopy);

	return gpu_ids;
}

double get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);

	double time_in_sec = (tv.tv_sec) + ((double) tv.tv_usec * (double) 10e-7);
	return time_in_sec;
}

// only for debug
void show_input_frames(InputFrames frames, int id)
{
	cv::Mat img = cv::Mat(cv::Size(DqnParams::kInputFrameCount * DqnParams::kCroppedFrameSize,
					DqnParams::kCroppedFrameSize), CV_8UC1);

	cv::Mat res = cv::Mat(cv::Size(2 * DqnParams::kInputFrameCount * DqnParams::kCroppedFrameSize,
					2 * DqnParams::kCroppedFrameSize), CV_8UC1);

	memset(img.data, 0, DqnParams::kInputFrameCount * DqnParams::kCroppedFrameSize * DqnParams::kCroppedFrameSize * sizeof(uchar));
	memset(res.data, 0, 2 * DqnParams::kInputFrameCount * DqnParams::kCroppedFrameSize * 2 * DqnParams::kCroppedFrameSize * sizeof(uchar));

	for (int k = 0; k < DqnParams::kInputFrameCount && k < frames.size(); k++)
	{
		for (int i = 0; i < DqnParams::kCroppedFrameSize; i++)
		{
			for (int j = 0; j < DqnParams::kCroppedFrameSize; j++)
			{
				int frame_p = i * DqnParams::kCroppedFrameSize + j;
				int img_p = i * img.step + k * DqnParams::kCroppedFrameSize + j;

				if (isnan((double) frames[k]->at(frame_p)))
					exit(printf("Problem!!! frame %d row %d col %d\n", k, i,j));

				img.data[img_p] = frames[k]->at(frame_p);
			}
		}
	}

//	static int first = 1;
//
//	if (first == 1)
//	{
//		cv::namedWindow("frame 0");
//		cv::namedWindow("frame 8");
//		cv::namedWindow("frame 20");
//		cv::namedWindow("frame 31");
//
//		cv::moveWindow("frame 0", 800, 0);
//		cv::moveWindow("frame 8", 800, 250);
//		cv::moveWindow("frame 20", 800, 500);
//		cv::moveWindow("frame 31", 800, 750);
//
//		first = 0;
//	}

	char name[64];
	sprintf(name, "frame %d", id);
	cv::resize(img, res, res.size());
	cv::imshow(/*"input frames"*/name, res);
	cv::waitKey(1);
	//printf("Show %s\n", name);
}


template<typename Dtype>
bool HasBlobSize(const caffe::Blob<Dtype>& blob, const int num,
		const int channels, const int height, const int width) {
	return ((blob.num() == num) && (blob.channels() == channels)
			&& (blob.height() == height) && (blob.width() == width));
}

std::string DqnCaffe::PrintQValues(const std::vector<float>& q_values) {
	assert(q_values.size() == _params.kNumCommands);

	std::ostringstream actions_buf;
	std::ostringstream q_values_buf;

	for (size_t i = 0; i < q_values.size(); ++i) {
		const std::string a_str = boost::algorithm::replace_all_copy(_action_to_string(i), "PLAYER_A_", "");
		const std::string q_str = boost::lexical_cast<std::string> (q_values[i]);
		const int column_size = std::max(a_str.size(), q_str.size()) + 1;

		actions_buf.width(column_size);
		actions_buf << a_str;

		q_values_buf.width(column_size);
		q_values_buf << q_str;
	}

	actions_buf << std::endl;
	q_values_buf << std::endl;

	return actions_buf.str() + q_values_buf.str();
}

DqnCaffe::DqnCaffe(DqnParams params, char *program_name __attribute__((unused)),
		std::string (*action_to_string)(int)) {
	_params = params;

	if (params.USE_GPU) {
		caffe::Caffe::SetDevice(params.GPU_IDS[0]);
		caffe::Caffe::set_mode(caffe::Caffe::GPU);
		if (params.USE_MULTI_GPU) {
			caffe::Caffe::set_solver_count(params.GPU_IDS.size());
		}
	} else {
		caffe::Caffe::set_mode(caffe::Caffe::CPU);
	}

	replay_memory_capacity_ = params.REPLAY_MEMORY_SIZE;
	solver_param_ = params.SOLVER_FILE;
	gamma_ = params.GAMMA;
	current_iter_ = 0;

	_action_to_string = action_to_string;
}

void DqnCaffe::Initialize() {
	caffe::SolverParameter solver_param;
	caffe::ReadProtoFromTextFileOrDie(_params.SOLVER_FILE, &solver_param);
	solver_.reset(caffe::SolverRegistry<float>::CreateSolver(solver_param));

	net_ = solver_->net();

	// Cache pointers to blobs that hold Q values
	q_values_blob_ = net_->blob_by_name("q_values");

	// Initialize dummy input data with 0
	std::fill(dummy_input_data_.begin(), dummy_input_data_.end(), 0.0);

	// Cache pointers to input layers
	frames_input_layer_ = boost::dynamic_pointer_cast<
			caffe::MemoryDataLayer<float> >(
			net_->layer_by_name("frames_input_layer"));

	assert(frames_input_layer_);
	assert(HasBlobSize(*net_->blob_by_name("frames"),
			DqnParams::kMinibatchSize, DqnParams::kInputFrameCount,
			DqnParams::kCroppedFrameSize,
			DqnParams::kCroppedFrameSize));

	target_input_layer_ = boost::dynamic_pointer_cast<
			caffe::MemoryDataLayer<float> >(
			net_->layer_by_name("target_input_layer"));

	assert(target_input_layer_);
	assert(HasBlobSize(*net_->blob_by_name("target"),
			DqnParams::kMinibatchSize, DqnParams::kNumCommands, 1, 1));

	filter_input_layer_ = boost::dynamic_pointer_cast<
			caffe::MemoryDataLayer<float> >(
			net_->layer_by_name("filter_input_layer"));

	assert(filter_input_layer_);
	assert(HasBlobSize(*net_->blob_by_name("filter"),
			DqnParams::kMinibatchSize, DqnParams::kNumCommands, 1, 1));

	additional_data_input_layer_ = boost::dynamic_pointer_cast<
			caffe::MemoryDataLayer<float> >(
			net_->layer_by_name("additional_data_input_layer"));

	assert(additional_data_input_layer_);
	assert(HasBlobSize(*net_->blob_by_name("additional_data"),
			DqnParams::kMinibatchSize, DqnParams::kNumAdditionalData, 1,1));
}

void DqnCaffe::LoadTrainedModel(const std::string& model_file) {
	net_->CopyTrainedLayersFrom(model_file);
}

void DqnCaffe::SaveTrainedModel() {
	solver_->Snapshot();
}

void DqnCaffe::InputDataIntoLayers(const FramesLayerInputData &frames_input,
		const TargetLayerInputData &target_input,
		const FilterLayerInputData &filter_input,
		const AdditionalDataLayerInputData &additional_data_input) {
	frames_input_layer_->Reset(const_cast<float*>(frames_input.data()),
			dummy_input_data_.data(), DqnParams::kMinibatchSize);
	target_input_layer_->Reset(const_cast<float*>(target_input.data()),
			dummy_input_data_.data(), DqnParams::kMinibatchSize);
	filter_input_layer_->Reset(const_cast<float*>(filter_input.data()),
			dummy_input_data_.data(), DqnParams::kMinibatchSize);

	if (additional_data_input.size() != DqnParams::kNumAdditionalData * DqnParams::kMinibatchSize)
	{
		printf("PROBLEMAA!!! additional_data_input.size(): %ld Deveria ser %d x %d = %d\n", additional_data_input.size(),
			DqnParams::kNumAdditionalData, DqnParams::kMinibatchSize, DqnParams::kNumAdditionalData * DqnParams::kMinibatchSize);

		exit(0);
	}

	additional_data_input_layer_->Reset(
			const_cast<float*>(additional_data_input.data()),
			dummy_input_data_.data(), DqnParams::kMinibatchSize);
}

std::pair<int, float> DqnCaffe::SelectActionGreedily(
		const InputFrames& last_frames, std::list<int> commands,
		int* map_buttons, std::vector<uint8> rom_information,
		std::vector<std::vector<float> > *qs /* default = NULL */) {
	std::vector<InputFrames> frames_vector;

	frames_vector.push_back(last_frames);

	std::vector<std::list<int> > commands_batch;
	commands_batch.push_back(commands);

	std::vector<int*> map_buttons_batch;
	map_buttons_batch.push_back(map_buttons);

	std::vector<std::vector<uint8> > rom_information_batch;
	rom_information_batch.push_back(rom_information);

	return SelectActionGreedily(frames_vector,
			commands_batch, map_buttons_batch, rom_information_batch, qs).front();
}

std::vector<std::pair<int, float> > DqnCaffe::SelectActionGreedily(
		const std::vector<InputFrames>& last_frames_batch,
		std::vector<std::list<int> > commands_batch,
		std::vector<int*> map_buttons_batch,
		std::vector<std::vector<uint8> > rom_information_batch,
		std::vector<std::vector<float> > *qs /* default = NULL */) {
	assert(last_frames_batch.size() <= DqnParams::kMinibatchSize);

	MyArray<float, DqnParams::kMinibatchDataSize> frames_input;
	AdditionalDataLayerInputData additional_data;

	for (size_t i = 0; i < DqnParams::kMinibatchSize; ++i) {
		// Input frames to the net and compute Q values for each legal actions
		for (int j = 0; j < DqnParams::kInputFrameCount; ++j) {
			if (i < last_frames_batch.size()) {
				const FrameDataSp& frame_data = last_frames_batch[i][j];

				for (int k = 0; k < DqnParams::kCroppedFrameDataSize; k++)
					frames_input[i * DqnParams::kInputDataSize + j * DqnParams::kCroppedFrameDataSize + k] = (float) frame_data->at(k) / 255.0;

				//std::copy(frame_data->begin(), frame_data->end(),
				//	frames_input.begin() + i * DqnParams::kInputDataSize + j * DqnParams::kCroppedFrameDataSize);
			} else {
				for (int k = 0; k < DqnParams::kCroppedFrameDataSize; k++)
					frames_input[i * DqnParams::kInputDataSize + j * DqnParams::kCroppedFrameDataSize + k] = 0;
			}
		}
	}

	for (size_t i = 0; i < DqnParams::kMinibatchSize; ++i)
	{
		if (i < commands_batch.size())
		{
			std::list<int>::iterator it;

			for (it = commands_batch[i].begin(); it != commands_batch[i].end();	it++)
			{
				float command = (float) (*it) / 10.0;

				for (int j = 0;	j < DqnParams::kNumCopyData; j++)
				{
					additional_data.push_back(command);
				}
			}
		}
		else
		{
			for (int j = 0; j < commands_batch[0].size() * DqnParams::kNumCopyData; j++)
			{
				additional_data.push_back(0);
			}
		}

		if (i < map_buttons_batch.size())
		{
			int *map = map_buttons_batch[i];

			for (int j = 0; j < TOTAL_BUTTONS; j++)
			{
				for (int j = 0; j < DqnParams::kNumCopyData; j++)
				{
					additional_data.push_back(map[j]);
				}
			}
		}
		else
		{
			for (int j = 0; j < TOTAL_BUTTONS * DqnParams::kNumCopyData; j++)
			{
				additional_data.push_back(0);
			}
		}

		if (i < rom_information_batch.size())
		{
			std::vector<std::vector<uint8> >::iterator it_batch;
			std::vector<uint8> it;

			for (uint8 v : rom_information_batch[i])
			{
				float info = (float) v / 100.0;

				for (int j = 0; j < DqnParams::kNumCopyData; j++)
				{
					additional_data.push_back(info);
				}
			}
		}
		else
		{
			for (int j = 0; j < DqnParams::kNumCopyData * rom_information_batch[0].size(); j++)
			{
				additional_data.push_back(0);
			}
		}
	}

	InputDataIntoLayers(frames_input, dummy_input_data_, dummy_input_data_, additional_data);
	net_->ForwardPrefilled(NULL);

	std::vector<std::pair<int, float> > results;
	results.reserve(last_frames_batch.size());

	if (qs != NULL)
		qs->clear();

	for (size_t i = 0; i < last_frames_batch.size(); ++i) {
		std::vector<float> q_values(_params.kNumCommands);

		for (size_t action_id = 0; action_id < _params.kNumCommands;
				action_id++) {
			float q = q_values_blob_->data_at(i, action_id, 0, 0);

			if (isnan(q))
				q = 0;

			q_values[action_id] = q;
		}

		if (qs != NULL)
			qs->push_back(q_values);

		// Select the action with the maximum Q value
		const int max_idx = std::distance(q_values.begin(), std::max_element(q_values.begin(), q_values.end()));
		results.push_back(std::pair<int, float>(max_idx, q_values[max_idx]));
	}

	return results;
}

std::pair<int, float> DqnCaffe::SelectAction(const InputFrames& last_frames,
		double epsilon, std::list<int> commands, int* map_buttons,
		std::vector<uint8> rom_information) {
	double probability;
	std::pair<int, float> output;

	assert(epsilon >= 0.0 && epsilon <= 1.0);
	probability = (double) rand() / (double) RAND_MAX;

	std::vector<std::vector<float> > qs;
	output = SelectActionGreedily(last_frames, commands, map_buttons,
			rom_information, &qs);

	show_input_frames(last_frames, 0);

	static int first = 1;

	if (first)
	{
		cv::waitKey(-1);
		first = 0;
	}

	//if (epsilon < 0.1)
	//{
	static int n = 0;
	if (n >= 40)
	{
		for (int i = 0; i < qs[0].size(); i++)
			fprintf(stderr, "(%d: %.2lf), ", i, qs[0][i]);
		fprintf(stderr, "\nOutput: %d prob: %lf epsilon: %lf\n", output.first, probability, epsilon);

		n = 0;
	}
	else
		n++;
	//}

	if (probability < epsilon) // Select randomly
	{
		int random_idx = rand() % _params.kNumCommands;

		output.first = random_idx;
		output.second = qs.front().at(random_idx); // value predicted by the network for this action

		return output;
	} else {
		return output;
	}
}

void DqnCaffe::AddTransition(const Transition& transition)
{
	if (replay_memory_.size() == replay_memory_capacity_)
	{
		//delete(replay_memory_.front().frame_after_action.get());
		free(replay_memory_.front().map_buttons);
		replay_memory_.pop_front();
	}

	replay_memory_.push_back(transition);
}


bool sortReplayMemory(const Transition a, const Transition b)
{
	return (a.reward < b.reward);
}


void DqnCaffe::Update()
{
	current_iter_++;

	int i, j, random_transition_id;

	// DEBUG:
	// std::cout << "iteration: " << current_iter_ << std::endl;

	// Sample transitions from replay memory
	std::vector<int> transitions;
	transitions.reserve(DqnParams::kMinibatchSize);

	std::deque<Transition> replay_memory_sorted = replay_memory_;
	//std::sort(replay_memory_sorted.begin(), replay_memory_sorted.end(), sortReplayMemory);

//	int end_pos = replay_memory_sorted.size()-1;
//	int begin_pos = end_pos - 0.05 * replay_memory_sorted.size();

	for (i = 0; i < DqnParams::kMinibatchSize; ++i) {

		int steps = 0;
		// @FILIPE: NAO ENTENDI ESSE DO WHILE
		/*
		do {
			random_transition_id = rand() % replay_memory_.size();
			steps++;
		} while (replay_memory_[random_transition_id].reward == 0 && steps < 10);
		*/

		random_transition_id = rand() % replay_memory_.size();
		transitions.push_back(random_transition_id);
	}

	// Compute target values: max_a Q(s',a)
	std::vector<InputFrames> target_last_frames_batch;
	/**
	 * OBS: The Q-learning update is given by: Qt(s, a) = Qt-1(s, a) + alpha * (imediate_reward + Qt-1(s', a) - Qt-1), where s'
	 *  is the next state, achieved by taking action a in state s. The loop below and the following call of SelectActionGreedily are
	 *  intended to compute Qt-1(s', a). This is why the inner loop starts with j=1, and the frame achieved AFTER the execution of the
	 *  action is pushed_back to the input frames, and these input frames are used to estimate the Q value of the best action in the next state.
	 */

	std::vector<std::list<int> > commands_batch;
	std::vector<int*> map_buttons_batch;
	std::vector<std::vector<uint8> > rom_information_batch;

	for (i = 0; i < (int) transitions.size(); i++) {
		int idx = transitions[i];
		Transition& transition = replay_memory_[idx];

		if (transition.is_final_state) {
			// This is a terminal state
			continue;
		}

		InputFrames target_last_frames;

		for (j = 1; j < DqnParams::kInputFrameCount; ++j)
			target_last_frames.push_back(transition.input_frames[j]);

		target_last_frames.push_back(transition.frame_after_action);
		//show_input_frames(target_last_frames, 0);

		//additional_data_batch.push_back(0);
		commands_batch.push_back(transition.commands);
		map_buttons_batch.push_back(transition.map_buttons);
		rom_information_batch.push_back(transition.rom_information);
		target_last_frames_batch.push_back(target_last_frames);
	}

	std::vector<std::pair<int, float> > actions_and_values = SelectActionGreedily(target_last_frames_batch, commands_batch, map_buttons_batch, rom_information_batch);

//	printf("BATCH ESTIMATION: ");
//	for (int i = 0; i < actions_and_values.size(); i++)
//	{
//		fprintf(stderr, "%d\t%d: %.2f\n", i, actions_and_values[i].first, actions_and_values[i].second);
//	}

	// frames_input: 4 frames
	static FramesLayerInputData frames_input;
	// target_input: expected rewards for each action -> this value is compared to the q-value predicted by the network and the difference is used to update the weights
	static TargetLayerInputData target_input;
	// filter_input: 0 for all actions except the performed for which the value is 1
	static FilterLayerInputData filter_input;
	static AdditionalDataLayerInputData additional_data_input;

	static int first = 1;

	if (first) {
		for (i = 0; i < frames_input.capacity(); i++)
			frames_input.push_back(0);

		for (i = 0; i < filter_input.capacity(); i++)
			filter_input.push_back(0);

		for (i = 0; i < target_input.capacity(); i++)
			target_input.push_back(0);

		for (i = 0; i < additional_data_input.capacity(); i++)
			additional_data_input.push_back(0);

		first = 0;
	} else {
		// Set all filters and target to 0 every iteration. In the
		// loop below, only the performed action in a transition and
		// the expected reward of this action will be turned on.
		for (i = 0; i < filter_input.size(); i++)
			filter_input[i] = 0;

		for (i = 0; i < target_input.size(); i++)
			target_input[i] = 0;

		for (i = 0; i < frames_input.size(); i++)
			frames_input[i] = 0;

		for (i = 0; i < additional_data_input.size(); i++)
			additional_data_input[i] = 0;
	}

	int transition_id_in_the_action_value_vector = 0;

	for (i = 0; i < DqnParams::kMinibatchSize; ++i)
	{
		if (replay_memory_.size() < transitions[i])
		{
			printf("ERRO GRAVISSIMO\n");
			exit(-1);
		}

		Transition& transition = replay_memory_[transitions[i]];
		int action = transition.action;

		assert(static_cast<int>(action) < DqnParams::kNumCommands);

		double reward = transition.reward;
		double target = 0.0;

		if (DqnParams::TrainingModel == DQN_Q_LEARNING)
		{
			// Q-value re-estimation (from Q-learning)
			if (!transition.is_final_state)
			{
				// @filipe: ATENCAO!! NAO APAGUE ESSE COMENTARIO!! A variavel "actions_and_values" eh menor que o numero de transicoes porque o Q-value do
				// proximo estado NAO eh calculado para estados finais. Por isso o indice "transition_id_in_the_action_value_vector" eh usado para acessa-la
				// ao inves do "i". NUNCA TROQUE O "transition_id_in_the_action_value_vector" ABAIXO PARA i!!! ISSO FARA O CODIGO ACESSAR POSICOES INVALIDAS
				// DO VETOR, NAO DARA NENHUM ERRO E A REDE USARA VALORES MALUCOS PARA SE CALCULAR SE ATUALIZAR!!!!
				target = reward	+
						gamma_ * actions_and_values[transition_id_in_the_action_value_vector].second /*- transition.estimated_reward*/;
				transition_id_in_the_action_value_vector++;

				//printf("%d TGT: %lf REW: %lf NET: %lf\n", i, target, reward, actions_and_values[transition_id_in_the_action_value_vector - 1].second);
			}
			else
			{
				// @filipe: Isso nao eh problema? Imagine que o robo passou por uma pose "A" e eventualmente chegou ao goal. A acao realizada na pose A
				// deve se reforcada nesse caso porque ela levou ao goal. Agora, imagine que o carro fez um trajeto qualquer e parou em "A". Com
				// o codigo abaixo, vamos treinar a recompensa imediata em "A"... Eh como se estivessemos desvalorizando "A" dado que podemos a partir
				// deste estado podemos alcancar uma recompensa bem maior que a imediata. Nao seria uma boa ideia escolher o maximo entre a recompensa
				// imediata e a predicao da rede? TODO: Pensar se essa solucao nao causa problema nos casos finais normais (atingir o goal e bater) e
				// se ela for OK, implementar.
				target = reward;
				//printf("%d TGT: %lf REW: %lf NET: %lf\n", i, target, reward, 0.0);
			}
		}
		else if (DqnParams::TrainingModel == DQN_TOTAL_REWARD ||
					DqnParams::TrainingModel == DQN_INFINITY_HORIZON_DISCOUNTED_MODEL ||
					DqnParams::TrainingModel == DQN_REWARD_WITH_DECAY)
		{
			target = transition.reward;
		}
		else
		{
			exit(printf("ERROR: Unknown training model!\n"));
		}

		assert(!isnan(target));

		target_input[i * DqnParams::kNumCommands + static_cast<int>(action)] = target;
		filter_input[i * DqnParams::kNumCommands + static_cast<int>(action)] = 1;

		int k;
		std::list<int>::iterator it;

		/* indice da informacao no vector additional_data_input. O valor de k
		  deve ser sempre maior nos 3 loops abaixo. */
		k = 0;

		/** ADICIONA OS COMANDOS **/
		//printf("\nPAST COMMANDS\n");
		for (it = transition.commands.begin(); it != transition.commands.end(); it++)
		{
			float command = (float) (*it) / 10.0;
			//printf("transition.commands[%d]: %f\n", k, command);

			for (j = 0;	j < DqnParams::kNumCopyData; j++, k++)
			{
				additional_data_input[DqnParams::kNumAdditionalData * i + k] = command;
			}
		}

		/** ADICIONA O ESTADO DOS BOTOES **/
		// printf("\nBUTTONS\n");
		for (int mid = 0; mid < TOTAL_BUTTONS; mid++)
		{
			//printf("transition.map_buttons[%d]: %f\n", mid, (float) transition.map_buttons[mid]);

			for (j = 0;	j < DqnParams::kNumCopyData; j++, k++)
			{
				additional_data_input[DqnParams::kNumAdditionalData * i + k] = (float) transition.map_buttons[mid];
			}
		}

		/** ADICIONA AS INFOS DA ROM **/
		//printf("\nROMS INFOS\n");
		for (int rid = 0; rid < transition.rom_information.size(); rid++)
		{
			float info = (float) transition.rom_information[rid] / 100.0;
			//printf("transition.rom_information[%d]: %f\n", rid, info);

			for (j = 0;	j < DqnParams::kNumCopyData; j++, k++)
			{
				additional_data_input[DqnParams::kNumAdditionalData * i + k] = info;
			}
		}
		//printf("\n");

		//printf("%d %d\n", i, transitions[i]);
		//getchar();

		assert(k == DqnParams::kNumAdditionalData);

		static double last_print = get_time();
		double curr_time = get_time();

		// DEBUG:
		if (i == 0 && (curr_time - last_print) > 1.0) {
			std::cerr << "Batch 0 - " << " filter:" << _action_to_string(action)
					<< " reward: " << reward << " net estimation: "
					<< actions_and_values[i].second << " target: " << target
					<< " DIFF: " << fabs(actions_and_values[i].second - target)
					<< std::endl;
			last_print = curr_time;
		}

		for (j = 0; j < DqnParams::kInputFrameCount; ++j)
		{
			FrameDataSp& frame_data = transition.input_frames[j];

			for (int k = 0; k < DqnParams::kCroppedFrameDataSize; k++)
				frames_input[i * DqnParams::kInputDataSize + j * DqnParams::kCroppedFrameDataSize + k] = (float) frame_data->at(k) / 255.0;

			//std::copy(frame_data->begin(), frame_data->end(),
			//	frames_input.begin() + i * DqnParams::kInputDataSize + j * DqnParams::kCroppedFrameDataSize);
		}
	}

//	printf("------------------------------\n\n");
//	getchar();

	InputDataIntoLayers(frames_input, target_input, filter_input,
			additional_data_input);

	if (!params()->USE_MULTI_GPU) {
		solver_->Step(4);
	} else {
//		caffe::P2PSync<float> sync(solver_, NULL, solver_.get()->param());
//		sync.Run(params()->GPU_IDS);
	}

	assert(!isnan(net_->layer_by_name("conv1_layer")->blobs().front()->data_at(1, 0, 0, 0)));
	assert(!isnan(net_->layer_by_name("conv2_layer")->blobs().front()->data_at(1, 0, 0, 0)));
	assert(!isnan(net_->layer_by_name("ip1_layer")->blobs().front()->data_at(1, 0, 0, 0)));
	assert(!isnan(net_->layer_by_name("ip1.5_layer")->blobs().front()->data_at(1, 0, 0, 0)));
	assert(!isnan(net_->layer_by_name("ip2_layer")->blobs().front()->data_at(1, 0, 0, 0)));

	static std::vector<float> max_diff;
	std::vector<float> current_max_diff;
	std::vector<float> current_pred;
	std::vector<float> current_expected;

	if (max_diff.size() == 0)
		for (int i = 0; i < _params.kNumCommands; i++, max_diff.push_back(0));

	for (int i = 0; i < _params.kNumCommands;i++, current_max_diff.push_back(0));
	for (int i = 0; i < _params.kNumCommands; i++, current_pred.push_back(0));
	for (int i = 0; i < _params.kNumCommands;i++, current_expected.push_back(0));

	for (i = 0; i < DqnParams::kMinibatchSize; i++) {
		Transition& transition = replay_memory_[transitions[i]];
		int action = transition.action;

		assert(static_cast<int>(action) < DqnParams::kNumCommands);

		int action_id = static_cast<int>(action);

		if (fabs(net_->blob_by_name("target")->data_at(i, action_id, 0, 0) -
				net_->blob_by_name("filtered_q_values")->data_at(i, action_id, 0, 0)) > max_diff[action_id])
			max_diff[action_id] = fabs(net_->blob_by_name("target")->data_at(i, action_id, 0, 0) -
									net_->blob_by_name("filtered_q_values")->data_at(i, action_id, 0, 0));

		if (fabs(net_->blob_by_name("target")->data_at(i, action_id, 0, 0) -
				net_->blob_by_name("filtered_q_values")->data_at(i,action_id, 0, 0)) >
				current_max_diff[action_id]) {

			current_max_diff[action_id] = fabs(net_->blob_by_name("target")->data_at(i, action_id, 0, 0) -
										net_->blob_by_name("filtered_q_values")->data_at(i, action_id, 0, 0));
			current_pred[action_id] = net_->blob_by_name("filtered_q_values")->data_at(i,action_id, 0, 0);
			current_expected[action_id] = net_->blob_by_name("target")->data_at(i, action_id, 0, 0);
		}
	}
}

